-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2016 at 04:31 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hospitalpro`
--
CREATE DATABASE IF NOT EXISTS `hospitalpro` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hospitalpro`;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_allergen`
--

CREATE TABLE IF NOT EXISTS `hsp_allergen` (
  `alrg_id` int(11) NOT NULL AUTO_INCREMENT,
  `alrg_name` varchar(255) NOT NULL,
  `alg_type` enum('Mild','Moderate','Severe') NOT NULL DEFAULT 'Mild',
  `alrg_status` enum('1','0') NOT NULL,
  `alrg_ref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`alrg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hsp_allergen`
--

INSERT INTO `hsp_allergen` (`alrg_id`, `alrg_name`, `alg_type`, `alrg_status`, `alrg_ref_id`) VALUES
(1, 'wdsd', 'Mild', '1', 0),
(2, 'wdsdtext', 'Mild', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hsp_brand`
--

CREATE TABLE IF NOT EXISTS `hsp_brand` (
  `brnd_id` int(11) NOT NULL AUTO_INCREMENT,
  `brnd_unit_id` int(11) NOT NULL DEFAULT '0',
  `brnd_name` varchar(255) NOT NULL,
  `brnd_generic_details` text NOT NULL,
  `brnd_unit` int(11) NOT NULL,
  `brnd_dosage_form` varchar(500) NOT NULL,
  `brnd_volume` varchar(500) NOT NULL,
  `brnd_company` int(11) NOT NULL,
  `brnd_status` enum('0','1') NOT NULL DEFAULT '1',
  `brnd_notes` text,
  PRIMARY KEY (`brnd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hsp_brand`
--

INSERT INTO `hsp_brand` (`brnd_id`, `brnd_unit_id`, `brnd_name`, `brnd_generic_details`, `brnd_unit`, `brnd_dosage_form`, `brnd_volume`, `brnd_company`, `brnd_status`, `brnd_notes`) VALUES
(10, 6, 'Brand 1', 'a:1:{s:0:"";s:0:"";}', 0, '7', '', 0, '1', ''),
(11, 5, 'Brand 10', 'a:1:{i:13;s:3:"150";}', 0, '7', '', 3, '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_complain`
--

CREATE TABLE IF NOT EXISTS `hsp_complain` (
  `cmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmp_cat_id` int(11) NOT NULL,
  `cmp_name` varchar(255) NOT NULL,
  `cmp_status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`cmp_id`),
  KEY `cmp_cat_id` (`cmp_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hsp_complain`
--

INSERT INTO `hsp_complain` (`cmp_id`, `cmp_cat_id`, `cmp_name`, `cmp_status`) VALUES
(1, 5, 'Fainting/Blackout', '1'),
(2, 5, 'Allergic rhinitis', '1'),
(3, 5, 'Dizziness', '1'),
(4, 5, 'Dehydration', '1'),
(5, 6, 'Breathlessness', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_complain_category`
--

CREATE TABLE IF NOT EXISTS `hsp_complain_category` (
  `cmpln_catid` int(11) NOT NULL AUTO_INCREMENT,
  `cmpln_catname` varchar(255) NOT NULL,
  `cmpln_catstatus` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`cmpln_catid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `hsp_complain_category`
--

INSERT INTO `hsp_complain_category` (`cmpln_catid`, `cmpln_catname`, `cmpln_catstatus`) VALUES
(5, 'General', '1'),
(6, 'CVS', '1'),
(7, 'Respiratory', '1'),
(8, 'GI', '1'),
(9, 'Miscellaneous', '1'),
(10, 'Neurologic', '1'),
(11, 'Psychiatric', '1'),
(12, 'Genito-urinary', '1'),
(13, 'Gynecological', '1'),
(14, 'Endocrine', '1'),
(15, 'Musculoskeletal', '1'),
(16, 'Integumentary (Skin, hair, nail)', '1'),
(17, 'Head/Neck/Mouth', '1'),
(18, 'Eye', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_health_condition`
--

CREATE TABLE IF NOT EXISTS `hsp_health_condition` (
  `hc_id` int(11) NOT NULL AUTO_INCREMENT,
  `hc_name` varchar(255) NOT NULL,
  `hc_status` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`hc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `hsp_health_condition`
--

INSERT INTO `hsp_health_condition` (`hc_id`, `hc_name`, `hc_status`) VALUES
(1, 'None', '0'),
(2, 'Arthritis', '0'),
(3, 'Asthma', '0'),
(4, 'COPD', '0'),
(5, 'Depression', '0'),
(6, 'Diabetes', '0'),
(7, 'Glaucoma', '0'),
(8, 'High Cholesterol', '0'),
(9, 'Hypertension', '0'),
(10, 'Kidney disease', '0'),
(11, 'Liver disease', '0'),
(12, 'Seasonal allergies', '0'),
(13, 'Seizures/Epilepsy', '0'),
(14, 'Thyroid', '0'),
(15, 'Ulcer/Acid reflux/GERD', '0'),
(16, 'Pregnancy', '0'),
(17, 'Nursing mother', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_patient`
--

CREATE TABLE IF NOT EXISTS `hsp_patient` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_fname` varchar(255) NOT NULL,
  `pt_mname` varchar(255) DEFAULT NULL,
  `pt_lname` varchar(255) DEFAULT NULL,
  `pt_dob` date NOT NULL,
  `pt_gender` enum('male','female') NOT NULL,
  `pt_mobile` varchar(255) NOT NULL,
  `pt_reg_number` varchar(255) NOT NULL,
  `pt_image` varchar(255) NOT NULL,
  `pt_address` text NOT NULL,
  `pt_city` varchar(255) DEFAULT NULL,
  `pt_zip` varchar(255) DEFAULT NULL,
  `pt_prefered_lang` varchar(255) DEFAULT NULL,
  `pt_prefered_pharma` varchar(255) DEFAULT NULL,
  `pt_insurance_id` int(11) DEFAULT NULL,
  `pt_insurance_name` varchar(255) DEFAULT NULL,
  `pt_referal_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_patient_family_history`
--

CREATE TABLE IF NOT EXISTS `hsp_patient_family_history` (
  `pfh_id` int(11) NOT NULL AUTO_INCREMENT,
  `pfh_name` varchar(255) NOT NULL,
  `pfh_relation` text NOT NULL,
  `pfh_status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`pfh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_patient_medical_history`
--

CREATE TABLE IF NOT EXISTS `hsp_patient_medical_history` (
  `pmh_id` int(11) NOT NULL AUTO_INCREMENT,
  `pmh_name` varchar(255) NOT NULL,
  `pmh_status` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`pmh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `hsp_patient_medical_history`
--

INSERT INTO `hsp_patient_medical_history` (`pmh_id`, `pmh_name`, `pmh_status`) VALUES
(1, 'None', '0'),
(2, 'Arthritis', '0'),
(3, 'Asthma', '0'),
(4, 'COPD', '0'),
(5, 'Depression', '0'),
(6, 'Diabetes Type I', '0'),
(7, 'Diabetes Type II', '0'),
(8, 'Eye problems', '0'),
(9, 'High Cholesterol', '0'),
(10, 'Hypertension', '0'),
(11, 'Kidney stones', '0'),
(12, 'Kidney disease', '0'),
(13, 'Jaundice', '0'),
(14, 'Liver disease', '0'),
(15, 'Heart attack', '0'),
(16, 'Seizures/Epilepsy', '0'),
(17, 'Thyroid disease', '0'),
(18, 'Ulcer/Acid reflux/GERD', '0'),
(19, 'Coronary artery disease', '0'),
(20, 'Congestive Heart Failure', '0'),
(21, 'Arrythmia', '0'),
(22, 'Cancer (Type)', '0'),
(23, 'Stroke', '0'),
(24, 'HIV/AIDS', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_patient_surgical_history`
--

CREATE TABLE IF NOT EXISTS `hsp_patient_surgical_history` (
  `psh_id` int(11) NOT NULL AUTO_INCREMENT,
  `psh_name` varchar(255) NOT NULL,
  `psh_status` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`psh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `hsp_patient_surgical_history`
--

INSERT INTO `hsp_patient_surgical_history` (`psh_id`, `psh_name`, `psh_status`) VALUES
(1, 'None', '0'),
(2, 'Caesarian Section', '0'),
(3, 'Cataract', '0'),
(4, 'Cardiac bypass surgery', '0'),
(5, 'Hysterectomy', '0'),
(6, 'Appendectomy', '0'),
(7, 'Cholecystectomy', '0'),
(8, 'Hernia repair', '0'),
(9, 'Angioplasty', '0'),
(10, 'Emergency surgery following accident', '0'),
(11, 'Prostatectomy', '0'),
(12, 'Hip & knee replacement', '0'),
(13, 'Bariatric surgery', '0'),
(14, 'Cosmetic surgery', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_vaccine`
--

CREATE TABLE IF NOT EXISTS `hsp_vaccine` (
  `vc_id` int(11) NOT NULL AUTO_INCREMENT,
  `vc_name` text NOT NULL,
  `vc_brand_id` text NOT NULL,
  `vc_type` enum('Live Viral','Inactivated Bacterial','Inactivated BacterialToxoids','Inactivated Viral','Recombinant Viral','Live Attenuated Viral','Recombinant Bacterial','Live Attenuated Bacterial','Inactivated Bacterial Viral','Inactivated/RecombinantViral') NOT NULL DEFAULT 'Live Viral',
  `vc_status` enum('1','0') NOT NULL DEFAULT '0',
  `vc_min_msg` text,
  PRIMARY KEY (`vc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hsp_vaccine`
--

INSERT INTO `hsp_vaccine` (`vc_id`, `vc_name`, `vc_brand_id`, `vc_type`, `vc_status`, `vc_min_msg`) VALUES
(1, 'test', '10', 'Live Viral', '1', NULL),
(2, 'test1', '11', 'Live Viral', '1', NULL),
(3, 'rs', '11', 'Live Viral', '0', NULL),
(4, 'test123', '10', 'Live Viral', '1', NULL),
(5, 'a', '10', 'Live Viral', '0', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hsp_complain`
--
ALTER TABLE `hsp_complain`
  ADD CONSTRAINT `hsp_complain_ibfk_1` FOREIGN KEY (`cmp_cat_id`) REFERENCES `hsp_complain_category` (`cmpln_catid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
