<?php $baseUrl = Yii::app()->theme->baseUrl; ?>
<?php if (Yii::app()->user->getState('usr_type') == "admin") {
    ?>
    <body class="page-header-fixed  pace-done">
        <?php
    } else {
        ?>
    <body class="page-header-fixed compact-menu page-horizontal-bar pace-done">
    <?php }
    ?>
    <div class="overlay"></div>
    <main class="page-content content-wrap">    
        <div class="navbar">
            <div class="navbar-inner">
                <div class="sidebar-pusher">
                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="logo-box">
                    <a href="<?php Yii::app()->createUrl('site/index') ?>" class="logo-text"><span>Hospital</span></a>
                </div><!-- Logo Box -->
                <div class="topmenu-outer">
                    <div class="top-menu">
                        <ul class="nav navbar-nav navbar-left">
                            <?php if (Yii::app()->user->getState('usr_type') == "admin") { ?> 
                                <li>        
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle" title=""  data-toggle="tooltip" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-bars"></i></a>
                                </li>
                            <?php } ?>

                            <li>        
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen" title=""  data-toggle="tooltip" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a>
                            </li>
                            <?php if (Yii::app()->user->getState('usr_type') != "admin") { ?> 
                                 <li>       
                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('user/staff'); ?>" class="waves-effect waves-button waves-classic " title=""  data-toggle="tooltip" data-placement="bottom" data-original-title="Staff" ><i class="fa fa-user-md">  &nbsp;</i></a>
                                </li>  
                                <li>        
                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('facility/admin'); ?>" class="waves-effect waves-button waves-classic " title=""  data-toggle="tooltip" data-placement="bottom" data-original-title="Facility"><i class="fa fa-hospital-o">  &nbsp;</i></a>
                                </li>  

                            <?php } ?>
                            <?php
                            if (Yii::app()->user->getState("facility") != "") {
                                ?>
                                <li>		
                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('patient/view'); ?>" class="waves-effect waves-button waves-classic "  title="Patient"><i class="fa  fa-plus-square-o ">  &nbsp; </i></a>
                                </li>
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle waves-effect waves-button waves-classic" title="Settings" href="#" aria-expanded="true">
                                        <i class="glyphicon glyphicon-cog"></i> 
                                    </a>
                                    <ul role="menu" class="dropdown-menu dropdown-md dropdown-list theme-settings">
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li role="presentation" class="no-link">
                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('facility/settings'); ?>">
                                                        Facility 
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li role="presentation" class="no-link">
                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('services/admin'); ?>">
                                                        Services
                                                    </a>

                                                </li>

                                            </ul>
                                        </li>
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li role="presentation" class="no-link">
                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('facility/staff'); ?>">
                                                        Staff
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li role="presentation" class="no-link">
                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('facility/workinghour'); ?>">
                                                        Working Hours
                                                    </a>

                                                </li>
                                            </ul>
                                        </li>
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li role="presentation" class="no-link">
                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('incomeExpense/admin'); ?>">
                                                        Income
                                                    </a>

                                                </li>

                                            </ul>
                                        </li>
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li role="presentation" class="no-link">
                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('incomeExpense/expense'); ?>">
                                                        Expense
                                                    </a>

                                                </li>

                                            </ul>
                                        </li>

                                    </ul>

                                </li>   

                                <?php
                            }
                            ?>

                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                    <span class="user-name"><?php echo Yii::app()->user->getState('usr_username'); ?><i class="fa fa-angle-down"></i></span>
                                </a>

                                <ul class="dropdown-menu dropdown-list" role="menu">
                                    <?php
                                    if (Yii::app()->user->getState('usr_type') == "doctor") {

                                        $attributes = array('fac_usr_id' => Yii::app()->user->id);
                                        $facility = CHtml::listData(Facility::model()->findAllByAttributes($attributes), 'fac_id', 'fac_name');
                                        foreach ($facility as $key => $val) {
                                            ?>
                                            <li role="presentation">
                                                <a href="<?php echo Yii::app()->createUrl('facility/main', array('id' => $key)) ?>"><i class="fa fa-h-square"></i>Facility - <?php echo $val; ?></a>
                                            </li>
                                            <?php
                                        }
                                        ?> 
                                    <?php } ?>   
                                    <li role="presentation"><a href="<?php echo Yii::app()->createUrl('user/editprofile') ?>"><i class="fa fa-edit"></i>Edit Profile</a></li>
                                    <li role="presentation"><a href="<?php echo Yii::app()->createUrl('user/changepassword'); ?>"><i class="fa fa-lock m-r-xs"></i>Change Password</a></li>
                                    <li role="presentation"><a href="<?php echo Yii::app()->createUrl('site/logout'); ?>"><i class="fa fa-power-off m-r-xs"></i>Log out</a></li>
                                </ul>
                            </li>
                        </ul><!-- Nav -->
                    </div><!-- Top Menu -->
                </div>
            </div>
        </div><!-- Navbar -->
        <?php if (Yii::app()->user->getState('usr_type') == "admin") { ?>
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <div class="sidebar-header">
                        <div class="sidebar-profile">
                            <a href="javascript:void(0);" id="profile-menu-link">
                                <div class="sidebar-profile-image">
                                    <img src="<?php echo $baseUrl; ?>/assets/images/profile-menu-image.png" class="img-circle img-responsive" alt="">
                                </div>
                                <div class="sidebar-profile-details">
                                    <span style="color: #337ab7;"><?php echo Yii::app()->user->getState("usr_username"); ?></span>
                                </div>
                            </a>
                        </div>
                    </div>

                    <?php
                    // echo Yii::app()->user->checkAccess('Customer admin')."test"; exit;
                    $this->widget('zii.widgets.CMenu', array(
                        'htmlOptions' => array('class' => 'menu accordion-menu'),
                        'encodeLabel' => false,
                        'items' => array(
                            array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Master </p><span class="arrow"></span>', 'url' => '#', 'itemOptions' => array('class' => 'droplink', 'tabindex' => "-1"), 'submenuOptions' => array('class' => 'sub-menu'), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-target' => '#'),
                                'items' => array(
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Disciplines</p>', 'url' => array('discipline/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Generic</p>', 'url' => array('generic/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Interactions</p>', 'url' => array('Interaction/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Allergen</p>', 'url' => array('allergen/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Dosage</p>', 'url' => array('dosage/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Frequency</p>', 'url' => array('frequency/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Class</p>', 'url' => array('hclass/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Company</p>', 'url' => array('company/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Route</p>', 'url' => array('route/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Disease</p>', 'url' => array('disease/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Brand</p>', 'url' => array('brand/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Units</p>', 'url' => array('units/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Complain Category</p>', 'url' => array('complaincategory/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Complain</p>', 'url' => array('complain/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Procedure Category</p>', 'url' => array('procedure/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Procedure</p>', 'url' => array('procedure/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Diagnosis Category</p>', 'url' => array('diagnosiscategory/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                    array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Diagnosis</p>', 'url' => array('diagnosis/admin'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                                )),
                            array('label' => '<span class="menu-icon glyphicon glyphicon-th"></span><p> Doctors</p>', 'url' => array('user/doctor'), 'htmlOptions' => array('class' => 'waves-effect waves-button')),
                        ),
                    ));
                    ?>

                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
        <?php } ?>