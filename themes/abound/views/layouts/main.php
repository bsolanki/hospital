<!DOCTYPE html>
<html lang="en" class="">
    <head>
        <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <meta charset="utf-8">
        <!-- <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'> -->
        <?php
        $baseUrl = Yii::app()->theme->baseUrl;
        $cs = Yii::app()->getClientScript();
        Yii::app()->clientScript->registerCoreScript('jquery');
        ?>
<!--        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>-->
        <?php
        $cs->registerCssFile('http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700');
        $cs->registerCssFile($baseUrl . '/assets/plugins/pace-master/themes/blue/pace-theme-flash.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/uniform/css/uniform.default.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/bootstrap/css/bootstrap.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/fontawesome/css/font-awesome.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/line-icons/simple-line-icons.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/waves/waves.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/switchery/switchery.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/3d-bold-navigation/css/style.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/slidepushmenus/css/component.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/weather-icons-master/css/weather-icons.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/metrojs/MetroJs.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/toastr/toastr.min.css');
        $cs->registerCssFile($baseUrl . '/assets/css/modern.css');
        $cs->registerCssFile($baseUrl . '/assets/css/custom.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
        $cs->registerCssFile($baseUrl . '/assets/plugins/bootstrap-datepicker/css/datepicker3.css');
        // $cs->registerScriptFile($baseUrl . '/assets/plugins/jquery/jquery-2.1.4.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/3d-bold-navigation/js/modernizr.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/jquery-ui/jquery-ui.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/pace-master/pace.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/jquery-blockui/jquery.blockui.js');
////        $cs->registerScriptFile($baseUrl . '/assets/plugins/bootstrap/js/bootstrap.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/switchery/switchery.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/uniform/jquery.uniform.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/classie/classie.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/3d-bold-navigation/js/main.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/waypoints/jquery.waypoints.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/jquery-counterup/jquery.counterup.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/toastr/toastr.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/js/pages/form-wizard.js');
//         
////          $cs->registerScriptFile($baseUrl.'/assets/plugins/flot/jquery.flot.min.js');
////          $cs->registerScriptFile($baseUrl.'/assets/plugins/flot/jquery.flot.time.min.js');
////          $cs->registerScriptFile($baseUrl.'/assets/plugins/flot/jquery.flot.symbol.min.js');
////          $cs->registerScriptFile($baseUrl.'/assets/plugins/flot/jquery.flot.resize.min.js');
////          $cs->registerScriptFile($baseUrl.'/assets/plugins/flot/jquery.flot.tooltip.min.js');
////          $cs->registerScriptFile($baseUrl.'/assets/plugins/curvedlines/curvedLines.js');
//        $cs->registerScriptFile($baseUrl . '/assets/plugins/metrojs/MetroJs.min.js');
//        $cs->registerScriptFile($baseUrl . '/assets/js/modern.js');
//          $cs->registerScriptFile($baseUrl.'/assets/js/pages/dashboard.js');
        ?>


        <!-- Javascripts -->
        <script src="<?php echo $baseUrl; ?>/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/pace-master/pace.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>

        <script src="<?php echo $baseUrl; ?>/assets/plugins/waves/waves.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/3d-bold-navigation/js/main.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/toastr/toastr.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/js/modern.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/js/pages/form-wizard.js"></script>
        <script src="<?php echo $baseUrl; ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    </head>

<?php
if (!Yii::app()->user->isGuest) {
    ?>
        <?php require_once('tpl_navigation.php') ?>
        <div class="page-sidebar sidebar horizontal-bar">
            <div class="page-sidebar-inner">
            </div><!-- Page Sidebar Inner -->
        </div>
        <div class="page-inner" <?php if (Yii::app()->user->getState('usr_type') != "doctor"): ?> style="min-height:1080px !important"<?php endif; ?>>

    <?php if (Yii::app()->user->getState('usr_type') != "doctor"): ?>
                <div class="page-title">
                <?php if (isset($this->breadcrumbs)): ?>
                        <?php
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links' => $this->breadcrumbs,
                            'homeLink' => CHtml::link('Dashboard'),
                            'htmlOptions' => array('class' => 'breadcrumb')
                        ));
                        ?><!-- breadcrumbs -->
                    <?php endif ?>
                </div>

    <?php endif ?>
            <?php if (Yii::app()->user->getState('usr_type') == "doctor"): ?> 
            <?php endif ?>
            <div id="">
        <?php } ?>
            <?php echo $content; ?>

            <?php
            if (!Yii::app()->user->isGuest) {
                ?>

            </div>

    <?php require_once('tpl_footer.php') ?>
        </div>
        <?php } ?>
</html>