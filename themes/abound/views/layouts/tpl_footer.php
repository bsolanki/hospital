</div>
<div id="loading"></div>
<div class="page-footer" >
    <p class="no-s"><?php echo Yii::app()->params['Copyright'] ?></p>
</div>

<script type="text/javascript">

    $(".add_button").click(function () {
    $(".add_form").toggle();
    });
            $("ul.navbar-right li.dropdown").click(function (event) {
    if ($(".editable-click").length > 0) {
    $(this).toggleClass('open');
    }
    });
            $(document).ready(function () {

<?php if (Yii::app()->user->getState('usr_type') == "doctor") { ?>
        $('body').removeAttr('class');
                $('body').addClass('page-header-fixed  pace-done page-horizontal-bar');
                $('.sidebar').addClass('horizontal-bar');
                $('.sidebar').removeClass('page-sidebar');
                $('.accordion-menu li ul.sub-menu').css('background', "#f7f7f7 none repeat scroll 0 0");
<?php } ?>


    $(document).ajaxStop(function () {
    $('#loading').hide();
    });
            $(document).ajaxStart(function () {
    $('#loading').show();
    });
    });
            function isNumber(evt) {
            evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
//    alert(charCode);
                    if (charCode > 31 && (charCode < 44 || charCode > 57)) {
            return false;
            }
            return true;
            }

    function changestatus(model, id) {
    if (confirm("Are you sure you want to change status?")) {
    $.ajax({url: <?php echo Yii::app()->baseUrl . "/"; ?> + model + "/ChangeStatus/" + id, success: function (result) {
    $('.grid-view').yiiGridView('update', {
    data: $(this).serialize()
    });
    }});
    } else {
    return false;
    }

    }

    function check_digit(e, obj, intsize, deczize)
    {
    var keycode;
            if (window.event)
            keycode = window.event.keyCode;
            else if (e) {
    keycode = e.which;
    }
    else {
    return true;
    }

    var fieldval = (obj.value),
            dots = fieldval.split(".").length;
            if (keycode == 46) {
    return dots <= 1;
    }

    if (keycode == 8 || keycode == 9 || keycode == 46 || keycode == 13) {
    // back space, tab, delete, enter 
    return true;
    }

    if ((keycode >= 32 && keycode <= 45) || keycode == 47 || (keycode >= 58 && keycode <= 127)) {
    return false;
    }

    if (fieldval == "0" && keycode == 48) {
    return false;
    }

    if (fieldval.indexOf(".") != - 1)
    {
    if (keycode == 46) {
    return false;
    }
    var splitfield = fieldval.split(".");
            if (splitfield[1].length >= deczize && keycode != 8 && keycode != 0)
    {
    return false;
    } else if (fieldval.length >= intsize && keycode != 46) {
    return false;
    } else {
    return true;
    }
    }
    }


    function Faildedmsg(message){

    setTimeout(function() {
    toastr.options = {
    closeButton: true,
            progressBar: true,
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut',
            timeOut: 5000
    };
            toastr.error('', message);
    }, 500);
    }

    function validationmsg(message){

    setTimeout(function() {
    toastr.options = {
    closeButton: true,
            progressBar: true,
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut',
            timeOut: 5000
    };
            toastr.success('', message);
    }, 500);
    }

</script>

