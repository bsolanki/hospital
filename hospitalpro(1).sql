-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 18, 2016 at 11:53 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hospitalpro`
--

-- --------------------------------------------------------

--
-- Table structure for table `hsp_brand`
--

CREATE TABLE IF NOT EXISTS `hsp_brand` (
  `brnd_id` int(11) NOT NULL AUTO_INCREMENT,
  `brnd_name` varchar(255) NOT NULL,
  `brnd_generic_details` text NOT NULL,
  `brnd_unit` int(11) NOT NULL,
  `brnd_dosage_form` varchar(500) NOT NULL,
  `brnd_volume` varchar(500) NOT NULL,
  `brnd_company` int(11) NOT NULL,
  `brnd_status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`brnd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `hsp_brand`
--

INSERT INTO `hsp_brand` (`brnd_id`, `brnd_name`, `brnd_generic_details`, `brnd_unit`, `brnd_dosage_form`, `brnd_volume`, `brnd_company`, `brnd_status`) VALUES
(4, 'bhavin', 'a:2:{i:13;s:2:"11";i:14;s:2:"12";}', 0, '7', '12', 3, '1'),
(5, 'test', 'a:2:{i:13;s:2:"50";i:14;s:2:"20";}', 0, '7', '15', 3, '1'),
(6, 'aaa', 'a:1:{i:13;s:2:"20";}', 0, '7', '12', 3, '1'),
(7, 'aba', 'a:3:{i:13;s:1:"1";i:14;s:1:"2";i:15;s:1:"3";}', 0, '7', '10', 3, '1'),
(8, 'dsadasd', 'a:3:{i:14;s:2:"15";i:15;s:2:"25";i:13;s:2:"30";}', 0, '7', '10', 3, '1'),
(9, 'aaa1', 'a:2:{i:13;s:2:"12";i:14;i:0;}', 0, '7', '10', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_class`
--

CREATE TABLE IF NOT EXISTS `hsp_class` (
  `cls_id` int(11) NOT NULL AUTO_INCREMENT,
  `cls_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cls_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`cls_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `hsp_class`
--

INSERT INTO `hsp_class` (`cls_id`, `cls_name`, `cls_status`) VALUES
(9, 'Class 3', '1'),
(10, 'Class 2', '1'),
(11, 'Class 1', '1'),
(12, 'T 123', '1'),
(13, 'abctett', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_clinic`
--

CREATE TABLE IF NOT EXISTS `hsp_clinic` (
  `cnc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnc_usr_id` int(11) NOT NULL,
  `cnc_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnc_address` text COLLATE utf8_unicode_ci,
  `cnc_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnc_zip` int(11) DEFAULT NULL,
  `cnc_opening_hrs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnc_closing_hrs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnc_lunch_hrs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnc_basic_fees` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cnc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_company`
--

CREATE TABLE IF NOT EXISTS `hsp_company` (
  `cmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmp_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cmp_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`cmp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hsp_company`
--

INSERT INTO `hsp_company` (`cmp_id`, `cmp_name`, `cmp_status`) VALUES
(3, 'Aristo Pharmaceuticals Pvt. Ltd.', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_discipline`
--

CREATE TABLE IF NOT EXISTS `hsp_discipline` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dis_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hsp_discipline`
--

INSERT INTO `hsp_discipline` (`dis_id`, `dis_name`, `dis_status`) VALUES
(3, 'Dermatology', '1'),
(4, 'Pharmacology1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_disease`
--

CREATE TABLE IF NOT EXISTS `hsp_disease` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_name` varchar(255) NOT NULL,
  `dis_status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `hsp_disease`
--

INSERT INTO `hsp_disease` (`dis_id`, `dis_name`, `dis_status`) VALUES
(7, 'Disease 1 1', '1'),
(9, 'diseases123', '1'),
(10, 'Disease 1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_dosage`
--

CREATE TABLE IF NOT EXISTS `hsp_dosage` (
  `ds_id` int(11) NOT NULL AUTO_INCREMENT,
  `ds_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ds_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`ds_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `hsp_dosage`
--

INSERT INTO `hsp_dosage` (`ds_id`, `ds_name`, `ds_status`) VALUES
(7, 'Tablet', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_email_template`
--

CREATE TABLE IF NOT EXISTS `hsp_email_template` (
  `email_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `email_template_name` varchar(75) NOT NULL,
  `email_template_desc` varchar(500) NOT NULL,
  `email_template_subject` varchar(75) NOT NULL,
  `email_template_body` varchar(500) NOT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_frequency`
--

CREATE TABLE IF NOT EXISTS `hsp_frequency` (
  `frqcy_id` int(11) NOT NULL AUTO_INCREMENT,
  `frqcy_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frqcy_status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`frqcy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_generic`
--

CREATE TABLE IF NOT EXISTS `hsp_generic` (
  `gn_id` int(11) NOT NULL AUTO_INCREMENT,
  `gn_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gn_status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `gn_class` int(11) DEFAULT NULL,
  `gn_diesease` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gn_max_dose_day` int(11) DEFAULT NULL,
  `gn_unit_id` int(11) DEFAULT NULL,
  `gn_notes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`gn_id`),
  KEY `hsp_unit_id` (`gn_unit_id`),
  KEY `hsp_class` (`gn_class`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `hsp_generic`
--

INSERT INTO `hsp_generic` (`gn_id`, `gn_name`, `gn_status`, `gn_class`, `gn_diesease`, `gn_max_dose_day`, `gn_unit_id`, `gn_notes`) VALUES
(13, 'Test1', '1', 10, 'a:2:{i:0;s:1:"7";i:1;s:1:"9";}', 11, 5, NULL),
(14, 'Test2', '1', 9, 'a:2:{i:0;s:1:"7";i:1;s:1:"9";}', 11, 5, NULL),
(15, 'A', '1', 9, 'a:2:{i:0;s:1:"7";i:1;s:1:"9";}', 11, 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hsp_instruction`
--

CREATE TABLE IF NOT EXISTS `hsp_instruction` (
  `inst_id` int(11) NOT NULL AUTO_INCREMENT,
  `inst_name` varchar(255) NOT NULL,
  PRIMARY KEY (`inst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_medicine`
--

CREATE TABLE IF NOT EXISTS `hsp_medicine` (
  `mdc_id` int(11) NOT NULL AUTO_INCREMENT,
  `mdc_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdc_brnd_id` int(11) NOT NULL,
  `mdc_generic_id` int(11) NOT NULL,
  `mdc_strength` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdc_unit_id` int(11) NOT NULL,
  `mdc_dosage_id` int(11) NOT NULL,
  `mdc_volume` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdc_cmpy_id` int(11) NOT NULL,
  `mdc_notes` text COLLATE utf8_unicode_ci,
  `mdc_max_dose` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdc_condition` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`mdc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_patient`
--

CREATE TABLE IF NOT EXISTS `hsp_patient` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_fname` varchar(255) NOT NULL,
  `pt_mname` varchar(255) DEFAULT NULL,
  `pt_lname` varchar(255) DEFAULT NULL,
  `pt_dob` date NOT NULL,
  `pt_gender` enum('male','female') NOT NULL,
  `pt_mobile` varchar(255) NOT NULL,
  `pt_reg_number` varchar(255) NOT NULL,
  `pt_image` varchar(255) NOT NULL,
  `pt_address` text NOT NULL,
  `pt_city` varchar(255) DEFAULT NULL,
  `pt_zip` varchar(255) DEFAULT NULL,
  `pt_prefered_lang` varchar(255) DEFAULT NULL,
  `pt_prefered_pharma` varchar(255) DEFAULT NULL,
  `pt_insurance_id` int(11) DEFAULT NULL,
  `pt_insurance_name` varchar(255) DEFAULT NULL,
  `pt_referal_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hsp_route`
--

CREATE TABLE IF NOT EXISTS `hsp_route` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(255) NOT NULL,
  `route_status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hsp_route`
--

INSERT INTO `hsp_route` (`route_id`, `route_name`, `route_status`) VALUES
(4, 'IV', '1'),
(5, 'IM', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_units`
--

CREATE TABLE IF NOT EXISTS `hsp_units` (
  `unt_id` int(11) NOT NULL AUTO_INCREMENT,
  `unt_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unt_status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `unt_calculate` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`unt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `hsp_units`
--

INSERT INTO `hsp_units` (`unt_id`, `unt_name`, `unt_status`, `unt_calculate`) VALUES
(5, 'mg', '1', '1'),
(6, 'IU', '1', '1'),
(7, '%', '1', '0'),
(8, 'a', '0', '1'),
(9, 'aaa', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hsp_user`
--

CREATE TABLE IF NOT EXISTS `hsp_user` (
  `usr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `usr_password` varchar(50) NOT NULL,
  `usr_fname` varchar(200) NOT NULL,
  `usr_lname` varchar(200) NOT NULL,
  `usr_type` enum('receptionist','doctor','nurse','physician_assistant','admin') NOT NULL DEFAULT 'doctor',
  `usr_email` varchar(255) NOT NULL,
  `usr_phone` varchar(30) DEFAULT NULL,
  `usr_address` text,
  `usr_status_medical_council` varchar(500) NOT NULL,
  `usr_medical_number` varchar(500) NOT NULL,
  `usr_dis_id` varchar(500) NOT NULL,
  `usr_status` enum('y','n') NOT NULL DEFAULT 'y',
  `usr_reset_pass_token` varchar(200) DEFAULT NULL,
  `usr_reset_pass_date` datetime DEFAULT NULL,
  `usr_created_by` int(11) unsigned DEFAULT NULL,
  `usr_modified_by` int(11) unsigned DEFAULT NULL,
  `usr_created_date` datetime DEFAULT NULL,
  `usr_modified_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `usr_last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  KEY `usr_username` (`usr_fname`,`usr_lname`,`usr_email`,`usr_phone`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hsp_user`
--

INSERT INTO `hsp_user` (`usr_id`, `usr_password`, `usr_fname`, `usr_lname`, `usr_type`, `usr_email`, `usr_phone`, `usr_address`, `usr_status_medical_council`, `usr_medical_number`, `usr_dis_id`, `usr_status`, `usr_reset_pass_token`, `usr_reset_pass_date`, `usr_created_by`, `usr_modified_by`, `usr_created_date`, `usr_modified_date`, `usr_last_login`) VALUES
(9, '827ccb0eea8a706c4c34a16891f84e7b', 'Bhavin', 'Solanki', 'doctor', 'zalambb2009@gmail.com', '9601199220', 'A-42', '', '', '', 'y', NULL, NULL, NULL, NULL, NULL, '2016-01-18 17:06:07', '2016-01-18 22:36:07'),
(10, 'e41735e82f79de563136923bd72de508', 'Hospital', 'Admin', 'admin', 'kunalshah67@gmail.com', '9173038310', 'F-203', '', '', '', 'y', '10F8qWE2zojLeZ1FBETK', '2015-12-31 08:45:36', NULL, NULL, NULL, '2016-01-13 01:16:16', '2016-01-12 17:16:16'),
(11, '00603b22f6be793f89dc04d4072d0016', 'Utsav', '', 'doctor', 'utsavyshah12@gmail.com', NULL, NULL, 'Gujarat', 'R0011', 'a:1:{i:0;s:1:"3";}', 'n', NULL, NULL, NULL, NULL, NULL, '2016-01-18 16:46:16', '2016-01-07 21:31:42'),
(15, 'f925916e2754e5e03f75dd58a5733251', 'Bhavin', '', 'doctor', 'bsolanki.ce@gmail.com', NULL, NULL, 'bhavin', '12341234', 'a:1:{i:0;s:1:"3";}', 'n', NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL),
(16, '0bcf64566f2a7494c1c00805f941dd32', 'd', '', 'doctor', 'bsolankice@gmail.com', NULL, NULL, 'fdfdfdfdfd', 'cdfdfdd', 'a:1:{i:0;s:1:"4";}', 'n', NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
