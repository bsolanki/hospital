<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Hospital',
    'theme' => 'abound',
    'aliases' => array(
        'editable.assets.bootstrap-editable' => realpath(__DIR__ . '/../extensions/editable/assets/bootstrap-editable'), // change if necessary
        'editable' => 'application.extensions.editable',
    ),
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.library.*',
        'editable.*', //easy include of editable classes
        'application.modules.rights.*',
        'application.modules.rights.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'csm',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1', '192.168.1.38'),
        ),
        'rights' => array(
            'superuserName' => 'super_admin', // Name of the role with super user privileges. 
            'authenticatedName' => 'Authenticated', // Name of the authenticated user role. 
            'userClass' => 'User',
            'userIdColumn' => 'usr_id', // Name of the user id column in the database.
            'userNameColumn' => 'usr_email', // Name of the user name column in the database. 
            'enableBizRule' => true, // Whether to enable authorization item business rules. 
            'enableBizRuleData' => true, // Whether to enable data for business rules. 
//            'displayDescription' => true, // Whether to use item description instead of name. 
            'flashSuccessKey' => 'RightsSuccess', // Key to use for setting success flash messages. 
            'flashErrorKey' => 'RightsError', // Key to use for setting error flash messages. 
            'baseUrl' => '/rights', // Base URL for Rights. Change if module is nested. 
            'layout' => 'rights.views.layouts.main', // Layout to use for displaying Rights. 
            'appLayout' => 'application.views.layouts.main', // Application layout. 
//            'cssFile' => 'rights.css', // Style sheet file to use for Rights. 
            'install' => false, // Whether to enable installer. 
            'debug' => false,
        ),
        
    ),
    // application components
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'RWebUser',
            'loginUrl' => array('/site/login'),
            'returnUrl' => Yii::app()->defaultController
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'assignmentTable' => 'hsp_auth_assignment',
            'itemTable' => 'hsp_auth_item',
            'itemChildTable' => 'hsp_auth_item_child',
            'rightsTable' => 'hsp_rights',
            'connectionID' => 'db',
            'defaultRoles' => array('guest')
        ),
        // uncomment the following to enable URLs in path-format
         'urlManager' => array(
             'urlFormat' => 'path',
             'showScriptName' => false,
             'rules' => array(
                 '<controller:\w+>/<id:\d+>' => '<controller>/view',
                 '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                 '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
             ),
         ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        //X-editable config
        'editable' => array(
            'class' => 'application.extensions.editable.EditableConfig',
            'form' => 'bootstrap', //form style: 'bootstrap', 'jqueryui', 'plain' 
            'mode' => 'popup', //mode: 'popup' or 'inline'  
            'defaults' => array(//default settings for all editable elements
                'emptytext' => 'Click to set'
            )
        ),
        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Username' => 'csmsmtp@gmail.com',
            'Password' => 'lucifer@@12',
            'Mailer' => 'smtp',
            'Port' => 587,
            'SMTPSecure' => 'tls',
            'SMTPAuth' => true,
            'From' => 'csmsmtp@gmail.com',
            'FromName' => 'Hospital'
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'Copyright' => 'webmaster@example.com',
        'CONTACT_NUMBER_IN_EMAIL_TEMPLATE' => '1-844-472-4363',
        'COMPANY_NAME_IN_EMAIL_TEMPLATE' => 'Hospital Pro',
        'COMPANY_ADDRESS_IN_EMAIL_TEMPLATE' => 'COMPANY_ADDRESS_IN_EMAIL_TEMPLATE',
        'EMAIL_ADDRESS_IN_EMAIL_TEMPLATE' => 'EMAIL_ADDRESS_IN_EMAIL_TEMPLATE',
        'COMPANY_NAME_IN_EMAIL_TEMPLATE' => 'Hospital Pro',
        'PassResetExpiry' => '86400',
        'flasMsgTimeOut' => '3000'
    ),
);
