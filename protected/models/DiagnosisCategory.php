<?php

/**
 * This is the model class for table "{{diagnosis_category}}".
 *
 * The followings are the available columns in table '{{diagnosis_category}}':
 * @property integer $dia_catid
 * @property string $dia_catname
 * @property string $dia_catstatus
 */
class DiagnosisCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{diagnosis_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dia_catname', 'required'),
			array('dia_catname', 'unique'),
			array('dia_catname', 'length', 'max'=>255),
			array('dia_catstatus', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dia_catid, dia_catname, dia_catstatus', 'safe', 'on'=>'search'),
		);
	} 
	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dia_catid' => 'Id',
			'dia_catname' => 'Category Name',
			'dia_catstatus' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dia_catid',$this->dia_catid);
		$criteria->compare('dia_catname',$this->dia_catname,true);
		$criteria->compare('dia_catstatus',$this->dia_catstatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'dia_catid DESC',
            )
		));
	}


    public function displayStatus() {

        if ($this->dia_catstatus == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("DiagnosisCategory" , '.$this->dia_catid.')',
           'style' => "cursor:pointer;"
           )
       );

    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DiagnosisCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
