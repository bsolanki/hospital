<?php

/**
 * This is the model class for table "{{allergen}}".
 *
 * The followings are the available columns in table '{{allergen}}':
 * @property integer $alrg_id
 * @property string $alrg_name
 * @property string $alrg_status
 * @property integer $alrg_ref_id
 */
class Allergen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{allergen}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alrg_name, alrg_status', 'required'),
			array('alrg_name', 'unique'),
			array('alrg_ref_id', 'numerical', 'integerOnly'=>true),
			array('alrg_name', 'length', 'max'=>255),
			array('alrg_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('alrg_id, alrg_name, alrg_status, alrg_ref_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'alrg_id' => 'Allergen',
			'alrg_name' => 'Name',
			'alrg_status' => 'Status',
			'alrg_ref_id' => 'Reference Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('alrg_id',$this->alrg_id);
		$criteria->compare('alrg_name',$this->alrg_name,true);
		$criteria->compare('alrg_status',$this->alrg_status,true);
		$criteria->compare('alrg_ref_id',$this->alrg_ref_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
                'defaultOrder' => 'alrg_id DESC',
            )
		));
	}

	public function displayStatus() {

        if ($this->alrg_status == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

        return CHtml::ajaxLink($img, Yii::app()->createUrl('/allergen/changeStatus', array('id' => $this->alrg_id)), array(
                    'beforeSend' => 'function() {           
                               $("#allergen-grid").addClass("grid-view-loading");
                               var selection = confirm("Are you sure you want to change status?");
                                if (selection == true)      
                                    return true;
                                else{
                                        return false;
                                        $("#allergen-grid").removeClass("grid-view-loading");
                                } 
                            }',
                    'complete' => 'function() {
                              $("#allergen-grid").removeClass("grid-view-loading");
                              update_grid_view();
                            }',
                        ), array(
                    'href' => 'javascript:;'
                        )
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Allergen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
