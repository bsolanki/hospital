<?php

/**
 * This is the model class for table "{{brand}}".
 *
 * The followings are the available columns in table '{{brand}}':
 * @property integer $brnd_id
 * @property string $brnd_name
 * @property string $brnd_generic_details
 * @property integer $brnd_unit
 * @property string $brnd_dosage_form
 * @property string $brnd_volume
 * @property integer $brnd_company
 * @property string $brnd_status
 */
class Brand extends CActiveRecord {

    public $generic_name;
    public $strength;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{brand}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('brnd_name, brnd_dosage_form', 'required'),
            array('brnd_volume, brnd_company, brnd_notes' , 'safe'),
            array('brnd_unit, brnd_company', 'numerical', 'integerOnly' => true),
            array('brnd_name', 'length', 'max' => 255),
            array('generic_name, strength', 'checkgeneric'),
            array('brnd_dosage_form, brnd_volume', 'length', 'max' => 500),
            array('brnd_status', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('brnd_id, brnd_name, brnd_generic_details, brnd_unit, brnd_dosage_form, brnd_volume, brnd_company, brnd_status', 'safe', 'on' => 'search'),
        );
    }

    public function checkgeneric() {

        $generic = array();
        if (empty($this->generic_name) || empty($this->strength)) {

          //  $this->addError('generic_name', 'Generic Name and strength can not be blank.');
        } else {

            $generic[$this->generic_name] = $this->strength;

            $temp_generic = array();
            foreach ($_POST as $key => $value) {
                if (strpos($key, 'generic_name') === 0) {
                    $temp_generic[str_replace("generic_name", "", $key)]['generic_name'] = $value;
                }
                if (strpos($key, 'strength') === 0) {
                    $temp_generic[str_replace("strength", "", $key)]['strength'] = $value;
                }
            }

            if (!empty($temp_generic)) {
                foreach ($temp_generic as $key => $val) {
                    if (array_key_exists($val['generic_name'], $generic)) {
                        $this->addError('generic_name', 'Duplicate Generic Name not allowed.');
                        break;
                    } else {
                        $generic[$val['generic_name']] = $val['strength'];
                    }
                }
            }
        }
    }

    public function getGenericDetails($get) {
        $data = array();
        if (!empty($this->brnd_generic_details)) {
            $details = unserialize($this->brnd_generic_details);
            //echo print_r($details);exit;
            asort($details);
            //print_r($details);exit;

            foreach ($details as $key => $val) {

                if ($get) {
                    $data[] = $val;
                } else {
                    $generic = Generic::model()->findBypk($key);
                    $data[] = $generic['gn_name'];
                }
            }
        }
        if ($get) {
            $string  =  implode("/", $data);
            if(!empty($this->brnd_unit_id)){
                $string .= " (" . $this->brUnit->unt_name . ") ";
            }
            
            return $string ;
        } else {
            return implode(" + ", $data);
        }

//        return '-';
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'brDosage' => array(self::BELONGS_TO, 'Dosage', 'brnd_dosage_form'),
            'brCompany' => array(self::BELONGS_TO, 'Company', 'brnd_company'),
            'brUnit' => array(self::BELONGS_TO, 'Units', 'brnd_unit_id'),
        );
    }

    public function getDosage() {
        return CHtml::listData(Dosage::model()->findAllByAttributes(array('ds_status' => '1')), 'ds_id', 'ds_name');
    }

    public function getCompany() {
        return CHtml::listData(Company::model()->findAllByAttributes(array('cmp_status' => '1')), 'cmp_id', 'cmp_name');
    }

    public function getGeneric() {
        return CHtml::listData(Generic::model()->findAllByAttributes(array('gn_status' => '1')), 'gn_id', 'gn_name');
    }
    public function getUnit(){
        return CHtml::listData(Units::model()->findAllByAttributes(array('unt_status' => '1')), 'unt_id', 'unt_name');
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'brnd_id' => 'Brnd',
            'brnd_name' => 'Name',
            'generic_name' => 'Generic Details',
            'brnd_unit' => 'Unit',
            'brnd_dosage_form' => 'Dosage Form',
            'brnd_volume' => 'Volume',
            'brnd_company' => 'Company',
            'brnd_status' => 'Status',
            'brnd_notes' => 'Notes',
            'brnd_unit_id' => 'Unit',
            
        );
    }
    
    public function CalculateMaxDose($genericDetails){
        
        $flipped = array_flip($genericDetails);
        $genericDetails = implode(",", $flipped);
        
        $genericdata = Generic::model()->findAllByAttributes(array(), array('condition'=>'gn_id IN ('.$genericDetails.')'));
        
        foreach($genericdata as $max)
                $maxdose[] = $max->gn_max_dose_day;
        
        return $maxdose;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('brnd_id', $this->brnd_id);
        $criteria->compare('brnd_name', $this->brnd_name, true);
        $criteria->compare('brnd_generic_details', $this->brnd_generic_details, true);
        $criteria->compare('brnd_unit', $this->brnd_unit);
        $criteria->compare('brnd_dosage_form', $this->brnd_dosage_form, true);
        $criteria->compare('brnd_volume', $this->brnd_volume, true);
        $criteria->compare('brnd_company', $this->brnd_company);
        $criteria->compare('brnd_status', $this->brnd_status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'brnd_id DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Brand the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function displayStatus() {

        if ($this->brnd_status == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("brand" , '.$this->brnd_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
    }
}
