<?php

/**
 * This is the model class for table "{{class}}".
 *
 * The followings are the available columns in table '{{class}}':
 * @property integer $cls_id
 * @property string $cls_name
 * @property string $cls_status
 */
class Hclass extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{class}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cls_name', 'required'),
            array('cls_name', 'unique'),	
			array('cls_name', 'length', 'max'=>255),
			array('cls_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cls_id, cls_name, cls_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cls_id' => 'Cls',
			'cls_name' => 'Name',
			'cls_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cls_id',$this->cls_id);
		$criteria->compare('cls_name',$this->cls_name,true);
		$criteria->compare('cls_status',$this->cls_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'cls_id DESC',
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hclass the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function displayStatus() {

	    if ($this->cls_status == '1')
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
	    else
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("hclass" , '.$this->cls_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
	}

}
       
