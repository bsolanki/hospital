<?php

/**
 * This is the model class for table "{{frequency}}".
 *
 * The followings are the available columns in table '{{frequency}}':
 * @property integer $frqcy_id
 * @property string $frqcy_name
 * @property string $frqcy_status
 */
class Frequency extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{frequency}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('frqcy_name', 'required'),
                        array('frqcy_name', 'unique'),
			array('frqcy_name', 'length', 'max'=>255),
			array('frqcy_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('frqcy_id, frqcy_name, frqcy_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'frqcy_id' => 'Frqcy',
			'frqcy_name' => 'Name',
			'frqcy_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('frqcy_id',$this->frqcy_id);
		$criteria->compare('frqcy_name',$this->frqcy_name,true);
		$criteria->compare('frqcy_status',$this->frqcy_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'frqcy_id DESC',
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Frequency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function displayStatus() {

	    if ($this->frqcy_status == '1')
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
	    else
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("frequency" , '.$this->frqcy_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
	}

}
