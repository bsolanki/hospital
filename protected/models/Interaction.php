<?php

/**
 * This is the model class for table "{{interaction}}".
 *
 * The followings are the available columns in table '{{interaction}}':
 * @property integer $intr_id
 * @property integer $intr_generic_1
 * @property integer $intr_generic_2
 * @property string $intr_severity_of_interaction
 * @property string $intr_mechanisam
 * @property string $intr_commnets
 */
class Interaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{interaction}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('intr_generic_1, intr_generic_2, intr_severity_of_interaction, intr_mechanisam, intr_commnets', 'required'),
			array('intr_generic_1, intr_generic_2', 'numerical', 'integerOnly'=>true),
			array('intr_generic_1, intr_generic_2', 'checkgeneric'),
			array('intr_severity_of_interaction', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('intr_id, intr_generic_1, intr_generic_2, intr_severity_of_interaction, intr_mechanisam, intr_commnets', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'intr_id' => 'Intr',
			'intr_generic_1' => 'Generic 1',
			'intr_generic_2' => 'Generic 2',
			'intr_severity_of_interaction' => 'Severity Of Interaction',
			'intr_mechanisam' => 'Mechanisam',
			'intr_commnets' => 'Commnets',
		);
	}

	public function getGenericDetails($get) {       
	  if($get){
	      $val = $this->intr_generic_1;
	  }else{
	    $val = $this->intr_generic_2;
	  }
	  if (!empty($val)) {                                      
	    $generic = Generic::model()->findBypk($val);
	    return $generic['gn_name'];                            
	  }   
	}
	
	public function getGeneric() {
	    return CHtml::listData(Generic::model()->findAllByAttributes(array('gn_status' => '1')), 'gn_id', 'gn_name');
	}
	
	public function checkgeneric() {	
	  if (!empty($_POST['Interaction']['intr_generic_1']) && !empty($_POST['Interaction']['intr_generic_2'])) {		  
	    if($_POST['Interaction']['intr_generic_1'] == $_POST['Interaction']['intr_generic_2']){
	      $this->addError('intr_generic_2', 'Duplicate Generic Name not allowed.');
	    }		    
	  }
	}
      
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('intr_id',$this->intr_id);
		$criteria->compare('intr_generic_1',$this->intr_generic_1);
		$criteria->compare('intr_generic_2',$this->intr_generic_2);
		$criteria->compare('intr_severity_of_interaction',$this->intr_severity_of_interaction,true);
		$criteria->compare('intr_mechanisam',$this->intr_mechanisam,true);
		$criteria->compare('intr_commnets',$this->intr_commnets,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Interaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
