<?php

/**
 * This is the model class for table "{{vitals}}".
 *
 * The followings are the available columns in table '{{vitals}}':
 * @property integer $vi_id
 * @property integer $vi_patient_id
 * @property string $vi_blood_pressure_1
 * @property string $vi_heart_rate
 * @property string $vi_respiratory_rate
 * @property string $vi_spo2
 * @property string $vi_temperature
 * @property string $vi_height
 * @property string $vi_weight
 * @property string $vi_BMI
 * @property string $vi_peak_flow_rate
 * @property string $vi_notes
 * @property integer $vi_modify_by
 * @property string $vi_modify_date
 */
class Vitals extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vitals}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vi_blood_pressure_1', 'required'),
			array('vi_id, vi_patient_id, vi_modify_by', 'numerical', 'integerOnly'=>true),
			array('vi_blood_pressure_1, vi_heart_rate, vi_respiratory_rate, vi_peak_flow_rate', 'length', 'max'=>200),
			array('vi_spo2, vi_temperature, vi_height, vi_weight, vi_BMI, vi_notes', 'length', 'max'=>100),
                        array('vi_id, vi_patient_id, vi_heart_rate, vi_respiratory_rate, vi_spo2, vi_temperature, vi_height, vi_weight, vi_BMI, vi_peak_flow_rate, vi_notes, vi_modify_by, vi_modify_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vi_id, vi_patient_id, vi_blood_pressure_1, vi_heart_rate, vi_respiratory_rate, vi_spo2, vi_temperature, vi_height, vi_weight, vi_BMI, vi_peak_flow_rate, vi_notes, vi_modify_by, vi_modify_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vi_id' => 'Vi',
			'vi_patient_id' => 'Vi Patient',
			'vi_blood_pressure_1' => 'Blood Pressure (mmHg)',
			'vi_blood_pressure_2' => 'Blood Pressure (mmHg)',
			'vi_heart_rate' => 'Heart Rate (bpm)',
			'vi_respiratory_rate' => 'Respiratory Rate (breaths/minute)',
			'vi_spo2' => 'SpO2 (%)',
			'vi_temperature' => 'Temperature (°F)',
			'vi_height' => 'Height (ft in)',
			'vi_weight' => 'Weight (Kg)',
			'vi_BMI' => 'BMI',
			'vi_peak_flow_rate' => 'Peak Flow Rate (L/min or %)',
			'vi_notes' => 'Notes',
			'vi_modify_by' => 'Modify By',
			'vi_modify_date' => 'Modify Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('vi_id',$this->vi_id);
		$criteria->compare('vi_patient_id', Yii::app()->user->getState('rx_patient_id') , TRUE);
		$criteria->compare('vi_blood_pressure_1',$this->vi_blood_pressure_1,true);
		$criteria->compare('vi_heart_rate',$this->vi_heart_rate,true);
		$criteria->compare('vi_respiratory_rate',$this->vi_respiratory_rate,true);
		$criteria->compare('vi_spo2',$this->vi_spo2,true);
		$criteria->compare('vi_temperature',$this->vi_temperature,true);
		$criteria->compare('vi_height',$this->vi_height,true);
		$criteria->compare('vi_weight',$this->vi_weight,true);
		$criteria->compare('vi_BMI',$this->vi_BMI,true);
		$criteria->compare('vi_peak_flow_rate',$this->vi_peak_flow_rate,true);
		$criteria->compare('vi_notes',$this->vi_notes,true);
		$criteria->compare('vi_modify_by',$this->vi_modify_by);
		$criteria->compare('vi_modify_date',$this->vi_modify_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vitals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
