<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property string $usr_id
 * @property string $usr_password
 * @property string $usr_fname
 * @property string $usr_lname
 * @property string $usr_type
 * @property string $usr_email
 * @property string $usr_phone
 * @property string $usr_address
 * @property string $usr_status
 * @property string $usr_reset_pass_token
 * @property string $usr_reset_pass_date
 * @property string $usr_created_by
 * @property string $usr_modified_by
 * @property string $usr_created_date
 * @property string $usr_modified_date
 * @property string $usr_last_login
 * @property string $usr_dis_id
 * @property string $usr_medical_number
 * @property string $usr_status_medical_council
 */
class User extends CActiveRecord {

    public $confirm_password;
    public $usr_profile_pic;
    public $salutation;
    public $education;
    public $usr_email1;
    

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            array('usr_fname, usr_lname, usr_phone, usr_email', 'required', 'on' => 'editProfile'),
            array('usr_fname, usr_lname', 'ext.validators.ESpecialCharacterValidator' , 'on'=>'editprofile'),
            array('usr_email+usr_status', 'application.extensions.uniqueColumnValidator', 'on'=>'editProfile'),    

            
            array('usr_password, usr_fname, usr_email', 'required', 'on' => 'signup1'),
            array('usr_dis_id, usr_medical_number, usr_status_medical_council', 'required', 'on' => 'signup1'),
            array('usr_fname', 'checkfname', 'on' => 'signup1'),
            array('usr_email', 'email', 'on' => 'signup1'),
            array('usr_email', 'unique', 'on' => 'signup1'),
            array('usr_medical_number', 'unique', 'on' => 'signup1'),
            array('usr_password', 'ext.validators.EPasswordStrength', 'min' => 8, 'on' => 'signup1'),
            
            array('usr_fname', 'checkfname', 'on' => 'staff'),
            array('usr_email', 'email', 'on' => 'staff'),
            array('usr_email', 'unique', 'on' => 'staff'),
            array('usr_fname, usr_email, usr_type', 'required', 'on' => 'staff'),
            
            array('usr_fname', 'checkfname', 'on' => 'settings'),
            array('usr_email', 'email', 'on' => 'settings'),
            array('usr_email', 'unique', 'on' => 'settings'),
            array('usr_fname, usr_email, usr_type', 'required', 'on' => 'settings'),
            
            
            
            //setup
            array('usr_salutation, usr_fname, usr_education, usr_medical_number, usr_phone', 'required', 'on' => 'setup'),
            array('usr_medical_number', 'unique', 'on' => 'setup'),
            array('usr_lname', 'checknewuser', 'on' => 'setup'),
//            array('usr_profile_pic', 'on' => 'profilepic'),
//            array('usr_password, usr_fname, usr_lname, usr_email', 'required'),
//            array('usr_password', 'length', 'max' => 50),
//            array('usr_fname, usr_lname, usr_reset_pass_token', 'length', 'max' => 200),
//            array('usr_type', 'length', 'max' => 19),
//            array('usr_email', 'length', 'max' => 255),
//            array('usr_phone', 'length', 'max' => 30),
//            array('usr_status', 'length', 'max' => 1),
//            array('usr_created_by, usr_modified_by', 'length', 'max' => 11),
//            array('usr_address, usr_reset_pass_date, usr_created_date, usr_modified_date, usr_last_login', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('usr_id, usr_password, usr_fname, usr_lname, usr_type, usr_email, usr_phone, usr_address, usr_status, usr_reset_pass_token, usr_reset_pass_date, usr_created_by, usr_modified_by, usr_created_date, usr_modified_date, usr_last_login, usr_medical_number, usr_dis_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'usr_id' => 'Usr',
            'usr_password' => 'Password',
            'usr_fname' => 'Name',
            'usr_lname' => 'Name',
            'usr_type' => 'Type',
            'usr_email' => 'Email',
            'usr_phone' => 'Phone',
            'usr_address' => 'Address',
            'usr_status' => 'Status',
            'usr_reset_pass_token' => 'Reset Pass Token',
            'usr_reset_pass_date' => 'Reset Pass Date',
            'usr_created_by' => 'Usr Created By',
            'usr_modified_by' => 'Usr Modified By',
            'usr_created_date' => 'Usr Created Date',
            'usr_modified_date' => 'Usr Modified Date',
            'usr_last_login' => 'Usr Last Login',
            'usr_dis_id' => 'Discipline',
            'usr_medical_number' => 'Medical Registration Number',
            'usr_status_medical_council' => 'State Medical Council',
            'usr_profile_pic' => 'Profile',
            'usr_salutation' => 'Salutation',
            'usr_education' => 'Education',
            'usr_email1' => 'Email'
        );
    }
    
    public function checknewuser(){
        
        if(!empty($this->usr_lname) && !empty($this->usr_email)){
           
            
        }
        
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('usr_id', $this->usr_id, true);
        $criteria->compare('usr_password', $this->usr_password, true);
        if (!empty($this->usr_fname))
            $criteria->addCondition('concat_ws(" ",usr_fname,usr_lname) like "%' . $this->usr_fname . '%" ', 'AND');
        $criteria->compare('usr_medical_number', $this->usr_medical_number, true);
        $criteria->compare('usr_type', $this->usr_type, true);
        $criteria->compare('usr_email', $this->usr_email, true);
        $criteria->compare('usr_phone', $this->usr_phone, true);
        $criteria->compare('usr_address', $this->usr_address, true);
        $criteria->compare('usr_status', $this->usr_status, true);
        $criteria->compare('usr_dis_id', $this->usr_dis_id, true);
        $criteria->compare('usr_reset_pass_date', $this->usr_reset_pass_date, true);
        $criteria->compare('usr_created_by', $this->usr_created_by, true);
        $criteria->compare('usr_modified_by', $this->usr_modified_by, true);
        $criteria->compare('usr_created_date', $this->usr_created_date, true);
        $criteria->compare('usr_modified_date', $this->usr_modified_date, true);
        $criteria->compare('usr_last_login', $this->usr_last_login, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function staffsearch(){
        
        $criteria = new CDbCriteria;

        $criteria->compare('usr_id', $this->usr_id, true);
        $criteria->compare('usr_parent_id', Yii::app()->user->id , true);
        if (!empty($this->usr_fname))
            $criteria->addCondition('concat_ws(" ",usr_fname,usr_lname) like "%' . $this->usr_fname . '%" ', 'AND');
        $criteria->compare('usr_medical_number', $this->usr_medical_number, true);
        $criteria->compare('usr_type', $this->usr_type, true);
        $criteria->compare('usr_email', $this->usr_email, true);
        $criteria->compare('usr_phone', $this->usr_phone, true);
        $criteria->compare('usr_address', $this->usr_address, true);
        $criteria->compare('usr_status', $this->usr_status, true);
        $criteria->compare('usr_dis_id', $this->usr_dis_id, true);
        $criteria->compare('usr_reset_pass_date', $this->usr_reset_pass_date, true);
        $criteria->compare('usr_created_by', $this->usr_created_by, true);
        $criteria->compare('usr_modified_by', $this->usr_modified_by, true);
        $criteria->compare('usr_created_date', $this->usr_created_date, true);
        $criteria->compare('usr_modified_date', $this->usr_modified_date, true);
        $criteria->compare('usr_last_login', $this->usr_last_login, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getdiscipline() {
        return CHtml::listData(Discipline::model()->findAllByAttributes(array('dis_status' => '1')), 'dis_id', 'dis_name');
    }

    public function getgridDiscipline() {
        $name = array();
        if (!empty($this->usr_dis_id)) {
            $data = unserialize($this->usr_dis_id);

            $i = 0;
            foreach ($data as $val) {
                $details = Discipline::model()->findByPk($val);
                $name[$i] = $details->dis_name;
                $i++;
            }
        }

        return implode(",", $name);
    }

    public function checkPassword() {

        if (!empty($this->usr_password)) {

            if (!preg_match("/^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*+-]).\S*$/", $this->usr_password)) {
                $this->addError('usr_password', 'Password is weak. Password must contain at least 8 characters, at least one lower case letter, at least one upper case letter, at least one special character');
            }

            if (empty($this->confirm_password)) {
                $this->addError('confirm_password', 'Confrim Password can not be blank.');
            } else if ($this->usr_password != $this->confirm_password) {
                $this->addError('confirm_password', 'Retype Password must be repeated exactly.');
            }
        }
    }

    public function Checkphonenumber() {
        if (!empty($this->usr_phone)) {
            if (!preg_match("/^[0-9]+$/i", $this->usr_phone)) {
                $this->addError('usr_phone', 'Please enter integer value.');
            }
        }
    }

    public function checkfname() {
        if(!empty($this->usr_fname)){
            if (!preg_match("/^[a-z ]+$/i", $this->usr_fname)) {
                $this->addError('usr_fname', 'Please enter alphabetic value .');
            }
        }
        
    }

    public function checklname() {
        if(!empty($this->usr_lname)){
            if (!preg_match("/^[a-z ]+$/i", $this->usr_lname)) {
                $this->addError('usr_lname', 'Please enter alphabetic value .');
            }
        }
        
    }

    // hash password
    public function hashPassword($password) {
        return md5($password);
    }

    // password validation
    public function validatePassword($password) {
        return $this->hashPassword($password) === $this->usr_password;
    }

    public function getclass() {
        return CHtml::listData(HClass::model()->findAllByAttributes(array('cls_status' => '1')), 'cls_id', 'cls_name');
    }

    public function displayStatus() {

        if ($this->usr_status == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("User" , '.$this->usr_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
   }
}