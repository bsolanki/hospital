<?php

/**
 * This is the model class for table "{{complain}}".
 *
 * The followings are the available columns in table '{{complain}}':
 * @property integer $cmp_id
 * @property integer $cmp_cat_id
 * @property string $cmp_name
 * @property string $cmp_status
 *
 * The followings are the available model relations:
 * @property ComplainCategory $cmpCat
 */
class Complain extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{complain}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cmp_cat_id, cmp_name', 'required'),
                        array('cmp_name', 'unique'),
			array('cmp_cat_id', 'numerical', 'integerOnly'=>true),
			array('cmp_name', 'length', 'max'=>255),
			array('cmp_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cmp_id, cmp_cat_id, cmp_name, cmp_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cmpCat' => array(self::BELONGS_TO, 'ComplainCategory', 'cmp_cat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cmp_id' => 'Id',
			'cmp_cat_id' => 'Category',
			'cmp_name' => 'Name',
			'cmp_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cmp_id',$this->cmp_id);
		$criteria->compare('cmp_cat_id',$this->cmp_cat_id);
		$criteria->compare('cmp_name',$this->cmp_name,true);
		$criteria->compare('cmp_status',$this->cmp_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getComplainCategory(){
        return CHtml::listData(ComplainCategory::model()->findAllByAttributes(array('cmpln_catstatus' => '1')), 'cmpln_catid', 'cmpln_catname');
    }

    public function displayStatus() {

        if ($this->cmp_status == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("Complain" , '.$this->cmp_id.')',
           'style' => "cursor:pointer;"
           )
       );
    }
    
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Complain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
