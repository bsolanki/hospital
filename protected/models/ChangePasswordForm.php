<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ChangePasswordForm extends CFormModel
{
	public $old_password;
	public $new_password;
        public $confirm_password;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
                    // username and password are required
                    array('old_password, new_password, confirm_password', 'required'),
                    //array('old_password, new_password, confirm_password', 'length', 'min'=>8, 'max'=>16),
                    array('new_password','ext.validators.EPasswordStrength', 'min'=>8),
                    array('confirm_password', 'compare', 'compareAttribute'=>'new_password'),
                    // password needs to be authenticated
                    array('old_password, new_password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
                    'old_password'=>'Current Password',
                    'new_password'=>'New Password',
                    'confirm_password'=>'Confirm New Password',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{            
            $user_model = User::model()->findByPk(Yii::app()->user->getState('usr_id'));

            if(User::model()->hashPassword($this->old_password)===$user_model->usr_password)
            {
                if(User::model()->hashPassword($this->new_password)===$user_model->usr_password)
                {
                    $this->addError('new_password',Yii::t('app','New password can not be same as current password.'));
                }
            }
            else
            {
                $this->addError('old_password',Yii::t('app','Incorrect Current Password.'));
            }
                        
	}

}
