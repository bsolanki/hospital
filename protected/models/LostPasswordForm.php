<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LostPasswordForm extends CFormModel
{
	public $username;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
                    // username and password are required
                    array('username', 'required'),
                    array('username', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
                    'username'=>'E-mail',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{            
                
            $user = User::model()->findByAttributes(array('usr_email'=>trim($this->username)));

            if($user===null) {
                $this->addError('username',Yii::t('app','Invalid e-mail.'));
            }
                                            
	}

}
