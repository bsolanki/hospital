<?php

/**
 * This is the model class for table "{{rx}}".
 *
 * The followings are the available columns in table '{{rx}}':
 * @property integer $rx_id
 * @property integer $rx_brand_id
 * @property string $rx_dose
 * @property string $rx_frequency
 * @property string $rx_instruction
 * @property string $rx_duration
 * @property string $rx_quantity
 * @property string $rx_condition
 * @property string $rx_note
 * @property string $rx_preview
 * @property string $rx_refill
 * @property string $rx_refill_date
 * @property string $rx_start_date
 * @property string $rx_end_date
 * @property string $rx_therapy
 */
class Rx extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $rx_unit;
        public $rx_patient_id;
        public $comment;
        public function tableName()
	{
		return '{{rx}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rx_brand_id, rx_dose, rx_frequency, rx_instruction, rx_duration, rx_quantity', 'required'),
			array('rx_brand_id', 'numerical', 'integerOnly'=>true),
                        array('rx_condition, rx_note, rx_preview, rx_refill, rx_refill_date, rx_start_date, rx_end_date, rx_therapy, rx_unit, rx_patient_id' ,'safe'),
			array('rx_dose, rx_frequency, rx_instruction, rx_duration, rx_quantity, rx_condition, rx_therapy', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rx_id, rx_brand_id, rx_dose, rx_frequency, rx_instruction, rx_duration, rx_quantity, rx_condition, rx_note, rx_preview, rx_refill, rx_refill_date, rx_start_date, rx_end_date, rx_therapy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'brrx' => array(self::BELONGS_TO, 'Brand', 'rx_brand_id'),
                    'brfre' => array(self::BELONGS_TO, 'Frequency', 'rx_frequency'),
                    'brins' => array(self::BELONGS_TO, 'Instruction', 'rx_instruction'),
		);
	}
        
        public function getbrand(){
            
            return CHtml::listData(Brand::model()->findAll(), 'brnd_id', 'brnd_name');
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rx_id' => 'Rx',
			'rx_brand_id' => 'Brand Name',
			'rx_dose' => 'Dose',
                        'rx_unit' => 'Unit',
			'rx_frequency' => 'Frequency',
			'rx_instruction' => 'Instruction',
			'rx_duration' => 'Duration',
			'rx_quantity' => 'Quantity',
			'rx_condition' => 'Condition',
			'rx_note' => 'Note',
			'rx_preview' => 'Preview',
			'rx_refill' => 'Refill',
			'rx_refill_date' => 'Refill Date',
			'rx_start_date' => 'Start Date',
			'rx_end_date' => 'End Date',
			'rx_therapy' => 'Therapy',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rx_id',$this->rx_id);
		$criteria->compare('rx_brand_id',$this->rx_brand_id);
		$criteria->compare('rx_dose',$this->rx_dose,true);
		$criteria->compare('rx_frequency',$this->rx_frequency,true);
		$criteria->compare('rx_instruction',$this->rx_instruction,true);
		$criteria->compare('rx_duration',$this->rx_duration,true);
		$criteria->compare('rx_quantity',$this->rx_quantity,true);
		$criteria->compare('rx_condition',$this->rx_condition,true);
		$criteria->compare('rx_note',$this->rx_note,true);
		$criteria->compare('rx_preview',$this->rx_preview,true);
		$criteria->compare('rx_refill',$this->rx_refill,true);
		$criteria->compare('rx_refill_date',$this->rx_refill_date,true);
		$criteria->compare('rx_start_date',$this->rx_start_date,true);
		$criteria->compare('rx_end_date',$this->rx_end_date,true);
		$criteria->compare('rx_therapy',$this->rx_therapy,true);
		$criteria->compare('rx_patient_id', Yii::app()->user->getState('rx_patient_id') ,true);

                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rx the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
