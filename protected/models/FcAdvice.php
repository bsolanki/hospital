<?php

/**
 * This is the model class for table "{{fc_advice}}".
 *
 * The followings are the available columns in table '{{fc_advice}}':
 * @property integer $af_id
 * @property integer $af_cat_id
 * @property integer $af_adv_id
 * @property string $af_from
 * @property string $af_to
 * @property integer $af_patient_id
 * @property integer $af_facility_id
 * @property string $af_modify_by
 */
class FcAdvice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fc_advice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('af_adv_id, af_from, af_to', 'required'),
			array('af_cat_id, af_patient_id, af_facility_id', 'numerical', 'integerOnly'=>true),
			array('af_from, af_to', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('af_id, af_cat_id, af_adv_id, af_from, af_to, af_patient_id, af_facility_id, af_modify_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'FcadviceToadvice' => array(self::BELONGS_TO, 'Advice', 'af_adv_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'af_id' => 'Af',
			'af_cat_id' => 'Advice Category',
			'af_adv_id' => 'Advice',
			'af_from' => 'From',
			'af_to' => 'To',
			'af_patient_id' => 'Patient',
			'af_facility_id' => 'Facility',
			'af_modify_by' => 'Modify By',
		);
	}
        
        public function fetchadvice($cat_id = NULL){
            
            if(empty($cat_id) || $cat_id == 0){
                return CHtml::listData(Advice::model()->findAllByAttributes(array() , 'adv_fc_id IN (0 , '.Yii::app()->user->getState('facility').')'), 'adv_id', 'adv_desc');
            }else{
                return CHtml::listData(Advice::model()->findAllByAttributes(array('adv_cat_id'=> $cat_id) , 'adv_fc_id IN (0 , '.Yii::app()->user->getState('facility').')'), 'adv_id', 'adv_desc');
            }
            
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('af_id',$this->af_id);
		$criteria->compare('af_cat_id',$this->af_cat_id);
		$criteria->compare('af_adv_id',$this->af_adv_id);
		$criteria->compare('af_from',$this->af_from,true);
		$criteria->compare('af_to',$this->af_to,true);
		$criteria->compare('af_patient_id',$this->af_patient_id);
		$criteria->compare('af_facility_id',$this->af_facility_id);
		$criteria->compare('af_modify_by',$this->af_modify_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FcAdvice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
