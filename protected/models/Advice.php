<?php

/**
 * This is the model class for table "{{advice}}".
 *
 * The followings are the available columns in table '{{advice}}':
 * @property integer $adv_id
 * @property integer $adv_cat_id
 * @property string $adv_desc
 * @property string $adv_status
 */
class Advice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{advice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adv_cat_id, adv_desc', 'required'),
			array('adv_cat_id', 'numerical', 'integerOnly'=>true),
			array('adv_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('adv_id, adv_cat_id, adv_desc, adv_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adCat' => array(self::BELONGS_TO, 'AdviceCategory', 'adv_cat_id'),
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'adv_id' => 'Adv',
			'adv_cat_id' => 'Category',
			'adv_desc' => 'Description',
			'adv_status' => 'Status',
		);
	}

    public function getAdviceCategory(){
        return CHtml::listData(AdviceCategory::model()->findAllByAttributes(array('ad_catstatus' => '1')), 'ad_cat', 'ad_catname');
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('adv_id',$this->adv_id);
		$criteria->compare('adv_cat_id',$this->adv_cat_id);
		$criteria->compare('adv_desc',$this->adv_desc,true);
		$criteria->compare('adv_status',$this->adv_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'adv_id DESC',
            )
		));
	}

    public function displayStatus() {

        if ($this->adv_status == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("Advice" , '.$this->adv_id.')',
           'style' => "cursor:pointer;"
           )
       );

    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Advice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
