<?php

/**
 * This is the model class for table "{{facility}}".
 *
 * The followings are the available columns in table '{{facility}}':
 * @property integer $fac_id
 * @property integer $fac_usr_id
 * @property string $fac_name
 * @property string $fac_address
 * @property string $fac_city
 * @property string $fac_state
 * @property string $fac_zip_code
 * @property string $fac_country
 * @property string $fac_phone
 * @property string $fac_fax
 * @property string $fac_email
 * @property string $fac_website
 * @property string $fac_to_timing
 * @property string $fac_from_timing
 * @property string $fac_status
 */
class Facility extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{facility}}';
	}

        /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('fac_usr_id, fac_name, fac_address, fac_city, fac_state, fac_zip_code, fac_country, fac_phone, fac_fax, fac_email, fac_website, fac_to_timing, fac_from_timing, fac_status', 'required'),
//			array('fac_usr_id', 'numerical', 'integerOnly'=>true),
//			array('fac_name, fac_city, fac_state, fac_country, fac_website', 'length', 'max'=>200),
//			array('fac_zip_code', 'length', 'max'=>10),
//			array('fac_phone, fac_fax', 'length', 'max'=>20),
//			array('fac_email, fac_to_timing, fac_from_timing', 'length', 'max'=>100),
//			array('fac_status', 'length', 'max'=>1),
//			
//			Create Facility rules
			array('fac_name', 'required' , 'on'=>'create'),
                        array('fac_name', 'checkname' ,'on'=>'create'),
                    
                        //Setup Facility
                        array('fac_name, fac_address, fac_city, fac_state, fac_zip_code, fac_country', 'required' , 'on'=>'setup'),
                        array('fac_name', 'unique' ,'on'=>'setup'),
                        
                        //Settings Facility
                        array('fac_name, fac_address, fac_city, fac_state, fac_zip_code, fac_country', 'required' , 'on'=>'settings'),
                        array('fac_phone, fac_fax, fac_website, fac_to_timing, fac_from_timing', 'safe' , 'on'=>'settings'),
                        array('fac_name', 'unique' ,'on'=>'settings'),
                    
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('fac_id, fac_usr_id, fac_name, fac_address, fac_city, fac_state, fac_zip_code, fac_country, fac_phone, fac_fax, fac_email, fac_website, fac_to_timing, fac_from_timing, fac_status', 'safe', 'on'=>'search'),
		);
	}
        
        public function checkname(){
            if(!empty($this->fac_name)){
                $count = $this->countByAttributes(array('fac_name'=> $this->fac_name , 'fac_usr_id'=>  Yii::app()->user->id));
                
                if($count != 0){
                    $this->addError('fac_name', 'Name "'.$this->fac_name.'" has already been taken.');
                }
            }
        }
        


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}



    public function getCountryState(){
        return CHtml::listData(States::model()->findAllByAttributes(array('countryID'=>'IND')), 'stateID', 'stateName');
    }
 
    public function getStateCity($state_id = NULL){
    	if(empty($state_id)){
			return CHtml::listData(Cities::model()->findAllByAttributes(array()), 'cityID', 'cityName');
    	}
    	else{
    		return CHtml::listData(Cities::model()->findAllByAttributes(array('stateID'=>$state_id)), 'cityID', 'cityName');	
    	}
        
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fac_id' => 'Fac',
			'fac_usr_id' => 'Fac Usr',
			'fac_name' => 'Name',
			'fac_address' => 'Address',
			'fac_city' => 'City',
			'fac_state' => 'State',
			'fac_zip_code' => 'Zip Code',
			'fac_country' => 'Country',
			'fac_phone' => 'Phone',
			'fac_fax' => 'Fax',
			'fac_email' => 'Email',
			'fac_website' => 'Website',
			'fac_to_timing' => 'To Timing',
			'fac_from_timing' => 'From Timing',
			'fac_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fac_id',$this->fac_id);
		$criteria->compare('fac_usr_id',  Yii::app()->user->id);
		$criteria->compare('fac_name',$this->fac_name,true);
		$criteria->compare('fac_address',$this->fac_address,true);
		$criteria->compare('fac_city',$this->fac_city,true);
		$criteria->compare('fac_state',$this->fac_state,true);
		$criteria->compare('fac_zip_code',$this->fac_zip_code,true);
		$criteria->compare('fac_country',$this->fac_country,true);
		$criteria->compare('fac_phone',$this->fac_phone,true);
		$criteria->compare('fac_fax',$this->fac_fax,true);
		$criteria->compare('fac_email',$this->fac_email,true);
		$criteria->compare('fac_website',$this->fac_website,true);
		$criteria->compare('fac_to_timing',$this->fac_to_timing,true);
		$criteria->compare('fac_from_timing',$this->fac_from_timing,true);
		$criteria->compare('fac_status',$this->fac_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Facility the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        

    public function displayStatus() {

        if ($this->fac_status == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("Facility" , '.$this->fac_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
           }
}
