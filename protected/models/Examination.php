<?php

/**
 * This is the model class for table "{{examination}}".
 *
 * The followings are the available columns in table '{{examination}}':
 * @property integer $exam_id
 * @property string $exam_name
 * @property string $exam_status
 */
class Examination extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{examination}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('exam_name', 'required'),
             array('exam_name', 'unique'),
			array('exam_name', 'length', 'max'=>255),
			array('exam_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('exam_id, exam_name, exam_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exam_id' => 'Exam',
			'exam_name' => 'Name',
			'exam_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('exam_id',$this->exam_id);
		$criteria->compare('exam_name',$this->exam_name,true);
		$criteria->compare('exam_status',$this->exam_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'exam_id DESC',
            )
		));
	}


    public function displayStatus() {

        if ($this->exam_status == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("Examination" , '.$this->exam_id.')',
           'style' => "cursor:pointer;"
           )
       );

    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Examination the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
