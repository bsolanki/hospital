<?php

/**
 * This is the model class for table "{{fc_instruction}}".
 *
 * The followings are the available columns in table '{{fc_instruction}}':
 * @property integer $hfi_id
 * @property integer $hfi_instruction_id
 * @property integer $hfi_fc_id
 */
class FcInstruction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fc_instruction}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hfi_instruction_id, hfi_fc_id', 'required'),
			array('hfi_instruction_id, hfi_fc_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('hfi_id, hfi_instruction_id, hfi_fc_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'hfi_id' => 'Hfi',
			'hfi_instruction_id' => 'Hfi Instruction',
			'hfi_fc_id' => 'Hfi Fc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('hfi_id',$this->hfi_id);
		$criteria->compare('hfi_instruction_id',$this->hfi_instruction_id);
		$criteria->compare('hfi_fc_id',$this->hfi_fc_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FcInstruction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
