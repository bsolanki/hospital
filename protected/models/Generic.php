<?php

/**
 * This is the model class for table "{{generic}}".
 *
 * The followings are the available columns in table '{{generic}}':
 * @property integer $gn_id
 * @property string $gn_name
 * @property string $gn_status
 * @property integer $gn_class
 * @property string $gn_diesease
 * @property integer $gn_max_dose_day
 * @property integer $gn_unit_id
 * @property string $gn_notes
 *
 * The followings are the available model relations:
 * @property Class $gnClass
 */
class Generic extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{generic}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gn_name,gn_unit_id', 'required'),
			array('gn_name', 'unique'),
			array('gn_class, gn_max_dose_day, gn_unit_id', 'numerical', 'integerOnly'=>true),
			array('gn_name', 'length', 'max'=>255),
			array('gn_status', 'length', 'max'=>1),
			array('gn_notes,gn_diesease', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('gn_id, gn_name, gn_status, gn_class, gn_diesease, gn_max_dose_day, gn_unit_id, gn_notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gnClass' => array(self::BELONGS_TO, 'HClass', 'gn_class'),
			'gnUnit' => array(self::BELONGS_TO, 'Units', 'gn_unit_id'),
		);
	}
        
        public function getConcatened(){
            
            return $this->gn_name.' ( '.$this->gnUnit->unt_name . ' ) ';
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'gn_id' => 'Gn',
			'gn_name' => 'Name',
			'gn_status' => 'Status',
			'gn_class' => 'Class',
			'gn_diesease' => 'Diesease',
			'gn_max_dose_day' => 'Max Dose Day',
			'gn_unit_id' => 'Unit',
			'gn_notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('gn_id',$this->gn_id);
		$criteria->compare('gn_name',$this->gn_name,true);
		$criteria->compare('gn_status',$this->gn_status,true);
		$criteria->compare('gn_class',$this->gn_class);
		$criteria->compare('gn_diesease',$this->gn_diesease,true);
		$criteria->compare('gn_max_dose_day',$this->gn_max_dose_day);
		$criteria->compare('gn_unit_id',$this->gn_unit_id);
		$criteria->compare('gn_notes',$this->gn_notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'gn_id DESC',
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Generic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getclass(){
        return CHtml::listData(Hclass::model()->findAllByAttributes(array('cls_status' => '1')), 'cls_id', 'cls_name');
    }

    public function getunit(){
        return CHtml::listData(Units::model()->findAllByAttributes(array('unt_status' => '1')), 'unt_id', 'unt_name');
    }

    public function getdiesease(){
        return CHtml::listData(Disease::model()->findAllByAttributes(array('dis_status' => '1')), 'dis_id', 'dis_name');
    }

    public function getdieseasebyid(){
    	 
       
    	if(!empty($this->gn_diesease) && $this->gn_diesease!=NULL){
    		$this->gn_diesease = implode(',', unserialize($this->gn_diesease));

	        $q = CHtml::listData(Disease::model()->findAllByAttributes(array() , array('condition' => 'dis_id IN ('. $this->gn_diesease . ')')), 'dis_id', 'dis_name');
	    	
	    //	print_r($q); exit;
	    	return implode(",", $q);
    	}
    	else return "-";
    }
	 

    public function displayStatus() {

	    if ($this->gn_status == '1')
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
	    else
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("generic" , '.$this->gn_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
	}

}