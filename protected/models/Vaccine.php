<?php

/**
 * This is the model class for table "{{vaccine}}".
 *
 * The followings are the available columns in table '{{vaccine}}':
 * @property integer $vc_id
 * @property string $vc_name
 * @property string $vc_brand_id
 * @property string $vc_status
 */
class Vaccine extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vaccine}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vc_name, vc_brand_id', 'required'),
            array('vc_name', 'unique'),
			array('vc_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vc_id, vc_name, vc_brand_id, vc_status', 'safe', 'on'=>'search'),
		);
	}
        
        public function getcontcate(){
            return $this->vc_name;
        }

	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brndVacine' => array(self::BELONGS_TO, 'Brand', 'vc_brand_id')
		);
	}
        

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vc_id' => 'Vc',
			'vc_name' => 'Name',
			'vc_brand_id' => 'Brand',
			'vc_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('vc_id',$this->vc_id);
		$criteria->compare('vc_name',$this->vc_name,true);
		$criteria->compare('vc_brand_id',$this->vc_brand_id,true);
		$criteria->compare('vc_status',$this->vc_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getbrand(){
        return CHtml::listData(Brand::model()->findAllByAttributes(array('brnd_status' => '1')), 'brnd_id', 'brnd_name');
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vaccine the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



    public function displayStatus() {

	    if ($this->vc_status == '1')
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
	    else
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("vaccine" , '.$this->vc_id.')',
                       'style' => "cursor:pointer;"
                       )
       );
	}

}