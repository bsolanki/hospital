<?php

/**
 * This is the model class for table "{{diagnosis}}".
 *
 * The followings are the available columns in table '{{diagnosis}}':
 * @property integer $diag_id
 * @property string $diag_name
 * @property string $diag_status
 */
class Diagnosis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{diagnosis}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('diag_name', 'required'),
             array('diag_name', 'unique'),
			array('diag_name', 'length', 'max'=>255),
			array('diag_catid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('diag_id, diag_name,diag_catid, diag_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cmpCat' => array(self::BELONGS_TO, 'DiagnosisCategory', 'diag_catid'),
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'diag_id' => 'Diag',
			'diag_catid' => 'Category',
			'diag_name' => 'Name',
			'diag_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('diag_id',$this->diag_id);
		$criteria->compare('diag_catid',$this->diag_catid);
		$criteria->compare('diag_name',$this->diag_name,true);
		$criteria->compare('diag_status',$this->diag_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'diag_id DESC',
            )
		));
	}


    public function getDiagnosisCategory(){
        return CHtml::listData(DiagnosisCategory::model()->findAllByAttributes(array('dia_catstatus' => '1')), 'dia_catid', 'dia_catname');
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Diagnosis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function displayStatus() {

        if ($this->diag_status == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("Diagnosis" , '.$this->diag_id.')',
           'style' => "cursor:pointer;"
           )
       );

    }



}
