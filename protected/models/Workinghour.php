<?php

/**
 * This is the model class for table "{{workinghour}}".
 *
 * The followings are the available columns in table '{{workinghour}}':
 * @property integer $wh_id
 * @property integer $wh_facility_id
 * @property string $wh_facility_data
 */
class Workinghour extends CActiveRecord
{
    
        public $weekday;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{workinghour}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wh_facility_id, wh_facility_data', 'required'),
			array('wh_facility_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wh_id, wh_facility_id, wh_facility_data', 'safe', 'on'=>'search'),
		);
	}
        
        public function gettime(){
            
            $times = array();
            for ($h = 0; $h < 24; $h++){
              for ($m = 0; $m < 60 ; $m += 10){
                $time = sprintf('%02d:%02d', $h, $m);
                $times["$time"] = "$time";
              }
            }
            
            return $times;
            
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wh_id' => 'Wh',
			'wh_facility_id' => 'Wh Facility',
			'wh_facility_data' => 'Wh Facility Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wh_id',$this->wh_id);
		$criteria->compare('wh_facility_id',$this->wh_facility_id);
		$criteria->compare('wh_facility_data',$this->wh_facility_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Workinghour the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
