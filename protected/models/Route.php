<?php

/**
 * This is the model class for table "{{route}}".
 *
 * The followings are the available columns in table '{{route}}':
 * @property integer $route_id
 * @property string $route_name
 * @property string $route_status
 */
class Route extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{route}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('route_name', 'required'),
                        array('route_name', 'unique'),
			array('route_name', 'length', 'max'=>255),
			array('route_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('route_id, route_name, route_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'route_id' => 'Route',
			'route_name' => 'Name',
			'route_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('route_name',$this->route_name,true);
		$criteria->compare('route_status',$this->route_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'route_id DESC',
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Route the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function displayStatus() {

        if ($this->route_status == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


        return CHtml::ajaxLink($img, Yii::app()->createUrl('/route/changeStatus', array('id' => $this->route_id)), array(
                    'beforeSend' => 'function() {           
                               $("#route-grid").addClass("grid-view-loading");
                               var selection = confirm("Are you sure you want to change status?");
                                if (selection == true)      
                                    return true;
                                else{
                                        return false;
                                        $("#route-grid").removeClass("grid-view-loading");
                                } 
                            }',
                    'complete' => 'function() {
                              $("#route-grid").removeClass("grid-view-loading");
                              update_grid_view();
                            }',
                        ), array(
                    'href' => 'javascript:;'
                        )
        );
    }
}
