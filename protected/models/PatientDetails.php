<?php

/**
 * This is the model class for table "{{patient_details}}".
 *
 * The followings are the available columns in table '{{patient_details}}':
 * @property integer $pd_id
 * @property string $pd_summary
 * @property string $pd_complains
 * @property string $pd_procedure
 * @property string $pd_preciption
 * @property string $pd_vaccine
 * @property string $pd_mar
 * @property string $pd_advice
 * @property string $pd_labs
 * @property string $pd_vitals
 */
class PatientDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{patient_details}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pd_summary, pd_complains, pd_procedure, pd_preciption, pd_vaccine, pd_mar, pd_advice, pd_labs, pd_vitals', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pd_id, pd_summary, pd_complains, pd_procedure, pd_preciption, pd_vaccine, pd_mar, pd_advice, pd_labs, pd_vitals', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pd_id' => 'Pd',
			'pd_summary' => 'Pd Summary',
			'pd_complains' => 'Pd Complains',
			'pd_procedure' => 'Pd Procedure',
			'pd_preciption' => 'Pd Preciption',
			'pd_vaccine' => 'Pd Vaccine',
			'pd_mar' => 'Pd Mar',
			'pd_advice' => 'Pd Advice',
			'pd_labs' => 'Pd Labs',
			'pd_vitals' => 'Pd Vitals',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pd_id',$this->pd_id);
		$criteria->compare('pd_summary',$this->pd_summary,true);
		$criteria->compare('pd_complains',$this->pd_complains,true);
		$criteria->compare('pd_procedure',$this->pd_procedure,true);
		$criteria->compare('pd_preciption',$this->pd_preciption,true);
		$criteria->compare('pd_vaccine',$this->pd_vaccine,true);
		$criteria->compare('pd_mar',$this->pd_mar,true);
		$criteria->compare('pd_advice',$this->pd_advice,true);
		$criteria->compare('pd_labs',$this->pd_labs,true);
		$criteria->compare('pd_vitals',$this->pd_vitals,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getcomplains($id = NULL){
            
          if(!empty($id))
               $condition =  array('condition'=>'cmp_id IN ('.  implode(",", $id).')');
          else
               $condition = ' ';
          return CHtml::listData(Complain::model()->findAllByAttributes(array() , $condition ), 'cmp_id' , 'cmp_name');
        }
        
        public function getvaccine(){
          return CHtml::listData(Vaccine::model()->findAll(), 'vc_id' , 'contcate');  
        }
        
        public function getallergen(){
          return CHtml::listData(Allergen::model()->findAll(), 'alrg_id' , 'alrg_name');  
        }

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PatientDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
