<?php

/**
 * This is the model class for table "{{generic}}".
 *
 * The followings are the available columns in table '{{generic}}':
 * @property integer $gn_id
 * @property string $gn_name
 * @property string $gn_status
 */
class Generic extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{generic}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gn_name', 'required'),
                    
                        array('gn_name', 'unique'),
			array('gn_id', 'numerical', 'integerOnly'=>true),
			array('gn_name', 'length', 'max'=>255),
			array('gn_status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('gn_id, gn_name, gn_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'gn_id' => 'Gn',
			'gn_name' => 'Name',
			'gn_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('gn_id',$this->gn_id);
		$criteria->compare('gn_name',$this->gn_name,true);
		$criteria->compare('gn_status',$this->gn_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Generic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function displayStatus() {

        if ($this->gn_status == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


        return CHtml::ajaxLink($img, Yii::app()->createUrl('/generic/changeStatus', array('id' => $this->gn_id)), array(
                    'beforeSend' => 'function() {           
                               $("#generic-grid").addClass("grid-view-loading");
                               var selection = confirm("Are you sure you want to change status?");
                                if (selection == true)      
                                    return true;
                                else{
                                        return false;
                                        $("#generic-grid").removeClass("grid-view-loading");
                                } 
                            }',
                    'complete' => 'function() {
                              $("#generic-grid").removeClass("grid-view-loading");
                              update_grid_view();
                            }',
                        ), array(
                    'href' => 'javascript:;'
                        )
        );
    }
    
    public function concatened(){
        
    }
}
