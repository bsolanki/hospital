<?php

/**
 * This is the model class for table "{{advice_category}}".
 *
 * The followings are the available columns in table '{{advice_category}}':
 * @property integer $ad_cat
 * @property string $ad_catname
 * @property string $ad_catstatus
 */
class AdviceCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{advice_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ad_catname', 'required'),
			array('ad_catname', 'unique'),
			array('ad_catname', 'length', 'max'=>255),
			array('ad_catstatus', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ad_cat, ad_catname, ad_catstatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ad_cat' => 'Id',
			'ad_catname' => 'Name',
			'ad_catstatus' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ad_cat',$this->ad_cat);
		$criteria->compare('ad_catname',$this->ad_catname,true);
		$criteria->compare('ad_catstatus',$this->ad_catstatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'ad_cat DESC',
            )
		));
	} 
	
    public function displayStatus() {

        if ($this->ad_catstatus == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("AdviceCategory" , '.$this->ad_cat.')',
           'style' => "cursor:pointer;"
           )
       );

    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdviceCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
