<?php

/**
 * This is the model class for table "{{units}}".
 *
 * The followings are the available columns in table '{{units}}':
 * @property integer $unt_id
 * @property string $unt_name
 * @property string $unt_status
 * @property string $unt_calculate
 */
class Units extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{units}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unt_name', 'required'),
			array('unt_name', 'unique'),
			array('unt_name', 'length', 'max'=>255),
			array('unt_status, unt_calculate', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('unt_id, unt_name, unt_status, unt_calculate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'unt_id' => 'Unt',
			'unt_name' => 'Name',
			'unt_status' => 'Status',
			'unt_calculate' => 'Calculate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('unt_id',$this->unt_id);
		$criteria->compare('unt_name',$this->unt_name,true);
		$criteria->compare('unt_status',$this->unt_status,true);
		$criteria->compare('unt_calculate',$this->unt_calculate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
                'defaultOrder' => 'unt_id DESC',
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Units the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function displayStatus() {

	    if ($this->unt_status == '1')
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
	    else
	        $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("units" , '.$this->unt_id.')',
                       'style' => "cursor:pointer;"
                       )
       );


	}
}
       