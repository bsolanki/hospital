<?php

/**
 * This is the model class for table "{{discipline}}".
 *
 * The followings are the available columns in table '{{discipline}}':
 * @property integer $dis_id
 * @property string $dis_name
 */
class Discipline extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{discipline}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('dis_name , dis_status', 'required'),
             array('dis_name', 'unique'),
            array('dis_name', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('dis_id, dis_name, dis_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'dis_id' => 'Dis',
            'dis_name' => 'Name',
            'dis_status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('dis_id', $this->dis_id);
        $criteria->compare('dis_name', $this->dis_name, true);
        $criteria->compare('dis_status', $this->dis_status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'dis_id DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Discipline the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function displayStatus() {


        if ($this->dis_status == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("Discipline" , '.$this->dis_id.')',
                       'style' => "cursor:pointer;"
                       )
       );

    }

}
