<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ResetPasswordForm extends CFormModel
{
	public $new_password;
        public $confirm_password;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
                    // username and password are required
                    array('new_password, confirm_password', 'required'),
                //   array('new_password, confirm_password', 'length', 'min'=>8, 'max'=>16),
                    array('new_password','ext.validators.EPasswordStrength', 'min'=>8),
                    array('new_password','CompareOldPassword'),
                    array('confirm_password', 'compare', 'compareAttribute'=>'new_password'),
                    
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
                    'new_password'=>'New Password',
                    'confirm_password'=>'Confirm New Password',
		);
	}

	public function CompareOldPassword($attribute, $params){

		$user = User::model()->findByAttributes(array('usr_id'=>base64_decode($_GET['token'])));
		
		if($user->usr_password == MD5($this->new_password)){
			$this->adderror("new_password" , "New password can not be same as current password.");
		}

	}
}
