<?php

/**
 * This is the model class for table "{{income_expense}}".
 *
 * The followings are the available columns in table '{{income_expense}}':
 * @property integer $ie_id
 * @property string $ie_title
 * @property string $ie_rs
 * @property string $ie_type
 * @property integer $ie_fs_id
 * @property integer $ie_created_by
 * @property string $ie_date
 */
class IncomeExpense extends CActiveRecord
{
        public $type;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{income_expense}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ie_title, ie_rs, ie_fs_id, ie_created_by', 'required'),
			array('ie_fs_id, ie_created_by', 'numerical', 'integerOnly'=>true),
			array('ie_title, ie_rs', 'length', 'max'=>200),
			array('ie_type', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ie_id, ie_title, ie_rs, ie_type, ie_fs_id, ie_created_by, ie_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'FcaUser' => array(self::BELONGS_TO, 'User', 'ie_created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ie_id' => 'Ie',
			'ie_title' => 'Title',
			'ie_rs' => 'Rs',
			'ie_type' => 'Type',
			'ie_fs_id' => 'Fs',
			'ie_created_by' => 'Created By',
			'ie_date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ie_id',$this->ie_id);
		$criteria->compare('ie_title',$this->ie_title,true);
		$criteria->compare('ie_rs',$this->ie_rs,true);
		$criteria->compare('ie_type','INCOME',true);
		$criteria->compare('ie_fs_id', Yii::app()->user->getState('facility'));
		$criteria->compare('ie_created_by',$this->ie_created_by);
		$criteria->compare('ie_date',$this->ie_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
	public function expensesearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ie_id',$this->ie_id);
		$criteria->compare('ie_title',$this->ie_title,true);
		$criteria->compare('ie_rs',$this->ie_rs,true);
		$criteria->compare('ie_type','EXPENSE',true);
		$criteria->compare('ie_fs_id', Yii::app()->user->getState('facility'));
		$criteria->compare('ie_created_by',$this->ie_created_by);
		$criteria->compare('ie_date',$this->ie_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function fetchTotal($keys , $type){
            
            $ie=self::model()->findAllByPk($keys);
            
            $total=0;
            
            foreach($ie as $val){
                if($val->ie_type == $type){
                    $total+=$val->ie_rs;
                }
                    
            }
            return "<b>Total :</b> ". $total ;
//            return $total;
            
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IncomeExpense the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
