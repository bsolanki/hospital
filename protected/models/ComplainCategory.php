<?php

/**
 * This is the model class for table "{{complain_category}}".
 *
 * The followings are the available columns in table '{{complain_category}}':
 * @property integer $cmpln_catid
 * @property string $cmpln_catname
 * @property string $cmpln_catstatus
 */
class ComplainCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{complain_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cmpln_catname', 'required'),
                         array('cmpln_catname', 'unique'),
			array('cmpln_catname', 'length', 'max'=>255),
			array('cmpln_catstatus', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cmpln_catid, cmpln_catname, cmpln_catstatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cmpln_catid' => 'Catid',
			'cmpln_catname' => 'Name',
			'cmpln_catstatus' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cmpln_catid',$this->cmpln_catid);
		$criteria->compare('cmpln_catname',$this->cmpln_catname,true);
		$criteria->compare('cmpln_catstatus',$this->cmpln_catstatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ComplainCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function displayStatus() {

        if ($this->cmpln_catstatus == '1')
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
        else
            $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';

       return CHtml::link(
                       $img, '', array(
                       'onClick' => 'changestatus("ComplainCategory" , '.$this->cmpln_catid.')',
                       'style' => "cursor:pointer;"
                       )
       );
    }
}
