<?php

/**
 * This is the model class for table "{{services}}".
 *
 * The followings are the available columns in table '{{services}}':
 * @property integer $ser_id
 * @property string $ser_name
 * @property string $ser_duration
 * @property string $ser_fees
 */
class Services extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{services}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ser_name, ser_fees', 'required' , 'on'=>'settings'),
			array('ser_name', 'unique' , 'on'=>'settings'),
			array('ser_id', 'numerical', 'integerOnly'=>true),
			array('ser_name, ser_fees', 'length', 'max'=>200),
			array('ser_duration', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ser_id, ser_name, ser_duration, ser_fees', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ser_id' => 'Ser',
			'ser_name' => 'Serivce Name',
			'ser_duration' => 'Duration (Minutes)',
			'ser_fees' => 'Fees (Rs.)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ser_id',$this->ser_id);
		$criteria->compare('ser_name',$this->ser_name,true);
		$criteria->compare('ser_duration',$this->ser_duration,true);
		$criteria->compare('ser_fac_id',  Yii::app()->user->getState('facility'));
		$criteria->compare('ser_fees',$this->ser_fees,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Services the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
