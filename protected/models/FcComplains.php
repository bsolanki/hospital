<?php

/**
 * This is the model class for table "{{fc_complains}}".
 *
 * The followings are the available columns in table '{{fc_complains}}':
 * @property integer $hfc_id
 * @property integer $hfc_fc_id
 * @property integer $hfc_compain_id
 */
class FcComplains extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fc_complains}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hfc_fc_id, hfc_compain_id', 'required'),
			array('hfc_fc_id, hfc_compain_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('hfc_id, hfc_fc_id, hfc_compain_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'hfc_id' => 'Hfc',
			'hfc_fc_id' => 'Hfc Fc',
			'hfc_compain_id' => 'Hfc Compain',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('hfc_id',$this->hfc_id);
		$criteria->compare('hfc_fc_id',$this->hfc_fc_id);
		$criteria->compare('hfc_compain_id',$this->hfc_compain_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FcComplains the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
