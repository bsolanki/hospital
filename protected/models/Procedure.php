<?php

/**
 * This is the model class for table "{{procedure}}".
 *
 * The followings are the available columns in table '{{procedure}}':
 * @property integer $pro_id
 * @property string $pro_name
 * @property string $pro_status
 */
class Procedure extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{procedure}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pro_name, pro_fc_id,pro_status', 'required'),
			array('pro_id', 'safe'),			
			array('pro_name', 'unique'),
			array('pro_name', 'length', 'max'=>255),
			array('pro_status', 'length', 'max'=>1),
// 			array('pro_cat_id', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pro_fc_id,pro_id, pro_name, pro_status', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
//			'cmpCat' => array(self::BELONGS_TO, 'ProcedureCategory', 'pro_cat_id'),
// 			'cmpCat' => array(self::BELONGS_TO, 'ProcedureCategory', 'pro_cat_id'),
		);
	}
	 
    public function getProcedureCategory(){
        return CHtml::listData(ProcedureCategory::model()->findAllByAttributes(array('pro_catstatus' => '1')), 'pro_catid', 'pro_catname');
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pro_id' => 'Id',
			'pro_name' => 'Name',
			'pro_fc_id' => 'Facility Id',
			'pro_cat_id' => 'Category',
			'pro_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pro_id',$this->pro_id);
		$criteria->compare('pro_name',$this->pro_name,true);
		$criteria->compare('pro_status',$this->pro_status,true);
		if(Yii::app()->user->getState('facility') != null){
		  $criteria->compare('pro_fc_id',  Yii::app()->user->getState('facility'),true);
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'pro_id DESC',
            )
		));
	}
  
    public function displayStatus() {

        if ($this->pro_status == '1')
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/active.png" title="Active"/>';
       else
           $img = '<img src="' . Yii::app()->theme->baseUrl . '/img/inactive.png" title="Inactive"/>';


       return CHtml::link(
           $img, '', array(
           'onClick' => 'changestatus("Procedure" , '.$this->pro_id.')',
           'style' => "cursor:pointer;"
           )
       );
       
    } 

    
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Procedure the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
