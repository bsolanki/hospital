<?php

/**
 * This is the model class for table "{{patient}}".
 *
 * The followings are the available columns in table '{{patient}}':
 * @property integer $pt_id
 * @property integer $pt_facility_id
 * @property string $pt_fname
 * @property string $pt_mname
 * @property string $pt_lname
 * @property string $pt_dob
 * @property string $pt_gender
 * @property string $pt_mobile
 * @property string $pt_reg_number
 * @property string $pt_image
 * @property string $pt_address
 * @property string $pt_city
 * @property string $pt_zip
 * @property string $pt_prefered_lang
 * @property string $pt_prefered_pharma
 * @property integer $pt_insurance_id
 * @property string $pt_insurance_name
 * @property string $pt_referal_by
 */
class Patient extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{patient}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pt_facility_id, pt_fname, pt_dob, pt_gender, pt_mobile, pt_address', 'required'),
			array('pt_zip, pt_mobile', 'numerical', 'integerOnly'=>true),
			array('pt_fname, pt_mname, pt_lname, pt_mobile, pt_reg_number, pt_image, pt_city, pt_zip, pt_prefered_lang, pt_prefered_pharma, pt_insurance_name, pt_referal_by', 'length', 'max'=>255),
			array('pt_gender', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pt_id, pt_facility_id, pt_fname, pt_mname, pt_lname, pt_dob, pt_gender, pt_mobile, pt_reg_number, pt_image, pt_address, pt_city, pt_zip, pt_prefered_lang, pt_prefered_pharma, pt_insurance_id, pt_insurance_name, pt_referal_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pt_id' => 'Pt',
			'pt_facility_id' => 'Pt Facility',
			'pt_fname' => 'First name',
			'pt_mname' => 'Middle name',
			'pt_lname' => 'Last name',
			'pt_dob' => 'Date Of Birth',
			'pt_gender' => 'Gender',
			'pt_mobile' => 'Mobile',
			'pt_reg_number' => 'Reg. Number',
			'pt_image' => 'Image',
			'pt_address' => 'Address',
			'pt_city' => 'City',
			'pt_zip' => 'Zip',
			'pt_prefered_lang' => 'Prefered Language',
			'pt_prefered_pharma' => 'Prefered Pharma',
			'pt_insurance_id' => 'Insurance',
			'pt_insurance_name' => 'Insurance Name',
			'pt_referal_by' => 'Referal By',
			'pt_relationship' => 'Relationship',
		);
	}
        
        

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pt_id',$this->pt_id);
		$criteria->compare('pt_facility_id',  Yii::app()->user->getState('facility'));
		//$criteria->compare('pt_fname',$this->pt_fname,true);
                if (!empty($this->pt_fname))
                    $criteria->addCondition('concat_ws(" ",pt_fname,pt_lname) like "%' . $this->pt_fname . '%" ', 'AND');
		$criteria->compare('pt_mname',$this->pt_mname,true);
		$criteria->compare('pt_lname',$this->pt_lname,true);
		$criteria->compare('pt_dob',$this->pt_dob,true);
		$criteria->compare('pt_gender',$this->pt_gender,true);
		$criteria->compare('pt_mobile',$this->pt_mobile,true);
		$criteria->compare('pt_reg_number',$this->pt_reg_number,true);
		$criteria->compare('pt_image',$this->pt_image,true);
		$criteria->compare('pt_address',$this->pt_address,true);
		$criteria->compare('pt_city',$this->pt_city,true);
		$criteria->compare('pt_zip',$this->pt_zip,true);
		$criteria->compare('pt_prefered_lang',$this->pt_prefered_lang,true);
		$criteria->compare('pt_prefered_pharma',$this->pt_prefered_pharma,true);
		$criteria->compare('pt_insurance_id',$this->pt_insurance_id);
		$criteria->compare('pt_insurance_name',$this->pt_insurance_name,true);
		$criteria->compare('pt_referal_by',$this->pt_referal_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Patient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getname(){
            
            if(empty($this->pt_lname) && empty($this->pt_mname)){
                return $this->pt_fname;
            }elseif(empty($this->pt_lname)){
                return $this->pt_fname . " " . $this->pt_mname ;
            }elseif(empty($this->pt_mname)){
                return $this->pt_fname . " " . $this->pt_lname ;
            }else{
                return $this->pt_fname . " " . $this->pt_mname . " " . $this->pt_lname ;
            }
        }
        
        public function getFrequency(){
            
            return CHtml::listData(Frequency::model()->findAll(), 'frqcy_id', 'frqcy_name');
            
        }
        
        public function getInstruction(){
            
            return CHtml::listData(Instruction::model()->findAll(), 'inst_id', 'inst_name');
            
        }
        
        public function getFrequencyByfacilityid(){
            
            $data = CHtml::listData(FcFrequency::model()->findAllByAttributes(array('hff_fc_id'=>Yii::app()->user->getState('facility'))) , 'hff_fc_fre_id' ,'hff_fc_fre_id');
            $data = implode(",", $data);
            if(empty($data))
                return array();
            return CHtml::listData(Frequency::model()->findAllByAttributes(array() , array('condition'=>'frqcy_id IN (' . $data . ')')) , 'frqcy_id', 'frqcy_name');
            
        }
        
        public function getInstructionByfacilityid(){
            
            $data = CHtml::listData(FcInstruction::model()->findAllByAttributes(array('hfi_fc_id'=>Yii::app()->user->getState('facility'))) , 'hfi_instruction_id' ,'hfi_instruction_id');
            $data = implode(",", $data);
            if(empty($data))
                return array();
            return CHtml::listData(Instruction::model()->findAllByAttributes(array() , array('condition'=>'inst_id IN (' . $data . ')')) , 'inst_id', 'inst_name');
            
        }
}
