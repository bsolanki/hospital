<?php
/* @var $this ComplainController */
/* @var $model Complain */

$this->breadcrumbs=array(
	'Complains'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Complain', 'url'=>array('index')),
	array('label'=>'Manage Complain', 'url'=>array('admin')),
);
?>

<h1>Create Complain</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>