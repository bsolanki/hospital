<?php
/* @var $this ComplainController */
/* @var $model Complain */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'complain-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">

	<div class="form-group col-lg-3 clear"> 
		<?php echo $form->labelEx($model,'cmp_name'); ?>
		<?php echo $form->textField($model,'cmp_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'cmp_name'); ?>
	</div>

    <div class="form-group col-lg-3 clear">
		<?php echo $form->labelEx($model,'cmp_cat_id'); ?> 
		<?php echo $form->dropDownList($model, 'cmp_cat_id', $model->getComplainCategory(), array('class' => 'form-control' , 'empty'=>'--Select Category--')); ?>           
		<?php echo $form->error($model,'cmp_cat_id'); ?>
	</div>
	
	<div class="form-group col-lg-2 clear"> 
		<?php echo $form->labelEx($model,'cmp_status'); ?>
		<?php echo $form->dropDownList($model, 'cmp_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'cmp_status'); ?>
	</div>
        <div class="buttons form-group col-lg-2 searchbutton">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	
	 	<?php
        //  FB::setEnabled('false');
        //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');

        echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Submit' : 'Save', $this->createUrl('Complain/create'), array('type' => 'POST',
            'success' => 'function(data) {
                             var response= jQuery.parseJSON (data);
                             if (response.success ==true){
                             //alert(response.div);
                             $("#statusMsg").html("");
                             $("#statusMsg").append(response.div);
                             $("#statusMsg").show().delay(3000).hide(0);
                             $("#complain-form")[0].reset();
                             update_grid_view();
                             return false;
                             }else{
                                data = JSON.parse(data);
                                      $.each(data, function(key, val) {
                        $("#complain-form #"+key+"_em_").text(val);                                                    
                        $("#complain-form #"+key+"_em_").show();
                        }); 
					}

                             }' //success
                ), array('type' => 'submit', 'class' => 'btn btn-primary')
        );
        ?>

        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->