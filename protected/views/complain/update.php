<?php
/* @var $this ComplainController */
/* @var $model Complain */

$this->breadcrumbs=array(
	'Complains'=>array('index'),
	$model->cmp_id=>array('view','id'=>$model->cmp_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Complain', 'url'=>array('index')),
	array('label'=>'Create Complain', 'url'=>array('create')),
	array('label'=>'View Complain', 'url'=>array('view', 'id'=>$model->cmp_id)),
	array('label'=>'Manage Complain', 'url'=>array('admin')),
);
?>

<h1>Update Complain <?php echo $model->cmp_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>