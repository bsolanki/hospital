<?php
/* @var $this ComplainController */
/* @var $model Complain */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cmp_id'); ?>
		<?php echo $form->textField($model,'cmp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cmp_cat_id'); ?>
		<?php echo $form->textField($model,'cmp_cat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cmp_name'); ?>
		<?php echo $form->textField($model,'cmp_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cmp_status'); ?>
		<?php echo $form->textField($model,'cmp_status',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->