<?php
/* @var $this ComplainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Complains',
);

$this->menu=array(
	array('label'=>'Create Complain', 'url'=>array('create')),
	array('label'=>'Manage Complain', 'url'=>array('admin')),
);
?>

<h1>Complains</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
