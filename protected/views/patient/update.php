<?php
/* @var $this PatientController */
/* @var $model Patient */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'patient-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <h4>Primary Details</h4>
    <hr>
    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_fname'); ?>
            <?php echo $form->textField($model, 'pt_fname', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_fname'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_dob'); ?>
            <?php echo $form->textField($model, 'pt_dob', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control date-picker')); ?>
            <?php echo $form->error($model, 'pt_dob'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_gender'); ?>
            <?php echo $form->dropDownList($model, 'pt_gender', array('male' => 'Male', 'female' => 'Female', 'other' => 'Other'), array('class' => 'form-control', 'empty' => '--Select Gender--')); ?>
            <?php echo $form->error($model, 'pt_gender'); ?>
        </div>

    </div>
    <div class="row">

        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_mobile'); ?>
            <?php echo $form->textField($model, 'pt_mobile', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_mobile'); ?>
        </div>
    </div>

    <h4>Secondary Details</h4>
    <hr>
    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_address'); ?>
            <?php echo $form->textArea($model, 'pt_address', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_address'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_city'); ?>
            <?php echo $form->textField($model, 'pt_city', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_city'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_zip'); ?>
            <?php echo $form->textField($model, 'pt_zip', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_zip'); ?>
        </div>
    </div>

    <div class="row">

        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_prefered_lang'); ?>
            <?php echo $form->textField($model, 'pt_prefered_lang', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_prefered_lang'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_prefered_pharma'); ?>
            <?php echo $form->textField($model, 'pt_prefered_pharma', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_prefered_pharma'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_insurance_id'); ?>
            <?php echo $form->textField($model, 'pt_insurance_id', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_insurance_id'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_insurance_name'); ?>
            <?php echo $form->textField($model, 'pt_insurance_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_insurance_name'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_referal_by'); ?>
            <?php echo $form->textField($model, 'pt_referal_by', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'pt_referal_by'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'pt_relationship'); ?>
            <?php echo $form->dropDownList($model, 'pt_relationship', array('self' => 'Self', 'parent' => 'Parent', 'Guardian' => 'guardian', 'friends' => 'Friends', 'spouse' => 'Spouse', 'other' => 'Other'), array('class' => 'form-control', 'empty' => '--Select Relationship--')); ?>
            <?php echo $form->error($model, 'pt_relationship'); ?>
        </div>
    </div>

    <div class="row buttons form-group col-lg-12">
        <?php
        //  FB::setEnabled('false');
        echo CHtml::submitButton('update', array('id' => 'updateinfo', 'class' => 'btn btn-primary'));
        ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    $("#updateinfo").click(function () {

        var data = $('#patient-form').serialize();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('patient/update') ?>",
            data: "data=" + data,
            method: "POST",
            success: function (data) {

                var response = JSON.parse(data);
                console.log(response);

                if (response.success == true) {

                    validationmsg(response.div);
                    return false;

                } else {

                    console.log("Fainting/Blackout");
                    $("#addcomplain-form #Complain_cmp_name_em_").text(response.Complain_cmp_name);
                    $("#addcomplain-form #Complain_cmp_name_em_").show();
                    return false;

                }

            }});

        return false;
    });
</script>