<?php
$baseUrl = Yii::app()->theme->baseUrl;

if (!empty($info)) {

    $myVarList = array(
        'patient_id' => $info->pt_id
    );

    Yii::app()->clientScript->registerScript("myVarList", 'myVarList = ' . $info->pt_id . ';');
}

?>

<div class="row panel panel-white col-sm-12">
    <div class="col-sm-3">
        <div class="panel-heading clearfix">
            <h4 class="panel-title" style="float:left;">Patients</h4>
            <div class="panel-control">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('patient/create'); ?>" class="btn btn-success btn btn-primary" style="float: right" ><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="table-responsive">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'patient-grid',
                'dataProvider' => $model->search(),
                'itemsCssClass' => 'table table-hover table-striped',
                'template' => "{items}\n{summary}{pager}",
                'filter' => $model,
                'columns' => array(
                    array(
                        'type' => 'raw',
                        'header' => '',
                        'name' => 'pt_fname',
                        'value' => 'CHtml::link("$data->pt_fname",Yii::app()->createUrl("patient/View",array("id"=>$data->pt_id)))',
                    ),
                    array(
                        //'header' => 'Actions',
                        'class' => 'CButtonColumn',
                        'template' => '{delete}',
                        'htmlOptions' => array('width' => '10%'),
                        'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Patient\'s records?'),
                        'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                        'buttons' => array(
                            'delete' => array(
                                'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                'url' => 'Yii::app()->createUrl("/patient/delete",array("id"=>$data["pt_id"]))',
                                'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                'imageUrl' => false,
                            ),
                        ),
                    ),
                ),
            ));
            ?>
        </div> 
    </div>
    <div class="col-sm-9">
        <?php
        
        if (!empty($info)) {
            ?>

            <div class="panel panel-white">

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title"><?php echo $info->pt_fname ?></h3>
                    </div>
                    <div class="panel-body">
                        <div role="tabpanel clearfix">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills" role="tablist">
                                <li role="presentation" class="active"><a href="#tab5" role="tab" data-toggle="tab">Summary</a></li>
                                <li role="presentation"><a href="#load_complain" role="tab" data-toggle="tab" id="complains" rel="<?php echo Yii::app()->createAbsoluteUrl('complain/loadcomplain') ?>">Complains</a></li>
                                <li role="presentation"><a href="#load_advice" role="tab" data-toggle="tab" id="advice" rel="<?php echo Yii::app()->createAbsoluteUrl('fcAdvice/admin') ?>" >Advice</a></li>
                                <li role="presentation"><a href="#load_rx" role="tab" data-toggle="tab" id="rx" rel="<?php echo Yii::app()->createAbsoluteUrl('rx/admin') ?>" >RX</a></li>
                                <li role="presentation"><a href="#load_info" role="tab" data-toggle="tab" id="information" rel="<?php echo Yii::app()->createAbsoluteUrl('patient/update') ?>" >info</a></li>
                                <li role="presentation"><a href="#load_vitals" role="tab" data-toggle="tab" id="vital" rel="<?php echo Yii::app()->createAbsoluteUrl('vitals/admin') ?>"  >Vitals</a></li>
                                <li role="presentation"><a href="#load_allergy" role="tab" data-toggle="tab" id="allergy" rel="<?php echo Yii::app()->createAbsoluteUrl('PtAllergen/admin') ?>"  >Allergy</a></li>
                                <!--
                                                            <li role="presentation"><a href="#tab10" role="tab" data-toggle="tab">MAR</a></li>-->

                                <!--                            <li role="presentation"><a href="#tab12" role="tab" data-toggle="tab">Allergy</a></li>
                                                            <li role="presentation"><a href="#tab13" role="tab" data-toggle="tab">lab's</a></li>
                                                            <li role="presentation"><a href="#tab14" role="tab" data-toggle="tab">Vitals</a></li>-->
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="tab5">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="load_complain">
                                    Complain
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="load_advice">
                                    Advice
                                </div>  
                                <div role="tabpanel" class="tab-pane fade" id="load_rx">
                                    Rx
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="load_info">
                                    info
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="load_vitals">
                                    Vital
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="load_allergy">
                                   ALLERGY
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="tab12">
                                    <div class="col-sm-6 center">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab13">
                                    <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab14">
                                    <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>
                                </div>
                            </div>
                        </div>
                        <?php // echo CHtml::submitButton('Save Changes', array('class' => 'btn btn-primary', 'style' => 'float:right'));       ?>
                    </div>

                </div>
                <?php
            } else {
                ?>
                <div class="demo_image col-sm-12"  >
                    <img width="40" height="40" alt="" src="<?php echo Yii::app()->baseUrl; ?>/images/anon_user.png" class="img-circle ">
                </div>
                <?php
            }
            ?>
        </div>

    </div>


    <?php
    
    $patient_script = Yii::app()->getClientScript();
    $patient_script->registerScriptFile($baseUrl . '/assets/js/patient.js', CClientScript::POS_END);
    
    
    ?>

    <script type="text/javascript">
        <?php 
        if(!empty($info)){
            ?>
                var patient_id = '<?php echo $info->pt_id ?>';
            <?php
        }
        ?>
        

        $("#patient-grid_c0").parent().hide();

        $(".grid-view").find(".filters").find('input').addClass("form-control");

    </script>