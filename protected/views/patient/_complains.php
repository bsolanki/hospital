

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="myModalLabel" class="modal-title">Modify Complain</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-sm-9">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'complain-form',
                        'enableAjaxValidation' => true,
                    ));
                    ?>

                    <?php
                    $this->widget('ext.select2.ESelect2', array(
                        'model' => $model,
                        'attribute' => 'pc_complain_id',
                        'data' => $model->getComplain(),
                        'htmlOptions' => array(
                            'multiple' => 'multiple',
                            //'class' => 'col-sm-6',
                            'style' => 'width:100%',
                        //'disabled'=>$model->isNewRecord ? false : true
                        ),
                        'options' => array(
                            'placeholder' => 'Select Complains'
                        ),
                    ));
                    ?>
                    <?php echo $form->error($model, 'pc_complain_id'); ?>
                    <?php
//  FB::setEnabled('false');
//  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');

                    echo CHtml::submitButton('Customize', array('id' => 'cust_complain', 'class' => 'btn btn-primary'));
                    ?>

                    <?php $this->endWidget(); ?>


                </div>

            </div>


            <script type="text/javascript">
                $('select').select2();

                $("#cust_complain").click(function () {
//                    
//                    alert($("#PtComplain_pc_complain_id").val());
//                    return false;
                    if ($("#PtComplain_pc_complain_id").val() == null) {
//                        alert("in");
                        $("#PtComplain_pc_complain_id_em_").html("Please select atleast one Complain.");
                        $("#PtComplain_pc_complain_id_em_").show();
                        return false;
                    }

                    $.ajax({
                        url: "<?php echo Yii::app()->createAbsoluteUrl('patient/complain') ?>",
                        data: "pd_complains=" + $("#PtComplain_pc_complain_id").val(),
                        method: "POST",
                        success: function (data) {

                            var response = jQuery.parseJSON(data);
                            var display = $("#display_selected_compalin");
                            display.html("");
                            $.each(response, function (key, val) {
                                display.append("<button type=\'button\' class=\'btn btn-info btn-rounded compain_selection\' id=\'" + key + "\' >" + val + "  </button>   ");
                            });
                            $(".modal").modal("toggle");
                            validationmsg("Complain has been customize successfully.");

                        }});

                    return false;
                });
            </script>