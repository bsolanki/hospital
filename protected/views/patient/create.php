<?php
/* @var $this PatientController */
/* @var $model Patient */

$this->breadcrumbs = array(
    'Patients' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Patient', 'url' => array('index')),
    array('label' => 'Manage Patient', 'url' => array('admin')),
);
?>

<div class="col-md-12">
    <div class="panel panel-white">
        <div class="panel-heading clearfix">
            <h4 class="panel-title">Add Patient </h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <?php $this->renderPartial('_form', array('model' => $model)); ?>

            </div>
        </div>
    </div>

</div>
</div> 
