<!-- load complain popup -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade in " >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 id="myModalLabel" class="modal-title">Modify Complains</h4>
            </div>
            <div class="modal-body">
                <?php
                $this->renderPartial('_complains'
                        , array(
                    'patient' => $patient_data
                ));
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="panel-heading clearfix">
            <h4 class="panel-title" style="float:left;">Patients</h4>
            <div class="panel-control">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('patient/create'); ?>" class="btn btn-success btn btn-primary" style="float: right" ><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="table-responsive">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'patient-grid',
                'dataProvider' => $model->search(),
                'itemsCssClass' => 'table table-hover table-striped',
                'template' => "{items}\n{summary}{pager}",
                'filter' => $model,
                'columns' => array(
                    array(
                        'type' => 'raw',
                        'header' => '',
                        'name' => 'pt_fname',
                        'value' => 'CHtml::link("$data->pt_fname",Yii::app()->createUrl("patient/list",array("id"=>$data->pt_id)))',
                    ),
                    array(
                        //'header' => 'Actions',
                        'class' => 'CButtonColumn',
                        'template' => '{delete}',
                        'htmlOptions' => array('width' => '10%'),
                        'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Patient\'s records?'),
                        'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                        'buttons' => array(
                            'delete' => array(
                                'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                'url' => 'Yii::app()->createUrl("/patient/delete",array("id"=>$data["pt_id"]))',
                                'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                'imageUrl' => false,
                            ),
                        ),
                    ),
                ),
            ));
            ?>
        </div> 
    </div>
    <div class="col-sm-9">
        <?php
        if (!empty($patient_details)) {
            ?>
            <div class="panel panel-white">

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title"><?php echo $patient_details->pt_fname . " " . $patient_details->pt_lname; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div role="tabpanel clearfix">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills" role="tablist">
                                <li role="presentation" class="active"><a href="#tab5" role="tab" data-toggle="tab">Summary</a></li>
                                <li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Complains</a></li>
                                <li role="presentation"><a href="#tab11" role="tab" data-toggle="tab">Advice</a></li>
                                <li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">RX</a></li>
                                <li role="presentation"><a href="#tab8" role="tab" data-toggle="tab">info</a></li>
                                <li role="presentation"><a href="#tab9" role="tab" data-toggle="tab">Vitals</a></li>
                                <!--
                                                            <li role="presentation"><a href="#tab10" role="tab" data-toggle="tab">MAR</a></li>-->

                                <!--                            <li role="presentation"><a href="#tab12" role="tab" data-toggle="tab">Allergy</a></li>
                                                            <li role="presentation"><a href="#tab13" role="tab" data-toggle="tab">lab's</a></li>
                                                            <li role="presentation"><a href="#tab14" role="tab" data-toggle="tab">Vitals</a></li>-->
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="tab5">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab6">
                                    <a href="#" data-target="#myModal" data-toggle="modal" style="float:right" > | Customize</a>
                                    <a id="addComplain" href="<?php echo Yii::app()->createAbsoluteUrl('patient/addComplain') ?>" data-toggle="modal"  style="float:right" > Add Complain </a>

                                    <div class="row">
                                        <div class="col-sm-6">

                                            <?php
                                            $this->widget('ext.select2.ESelect2', array(
                                                'model' => $patient,
                                                'attribute' => 'pd_complains',
                                                'data' => $patient->getcomplains($patient->pd_complains),
                                                'htmlOptions' => array(
                                                    'multiple' => 'multiple',
                                                    'class' => 'col-sm-6',
                                                    'style' => 'width:100%',
                                                    'id' => 'complains_dropdown'
                                                ),
                                                'options' => array(
                                                    'placeholder' => 'Select Complains'
                                                ),
                                            ));
                                            ?> 
                                        </div>

                                    </div>
                                    <p id="display_selected_compalin" >
                                        <?php
                                        $details = $patient->getcomplains($patient_data->pd_complains);
                                        if (!empty($details)) {
                                            foreach ($details as $key => $val) {
                                                ?>
                                                <button type="button" class="btn btn-info btn-rounded compain_selection" id="<?php echo $key; ?>" ><?php echo $val; ?></button>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab11">
                                    <?php
                                    $this->renderPartial('application.views.fcAdvice.admin', array(
                                        'model' => $fcadvice,
                                        //'create_model' => $createrx_model
                                    ));
                                    ?>

                                </div>  
                                <div role="tabpanel" class="tab-pane fade" id="tab7">
                                    <?php
                                    $this->renderPartial('application.views.rx.admin', array(
                                        'model' => $rx_model,
                                        'create_model' => $createrx_model
                                    ));
                                    ?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab8">
                                            <?php
                                            $this->renderPartial('application.views.patient.view', array(
                                                'model' => $info,
                                            ));
                                            ?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab9">
                                    <?php
                                    $this->renderPartial('application.views.vitals.admin', array(
                                        'model' => $vital,
                                        'create_model' => $create_vital
                                    ));
                                    ?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab10">
                                    <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="tab12">
                                    <div class="col-sm-6 center">
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab13">
                                    <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab14">
                                    <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>
                                </div>
                            </div>
                        </div>
    <?php // echo CHtml::submitButton('Save Changes', array('class' => 'btn btn-primary', 'style' => 'float:right'));    ?>
                    </div>

                </div>
    <?php
}
?>
        </div>

        <script type="text/javascript">

            $("#patient-grid_c0").parent().hide();

            $(".grid-view").find(".filters").find('input').addClass("form-control");

            $("#display_selected_compalin").on("click", ".compain_selection", function () {

                var selectedValues = $("#complains_dropdown").val();
                selectedValues += ',' + this.id;
                selectedValues = selectedValues.toString().split(',');
                $("#complains_dropdown").select2("val", selectedValues);
            });

    // Support for AJAX loaded modal window.
    // Focuses on first input textbox after it loads the window.
            $('#addComplain').click(function (e) {

                var url = $(this).attr('href');
                if (url.indexOf('#') == 0) {
                    $(url).modal('open');
                } else {
                    $.get(url, function (data) {
                        $('<div class="modal fade" id="add_complain">' + data + '</div>').modal();
                    }).success(function () {
                        $('input:text:visible:first').focus();
                    });
                }
                return false;
            });

        </script>