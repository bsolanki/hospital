<?php
/* @var $this PatientController */
/* @var $model Patient */

$this->breadcrumbs = array(
    'Patients' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Patient', 'url' => array('index')),
    array('label' => 'Create Patient', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#patient-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#patient-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>

<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->

    <div class="col-md-12" >
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Patients</h4>
                <div class="panel-control">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('patient/create'); ?>" class="btn btn-success btn btn-primary" ><i class="fa fa-plus"></i> Add Patient </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'patient-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
//                            'pt_id',
//                            'pt_facility_id',
                            array(
                                'header'=>'Details',
                                'type'=>'raw',
                                'value' => 'CHtml::link("set details",Yii::app()->createUrl("patient/details",array("id"=>$data->pt_id)))',
                            ),
                            array(
                                'name' => 'pt_fname',
                                'header' => 'Name',
                                'value' => '$data->getname()',
                                'htmlOptions' => array('width' => '25%'),
                                'headerHtmlOptions' => array('width' => '25%'),
                            ),
                            array(
                                'name' => 'pt_dob',
                                'value' => 'Utility::timezoneConvert($data->pt_dob)',
                                'htmlOptions' => array('width' => '25%'),
                                'headerHtmlOptions' => array('width' => '25%'),
                            ),
                            'pt_gender',
                            'pt_mobile',
                            /*
                              'pt_gender',
                              'pt_mobile',
                              'pt_reg_number',
                              'pt_image',
                              'pt_address',
                              'pt_city',
                              'pt_zip',
                              'pt_prefered_lang',
                              'pt_prefered_pharma',
                              'pt_insurance_id',
                              'pt_insurance_name',
                              'pt_referal_by',
                             */
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{edit} {delete}',
                                'htmlOptions' => array('width' => '10%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Patient\'s records?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
                                    'edit' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i>' ),
                                        'url' => 'Yii::app()->createUrl("/patient/update",array("id"=>$data["pt_id"]))',
                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
                                        'imageUrl' => false,
                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                        'url' => 'Yii::app()->createUrl("/patient/delete",array("id"=>$data["pt_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div> 


