
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="myModalLabel" class="modal-title">Add Complain</h4>
        </div>
        <div class="modal-body">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'addcomplain-form',
                'action' => Yii::app()->createAbsoluteUrl('patient/addComplain'),
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => true,
            ));
            ?>

            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php echo $form->errorSummary($model); ?>

            <div class="row">

                <div class="form-group col-lg-12 clear"> 
                    <?php echo $form->labelEx($model, 'cmp_name'); ?>
                    <?php echo $form->textField($model, 'cmp_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'cmp_name'); ?>
                </div>

                <div class="buttons form-group col-lg-2">
                    <?php
                        //  FB::setEnabled('false');
                        echo CHtml::submitButton('Add', array('id' => 'addcomplain' , 'class' => 'btn btn-primary'));
                    ?>

                    <?php $this->endWidget(); ?>

                </div>
            </div>

            <script type="text/javascript">
                $("#addcomplain").click(function () {
                    
                    $.ajax({
                        url: "<?php echo Yii::app()->createAbsoluteUrl('patient/addcomplain') ?>",
                        data: "complain_name=" + $("#Complain_cmp_name").val(),
                        method: "POST",
                        success: function (data) {

                            var response= JSON.parse(data);
                            console.log(response);
                            
                            if (response.success == true) {
                                
                                validationmsg("Complain has been added successfully.");
                                $("#add_complain").modal("toggle");
                                $('#complains_dropdown').append($("<option>").val(response.cmp_id).html(response.cmp_name));
                                return false;
                                
                            } else {
                                
                                console.log("Fainting/Blackout");
                                $("#addcomplain-form #Complain_cmp_name_em_").text(response.Complain_cmp_name);
                                $("#addcomplain-form #Complain_cmp_name_em_").show();
                                return false;
                                
                            }

                        }});

                    return false;
                });
            </script>