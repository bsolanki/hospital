<?php
/* @var $this PatientController */
/* @var $model Patient */

$this->breadcrumbs=array(
	'Patients'=>array('index'),
	$model->pt_id,
);

$this->menu=array(
	array('label'=>'List Patient', 'url'=>array('index')),
	array('label'=>'Create Patient', 'url'=>array('create')),
	array('label'=>'Update Patient', 'url'=>array('update', 'id'=>$model->pt_id)),
	array('label'=>'Delete Patient', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pt_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Patient', 'url'=>array('admin')),
);
?>

<h1>View Patient #<?php echo $model->pt_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'pt_id',
		'pt_facility_id',
		'pt_fname',
		'pt_mname',
		'pt_lname',
		'pt_dob',
		'pt_gender',
		'pt_mobile',
		'pt_reg_number',
		'pt_image',
		'pt_address',
		'pt_city',
		'pt_zip',
		'pt_prefered_lang',
		'pt_prefered_pharma',
		'pt_insurance_id',
		'pt_insurance_name',
		'pt_referal_by',
	),
)); ?>
