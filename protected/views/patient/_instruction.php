<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="myModalLabel" class="modal-title">Modify Instruction</h4>
        </div>
        <div class="modal-body">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'instruction-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => true,
            ));
            ?>

            <?php
            $this->widget('ext.select2.ESelect2', array(
                'model' => $instruction,
                'attribute' => 'hfi_instruction_id',
                'data' => Patient::model()->getInstruction(),
                'htmlOptions' => array(
                    'multiple' => 'multiple',
                    //'class' => 'col-sm-6',
                    'id'=>'frequency',
                    'style' => 'width:100%',
                //'disabled'=>$model->isNewRecord ? false : true
                ),
                'options' => array(
                    'placeholder' => 'Select frequency'
                ),
            ));
            ?>
            <?php
//  FB::setEnabled('false');
//  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');

            echo CHtml::ajaxSubmitButton('Customize', $this->createUrl('patient/complain'), array('type' => 'POST',
                'success' => 'function(data) {
        
                             var response= jQuery.parseJSON (data);
                             var display = $("#display_selected_compalin");
                             display.html("");
                             $.each(response, function(key, val) {
                                display.append("<button type=\'button\' class=\'btn btn-info btn-rounded compain_selection\' id=\'"+key +  "\' >" + val +"  </button>   ");
                             }); 
                             $("#myModal").modal("toggle");
                             validationmsg("Complain has been customize successfully.");

                             if (response.success ==true){
                             //alert(response.div);
                             $("#statusMsg").html("");
                             $("#statusMsg").append(response.div);
                             $("#statusMsg").show().delay(3000).hide(0);
                             $("#frequency-form")[0].reset();
                             update_grid_view(); 
                             return false;
                             }else{
//                             alert(data);
data = JSON.parse(data);
                                      $.each(data, function(key, val) {
                                        $("#complain-form #"+key+"_em_").text(val);                                                    
                                        $("#complain-form #"+key+"_em_").show();
                                        });    
                                 }

                             }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary', 'style' => 'float:left')
            );
            ?>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>