<?php
/* @var $this PatientController */
/* @var $data Patient */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pt_id), array('view', 'id'=>$data->pt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_facility_id')); ?>:</b>
	<?php echo CHtml::encode($data->pt_facility_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_fname')); ?>:</b>
	<?php echo CHtml::encode($data->pt_fname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_mname')); ?>:</b>
	<?php echo CHtml::encode($data->pt_mname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_lname')); ?>:</b>
	<?php echo CHtml::encode($data->pt_lname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_dob')); ?>:</b>
	<?php echo CHtml::encode($data->pt_dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_gender')); ?>:</b>
	<?php echo CHtml::encode($data->pt_gender); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->pt_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_reg_number')); ?>:</b>
	<?php echo CHtml::encode($data->pt_reg_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_image')); ?>:</b>
	<?php echo CHtml::encode($data->pt_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_address')); ?>:</b>
	<?php echo CHtml::encode($data->pt_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_city')); ?>:</b>
	<?php echo CHtml::encode($data->pt_city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_zip')); ?>:</b>
	<?php echo CHtml::encode($data->pt_zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_prefered_lang')); ?>:</b>
	<?php echo CHtml::encode($data->pt_prefered_lang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_prefered_pharma')); ?>:</b>
	<?php echo CHtml::encode($data->pt_prefered_pharma); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_insurance_id')); ?>:</b>
	<?php echo CHtml::encode($data->pt_insurance_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_insurance_name')); ?>:</b>
	<?php echo CHtml::encode($data->pt_insurance_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_referal_by')); ?>:</b>
	<?php echo CHtml::encode($data->pt_referal_by); ?>
	<br />

	*/ ?>

</div>