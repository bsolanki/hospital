<?php
/* @var $this PatientController */
/* @var $model Patient */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'pt_fname'); ?>
        <?php echo $form->textField($model, 'pt_fname',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

    <div class="row buttons form-group" >

        <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary searchbutton' , 'style'=>'margin-top:25px;')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->