<?php
/* @var $this IncomeExpenseController */
/* @var $model IncomeExpense */

$this->breadcrumbs=array(
	'Income Expenses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List IncomeExpense', 'url'=>array('index')),
	array('label'=>'Manage IncomeExpense', 'url'=>array('admin')),
);
?>

<h1>Create IncomeExpense</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>