<?php
/* @var $this IncomeExpenseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Income Expenses',
);

$this->menu=array(
	array('label'=>'Create IncomeExpense', 'url'=>array('create')),
	array('label'=>'Manage IncomeExpense', 'url'=>array('admin')),
);
?>

<h1>Income Expenses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
