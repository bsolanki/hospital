<?php
/* @var $this IncomeExpenseController */
/* @var $model IncomeExpense */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ie_id'); ?>
		<?php echo $form->textField($model,'ie_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ie_title'); ?>
		<?php echo $form->textField($model,'ie_title',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ie_rs'); ?>
		<?php echo $form->textField($model,'ie_rs',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ie_type'); ?>
		<?php echo $form->textField($model,'ie_type',array('size'=>7,'maxlength'=>7)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ie_created_by'); ?>
		<?php echo $form->textField($model,'ie_created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ie_date'); ?>
		<?php echo $form->textField($model,'ie_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->