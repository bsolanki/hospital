<?php
/* @var $this IncomeExpenseController */
/* @var $model IncomeExpense */

$this->breadcrumbs=array(
	'Income Expenses'=>array('index'),
	$model->ie_id=>array('view','id'=>$model->ie_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List IncomeExpense', 'url'=>array('index')),
	array('label'=>'Create IncomeExpense', 'url'=>array('create')),
	array('label'=>'View IncomeExpense', 'url'=>array('view', 'id'=>$model->ie_id)),
	array('label'=>'Manage IncomeExpense', 'url'=>array('admin')),
);
?>

<h1>Update IncomeExpense <?php echo $model->ie_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>