<?php
/* @var $this IncomeExpenseController */
/* @var $data IncomeExpense */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ie_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ie_id), array('view', 'id'=>$data->ie_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ie_title')); ?>:</b>
	<?php echo CHtml::encode($data->ie_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ie_rs')); ?>:</b>
	<?php echo CHtml::encode($data->ie_rs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ie_type')); ?>:</b>
	<?php echo CHtml::encode($data->ie_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ie_created_by')); ?>:</b>
	<?php echo CHtml::encode($data->ie_created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ie_date')); ?>:</b>
	<?php echo CHtml::encode($data->ie_date); ?>
	<br />


</div>