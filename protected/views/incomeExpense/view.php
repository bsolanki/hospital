<?php
/* @var $this IncomeExpenseController */
/* @var $model IncomeExpense */

$this->breadcrumbs=array(
	'Income Expenses'=>array('index'),
	$model->ie_id,
);

$this->menu=array(
	array('label'=>'List IncomeExpense', 'url'=>array('index')),
	array('label'=>'Create IncomeExpense', 'url'=>array('create')),
	array('label'=>'Update IncomeExpense', 'url'=>array('update', 'id'=>$model->ie_id)),
	array('label'=>'Delete IncomeExpense', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ie_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage IncomeExpense', 'url'=>array('admin')),
);
?>

<h1>View IncomeExpense #<?php echo $model->ie_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ie_id',
		'ie_title',
		'ie_rs',
		'ie_type',
		'ie_created_by',
		'ie_date',
	),
)); ?>
