<?php
/* @var $this IncomeExpenseController */
/* @var $model IncomeExpense */

$this->breadcrumbs = array(
    'Income Expenses' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List IncomeExpense', 'url' => array('index')),
    array('label' => 'Create IncomeExpense', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#income-expense-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#income-expense-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>

<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade in " >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 id="myModalLabel" class="modal-title">Add Expense</h4>
            </div>
            <div class="modal-body">
                <?php
                $this->renderPartial('_form', array(
                    'model' => $create_model,
                    'type'=>'expense'
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="col-md-10 col-lg-offset-1" >
    <div class="panel panel-white">
        <div class="panel-heading clearfix">
            <h4 class="panel-title">Expense</h4>
            <div class="panel-control">
                <a href="#" class="btn btn-success btn btn-primary" data-target="#myModal" data-toggle="modal" ><i class="fa fa-plus"></i> Add Expense</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'income-expense-grid',
                    'itemsCssClass' => 'table table-hover table-striped',
                    'template' => "{items}\n{summary}{pager}",
                    'dataProvider' => $model->expensesearch(),
//                    'filter' => $model,
                    'columns' => array(
                        array(
                            //'class' => 'editable.EditableColumn',
                            'name' => 'ie_title',
                            'headerHtmlOptions' => array('style' => 'width: 20%'),
//                            'editable' => array(//editable section
//                                //   'apply'      => '$data->user_status != 4', //can't edit deleted users
//                                'url' => $this->createUrl('incomeExpense/inlineedit'),
//                                'placement' => 'right',
//                                'mode' => 'inline'
//                            )
                        ),
                       // 'ie_title',
                        //'ie_type',
                        array(
                            'name' => 'ie_created_by',
                            'value' => '$data->FcaUser->usr_fname',
                        //'type'=>'raw',
                        //'value' => 'CHtml::link($data->fac_name,Yii::app()->createUrl("facility/main",array("id"=>$data->fac_id)))',
                        ),
                        array(
                            'name' => 'ie_date',
                            'value' => 'Utility::timezoneConvert($data->ie_date)',
                        //'type'=>'raw',
                        //'value' => 'CHtml::link($data->fac_name,Yii::app()->createUrl("facility/main",array("id"=>$data->fac_id)))',
                        ),
                        array(
                            'name' => 'ie_rs',
                          //  'class' => 'editable.EditableColumn',
                            'footer' => $model->fetchTotal($model->search()->getKeys(),'EXPENSE'),
//                            'editable' => array(//editable section
//                                //   'apply'      => '$data->user_status != 4', //can't edit deleted users
//                                'url' => $this->createUrl('incomeExpense/inlineedit'),
//                                'placement' => 'right',
//                                'mode' => 'inline'
//                            )
                        //'type'=>'raw',
                        //'value' => 'CHtml::link($data->fac_name,Yii::app()->createUrl("facility/main",array("id"=>$data->fac_id)))',
                        ),
                        array(
                            'header' => 'Actions',
                            'class' => 'CButtonColumn',
                            'template' => '{delete}',
                            'htmlOptions' => array('width' => '8%'),
                            'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Expense?'),
                            'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                            'buttons' => array(
//                                    'edit' => array(
//                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i> Edit'),
//                                        'url' => 'Yii::app()->createUrl("/route/update",array("id"=>$data["route_id"]))',
//                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
//                                        'imageUrl' => false,
//                                    ),
                                'delete' => array(
                                    'label' => Yii::t('common', '<i class="fa fa-remove"></i>'),
                                    'url' => 'Yii::app()->createUrl("/incomeExpense/delete",array("id"=>$data["ie_id"]))',
                                    'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                    'imageUrl' => false,
                                ),
                            ),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>

</div>
</div> 