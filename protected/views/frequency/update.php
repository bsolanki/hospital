<?php

/* @var $this FrequencyController */
/* @var $model Frequency */

$this->breadcrumbs=array(
	'Frequencies'=>array('index'),
	$model->frqcy_id=>array('view','id'=>$model->frqcy_id),
	'Update',
);

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Edit Frequency</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>
