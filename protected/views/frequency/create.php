<?php
/* @var $this FrequencyController */
/* @var $model Frequency */

$this->breadcrumbs=array(
	'Frequencies'=>array('index'),
	'Add',
);

$this->menu=array(
	array('label'=>'List Frequency', 'url'=>array('index')),
	array('label'=>'Manage Frequency', 'url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Add  Frequency</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div> 