<?php
/* @var $this FrequencyController */
/* @var $model Frequency */

$this->breadcrumbs=array(
	'Frequencies'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Frequency', 'url'=>array('index')),
	array('label'=>'Create Frequency', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#frequency-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

function update_grid_view()
{
	$('#frequency-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}

");
?>
<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->

    <div class="col-md-12">

        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Frequencies </h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" class="btn btn-success btn btn-primary add_button" ><i class="fa fa-plus"></i> Add Frequencies </a>
                </div>
			</div>

<div class="panel-body">
<div class="add_form" style="display: none;">
                    <?php
                    $this->renderPartial('_form', array(
                        'model' => $model,
                    ));
                    ?>
                </div>
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'frequency-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
                            // 'dis_id',
//                            array(
//                                'name' => 'frqcy_name',
//                                'htmlOptions' => array('width' => '70%'),
//                                'headerHtmlOptions' => array('width' => '70%'),
//                            ),
                            array(
                                'class' => 'editable.EditableColumn',
                                'name' => 'frqcy_name',
                                'headerHtmlOptions' => array('style' => 'width: 70%'),
                                'editable' => array(//editable section
                                    //   'apply'      => '$data->user_status != 4', //can't edit deleted users
                                    'url' => $this->createUrl('frequency/inlineedit'),
                                    'placement' => 'right',
                                    'mode' => 'inline'
                                )
                            ),
                            array(
                                'name' => 'frqcy_status',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '8%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Frequency?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
//                                    'edit' => array(
//                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i> Edit'),
//                                        'url' => 'Yii::app()->createUrl("/frequency/update",array("id"=>$data["frqcy_id"]))',
//                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
//                                        'imageUrl' => false,
//                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> Delete'),
                                        'url' => 'Yii::app()->createUrl("/frequency/delete",array("id"=>$data["frqcy_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>  
                </div>
            </div>

                </div>
            </div>
