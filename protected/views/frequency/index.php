<?php
/* @var $this FrequencyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Frequencies',
);

$this->menu=array(
	array('label'=>'Create Frequency', 'url'=>array('create')),
	array('label'=>'Manage Frequency', 'url'=>array('admin')),
);
?>

<h1>Frequencies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
