<?php
/* @var $this UnitsController */
/* @var $model Units */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'units-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
        )
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php // echo $form->errorSummary($model);  ?>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'unt_name'); ?>
            <?php echo $form->textField($model, 'unt_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'unt_name'); ?>
        </div>
        <div class="form-group col-lg-2 ">
            <?php echo $form->labelEx($model, 'unt_calculate'); ?><br>
            <?php echo $form->checkBox($model, 'unt_calculate', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'unt_calculate'); ?>
        </div>
        <div class="form-group col-lg-3 clear">
            <?php echo $form->labelEx($model, 'unt_status'); ?>
            <?php echo $form->dropDownList($model, 'unt_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'unt_status'); ?>
        </div>

        <?php $this->endWidget(); ?>


        <div class="buttons form-group col-lg-3 searchbutton">
            <?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
            <!--<a href="<?php echo Yii::app()->createUrl('/units/admin') ?>" class="btn btn-danger" >Cancel</a>=-->

            <?php
            //  FB::setEnabled('false');
            //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');
            echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Submit' : 'Save', $this->createUrl('units/create'), array('type' => 'POST',
                'success' => 'function(data) {
                                 
                                 var response= jQuery.parseJSON(data);

                                 if (response.success ==true){
                                 //alert(response.div);
                                 $("#statusMsg").html("");
                                 $("#statusMsg").append(response.div);
                                 $("#statusMsg").show().delay(3000).hide(0);
                                 $("#units-form")[0].reset();
                                 update_grid_view();
                                 return false;
                                 }else{
                             
data = JSON.parse(data);
                                      $.each(data, function(key, val) {
                        $("#units-form #"+key+"_em_").text(val);                                                    
                        $("#units-form #"+key+"_em_").show();
                        }); 
                                     }

                                 }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>
        </div>


    </div>
</div><!-- form -->