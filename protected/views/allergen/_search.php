<?php
/* @var $this AllergenController */
/* @var $model Allergen */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'alrg_name'); ?>
        <?php echo $form->textField($model, 'alrg_name', array('class' => 'form-control')); ?>
    </div>
    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'alrg_status'); ?>
        <?php echo $form->dropDownList($model, 'alrg_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control' ,'empty' =>'All' )); ?>
       </div>

    <div class="row buttons form-group">
        
        <?php echo CHtml::submitButton('Search' , array('class'=>'btn btn-primary searchbutton')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->