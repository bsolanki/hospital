<?php
/* @var $this AllergenController */
/* @var $model Allergen */

$this->breadcrumbs=array(
	'Allergens'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Allergen', 'url'=>array('index')),
	array('label'=>'Manage Allergen', 'url'=>array('admin')),
);
?>

<h1>Create Allergen</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>