<?php
/* @var $this AllergenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Allergens',
);

$this->menu=array(
	array('label'=>'Create Allergen', 'url'=>array('create')),
	array('label'=>'Manage Allergen', 'url'=>array('admin')),
);
?>

<h1>Allergens</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
