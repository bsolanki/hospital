<?php
/* @var $this AllergenController */
/* @var $model Allergen */

$this->breadcrumbs=array(
	'Allergens'=>array('index'),
	$model->alrg_id=>array('view','id'=>$model->alrg_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Allergen', 'url'=>array('index')),
	array('label'=>'Create Allergen', 'url'=>array('create')),
	array('label'=>'View Allergen', 'url'=>array('view', 'id'=>$model->alrg_id)),
	array('label'=>'Manage Allergen', 'url'=>array('admin')),
);
?>

<h1>Update Allergen <?php echo $model->alrg_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>