<?php
/* @var $this ComplainCategoryController */
/* @var $model ComplainCategory */

$this->breadcrumbs=array(
	'Complain Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ComplainCategory', 'url'=>array('index')),
	array('label'=>'Manage ComplainCategory', 'url'=>array('admin')),
);
?>

<h1>Create ComplainCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>