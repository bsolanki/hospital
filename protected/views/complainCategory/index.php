<?php
/* @var $this ComplainCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Complain Categories',
);

$this->menu=array(
	array('label'=>'Create ComplainCategory', 'url'=>array('create')),
	array('label'=>'Manage ComplainCategory', 'url'=>array('admin')),
);
?>

<h1>Complain Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
