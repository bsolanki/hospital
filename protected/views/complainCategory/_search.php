<?php
/* @var $this ComplainCategoryController */
/* @var $model ComplainCategory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cmpln_catid'); ?>
		<?php echo $form->textField($model,'cmpln_catid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cmpln_catname'); ?>
		<?php echo $form->textField($model,'cmpln_catname',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cmpln_catstatus'); ?>
		<?php echo $form->textField($model,'cmpln_catstatus',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->