<?php
/* @var $this ComplainCategoryController */
/* @var $model ComplainCategory */

$this->breadcrumbs=array(
	'Complain Categories'=>array('index'),
	$model->cmpln_catid=>array('view','id'=>$model->cmpln_catid),
	'Update',
);

$this->menu=array(
	array('label'=>'List ComplainCategory', 'url'=>array('index')),
	array('label'=>'Create ComplainCategory', 'url'=>array('create')),
	array('label'=>'View ComplainCategory', 'url'=>array('view', 'id'=>$model->cmpln_catid)),
	array('label'=>'Manage ComplainCategory', 'url'=>array('admin')),
);
?>

<h1>Update ComplainCategory <?php echo $model->cmpln_catid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>