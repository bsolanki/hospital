<?php
/* @var $this CompanyController */
/* @var $model Company */

$this->breadcrumbs = array(
    'Company' => array('admin'),
    'Add',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Add  Company</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>
