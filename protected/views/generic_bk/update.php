<?php
/* @var $this DisciplineController */
/* @var $model Discipline */

$this->breadcrumbs = array(
    'Generic' => array('admin'),
    'Update',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Edit  Generic</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>
