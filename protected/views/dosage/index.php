<?php
/* @var $this DosageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dosages',
);

$this->menu=array(
	array('label'=>'Create Dosage', 'url'=>array('create')),
	array('label'=>'Manage Dosage', 'url'=>array('admin')),
);
?>

<h1>Dosages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
