<?php
/* @var $this FcAdviceController */
/* @var $model FcAdvice */

$this->breadcrumbs=array(
	'Fc Advices'=>array('index'),
	$model->af_id,
);

$this->menu=array(
	array('label'=>'List FcAdvice', 'url'=>array('index')),
	array('label'=>'Create FcAdvice', 'url'=>array('create')),
	array('label'=>'Update FcAdvice', 'url'=>array('update', 'id'=>$model->af_id)),
	array('label'=>'Delete FcAdvice', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->af_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FcAdvice', 'url'=>array('admin')),
);
?>

<h1>View FcAdvice #<?php echo $model->af_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'af_id',
		'af_cat_id',
		'af_adv_id',
		'af_from',
		'af_to',
		'af_patient_id',
		'af_facility_id',
		'af_modify_by',
	),
)); ?>
