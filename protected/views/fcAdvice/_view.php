<?php
/* @var $this FcAdviceController */
/* @var $data FcAdvice */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->af_id), array('view', 'id'=>$data->af_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_cat_id')); ?>:</b>
	<?php echo CHtml::encode($data->af_cat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_adv_id')); ?>:</b>
	<?php echo CHtml::encode($data->af_adv_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_from')); ?>:</b>
	<?php echo CHtml::encode($data->af_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_to')); ?>:</b>
	<?php echo CHtml::encode($data->af_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_patient_id')); ?>:</b>
	<?php echo CHtml::encode($data->af_patient_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('af_facility_id')); ?>:</b>
	<?php echo CHtml::encode($data->af_facility_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('af_modify_by')); ?>:</b>
	<?php echo CHtml::encode($data->af_modify_by); ?>
	<br />

	*/ ?>

</div>