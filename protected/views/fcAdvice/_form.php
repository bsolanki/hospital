<?php
/* @var $this FcAdviceController */
/* @var $model FcAdvice */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fc-advice-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
        )
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group">
            <div class="col-md-3">
                <ul class="list-unstyled mailbox-nav">
                    <?php
                    $cat_data = CHtml::listData(AdviceCategory::model()->findAllByAttributes(array(), 'ad_fc_id IN (0 , ' . Yii::app()->user->getState('facility') . ')'), 'ad_cat', 'ad_catname');
                    if (!empty($cat_data)) {
                        ?>
                        <li class="active advice_li"><a href="#" class="get_advice" id="0">All Advice</a></li>
                        <?php
                        foreach ($cat_data as $key => $val) {
                            ?>
                            <li class="advice_li" ><a href="#" id="<?php echo $key; ?>" class="get_advice" ><?php echo $val; ?></a></li>
                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-sm-8">
                <?php
                $advice_data = FcAdvice::model()->fetchadvice();

                if (!empty($advice_data)) {
                    ?>
                    <table class="table">
                        <tbody id="advice_list">
                            <?php
                            foreach ($advice_data as $key => $val) {
                                ?>
                                <tr class="read">
                                    <td class="hidden-xs">
                                        <span><input type="checkbox" class="checkbox-mail" name="FcAdvice[af_adv_id][]" value="<?php echo $key; ?>"></span>
                                    </td>
                                    <td>
                                        <?php echo $val; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>


                        </tbody>
                    </table>
                    <?php
                }
                ?>

            </div>
        </div>

    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-3">

            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'af_from'); ?>
                <?php echo $form->textField($model, 'af_from', array('class' => 'form-control date-picker', 'size' => 60, 'maxlength' => 200)); ?>
                <?php echo $form->error($model, 'af_from'); ?>
            </div>

            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'af_to'); ?>
                <?php echo $form->textField($model, 'af_to', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control date-picker')); ?>
                <?php echo $form->error($model, 'af_to'); ?>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group col-sm-12 text-right">
            <?php
            echo CHtml::ajaxSubmitButton('Add Record', $this->createUrl('FcAdvice/create'), array('type' => 'POST',
                'success' => 'function(data) {
                             console.log("Bhavin");
                             var response= jQuery.parseJSON (data);

                             if (response.success ==true){
                                    
                                    validationmsg(response.div);
                                     $("#fc-advice-form")[0].reset();
                                    update_grid_view();
                                    return false;
                             
                             }else{
                             
                                data = JSON.parse(data);
                                $.each(data, function(key, val) {
                                      $("#fc-advice-form #"+key+"_em_").text(val);                                                    
                                      $("#fc-advice-form #"+key+"_em_").show();
                                });      

                             }

                             }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">

    $ = jQuery;

    $('.checkbox-mail').each(function () {
        $(this).click(function () {
            if ($(this).closest('tr').hasClass("checked")) {
                $(this).closest('tr').removeClass('checked');
                //hiddenMailOptions();
            } else {
                $(this).closest('tr').addClass('checked');
                //  hiddenMailOptions();
            }
        });
    });

    $('.date-picker').datepicker({
        orientation: "top auto",
        autoclose: true
    });

    $(".get_advice").click(function () {

        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('advice/fetchadvice') ?>',
            data: {cat_id: $(this).attr('id')},
            type: 'post',
            success: function (output) {

                if (output != '') {

                    $("#advice_list").html('');
                    $.each($.parseJSON(output), function (idx, obj) {
                        //var newBox = '<tr class="read"><td class="hidden-xs"><span><input type="checkbox" class="checkbox-mail" name="FcAdvice[af_adv_id][]" value="'+ idx +'"></span></td><td>'+ obj +'</td></tr>';
                        var newBox = '<tr class="read"><td class="hidden-xs"><span><div class="checker"><span><input type="checkbox" class="checkbox-mail" name="FcAdvice[af_adv_id][]" value="' + idx + '"></span></div></span></td><td>' + obj + '</td></tr>';
                        $("#advice_list").append(newBox).trigger('create');
                    });

                } else {

                    $("#advice_list").html('');
                    $("#advice_list").html('No advice availble for this category.');

                }


            }
        });
    });
</script>