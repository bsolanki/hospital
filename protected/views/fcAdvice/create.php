<?php
/* @var $this FcAdviceController */
/* @var $model FcAdvice */

$this->breadcrumbs=array(
	'Fc Advices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FcAdvice', 'url'=>array('index')),
	array('label'=>'Manage FcAdvice', 'url'=>array('admin')),
);
?>

<h1>Create FcAdvice</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>