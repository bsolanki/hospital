<?php
/* @var $this FcAdviceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fc Advices',
);

$this->menu=array(
	array('label'=>'Create FcAdvice', 'url'=>array('create')),
	array('label'=>'Manage FcAdvice', 'url'=>array('admin')),
);
?>

<h1>Fc Advices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
