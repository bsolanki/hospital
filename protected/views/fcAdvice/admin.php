<?php
/* @var $this FcAdviceController */
/* @var $model FcAdvice */

$this->breadcrumbs = array(
    'Fc Advices' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List FcAdvice', 'url' => array('index')),
    array('label' => 'Create FcAdvice', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fc-advice-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#fc-advice-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-white">
            <div class="clearfix">
                <div class="panel-control text-right">
                    <a href="javascript:void(0);" class="btn btn-success btn btn-primary add_button_advice" ><i class="fa fa-plus"></i></a>
                </div>
            </div>

            <div class="panel-body">
                <div class="add_form_advice" style="display: none;">
                    <?php
                    $this->renderPartial('_form', array(
                        'model' => $model,
                    ));
                    ?>
                </div>
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'fc-advice-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //	'filter'=>$model,
                        'columns' => array(
                            //'af_id',
                            array(
                                'type' => 'raw',
                                'name' => 'af_adv_id',
                                'value' => '(!empty($data->FcadviceToadvice)) ? $data->FcadviceToadvice->adv_desc : "-"',
                            ),
                            array(
                                'type' => 'raw',
                                'name' => 'af_adv_id',
                                'value' => '(!empty($data->FcadviceToadvice->adCat)) ? $data->FcadviceToadvice->adCat->ad_catname : "-"',
                            ),
//                            'af_adv_id',
//                            'af_cat_id',
                            'af_from',
                            'af_to',
                        /*
                          'af_patient_id',
                          'af_facility_id',
                          'af_modify_by',

                          array(
                          'class' => 'CButtonColumn',
                          ), */
                        ),
                    ));
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(".add_button_advice").click(function () {
        $(".add_form_advice").toggle();
    });
    $('select').select2();

</script>