<?php
/* @var $this FcAdviceController */
/* @var $model FcAdvice */

$this->breadcrumbs=array(
	'Fc Advices'=>array('index'),
	$model->af_id=>array('view','id'=>$model->af_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FcAdvice', 'url'=>array('index')),
	array('label'=>'Create FcAdvice', 'url'=>array('create')),
	array('label'=>'View FcAdvice', 'url'=>array('view', 'id'=>$model->af_id)),
	array('label'=>'Manage FcAdvice', 'url'=>array('admin')),
);
?>

<h1>Update FcAdvice <?php echo $model->af_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>