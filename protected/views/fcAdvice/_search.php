<?php
/* @var $this FcAdviceController */
/* @var $model FcAdvice */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'af_id'); ?>
		<?php echo $form->textField($model,'af_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_cat_id'); ?>
		<?php echo $form->textField($model,'af_cat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_adv_id'); ?>
		<?php echo $form->textField($model,'af_adv_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_from'); ?>
		<?php echo $form->textField($model,'af_from',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_to'); ?>
		<?php echo $form->textField($model,'af_to',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_patient_id'); ?>
		<?php echo $form->textField($model,'af_patient_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_facility_id'); ?>
		<?php echo $form->textField($model,'af_facility_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'af_modify_by'); ?>
		<?php echo $form->textField($model,'af_modify_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->