<?php
/* @var $this VitalsController */
/* @var $model Vitals */

$this->breadcrumbs=array(
	'Vitals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Vitals', 'url'=>array('index')),
	array('label'=>'Manage Vitals', 'url'=>array('admin')),
);
?>

<h1>Create Vitals</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>