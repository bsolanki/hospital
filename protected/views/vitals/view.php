<?php
/* @var $this VitalsController */
/* @var $model Vitals */

$this->breadcrumbs=array(
	'Vitals'=>array('index'),
	$model->vi_id,
);

$this->menu=array(
	array('label'=>'List Vitals', 'url'=>array('index')),
	array('label'=>'Create Vitals', 'url'=>array('create')),
	array('label'=>'Update Vitals', 'url'=>array('update', 'id'=>$model->vi_id)),
	array('label'=>'Delete Vitals', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->vi_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Vitals', 'url'=>array('admin')),
);
?>

<h1>View Vitals #<?php echo $model->vi_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'vi_id',
		'vi_patient_id',
		'vi_blood_pressure',
		'vi_heart_rate',
		'vi_respiratory_rate',
		'vi_spo2',
		'vi_temperature',
		'vi_height',
		'vi_weight',
		'vi_BMI',
		'vi_peak_flow_rate',
		'vi_notes',
		'vi_modify_by',
		'vi_modify_date',
	),
)); ?>
