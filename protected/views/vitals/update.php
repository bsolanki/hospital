<?php
/* @var $this VitalsController */
/* @var $model Vitals */

$this->breadcrumbs=array(
	'Vitals'=>array('index'),
	$model->vi_id=>array('view','id'=>$model->vi_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Vitals', 'url'=>array('index')),
	array('label'=>'Create Vitals', 'url'=>array('create')),
	array('label'=>'View Vitals', 'url'=>array('view', 'id'=>$model->vi_id)),
	array('label'=>'Manage Vitals', 'url'=>array('admin')),
);
?>

<h1>Update Vitals <?php echo $model->vi_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>