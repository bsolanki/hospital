<?php
/* @var $this VitalsController */
/* @var $data Vitals */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vi_id), array('view', 'id'=>$data->vi_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_patient_id')); ?>:</b>
	<?php echo CHtml::encode($data->vi_patient_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_blood_pressure')); ?>:</b>
	<?php echo CHtml::encode($data->vi_blood_pressure); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_heart_rate')); ?>:</b>
	<?php echo CHtml::encode($data->vi_heart_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_respiratory_rate')); ?>:</b>
	<?php echo CHtml::encode($data->vi_respiratory_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_spo2')); ?>:</b>
	<?php echo CHtml::encode($data->vi_spo2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_temperature')); ?>:</b>
	<?php echo CHtml::encode($data->vi_temperature); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_height')); ?>:</b>
	<?php echo CHtml::encode($data->vi_height); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_weight')); ?>:</b>
	<?php echo CHtml::encode($data->vi_weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_BMI')); ?>:</b>
	<?php echo CHtml::encode($data->vi_BMI); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_peak_flow_rate')); ?>:</b>
	<?php echo CHtml::encode($data->vi_peak_flow_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_notes')); ?>:</b>
	<?php echo CHtml::encode($data->vi_notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_modify_by')); ?>:</b>
	<?php echo CHtml::encode($data->vi_modify_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vi_modify_date')); ?>:</b>
	<?php echo CHtml::encode($data->vi_modify_date); ?>
	<br />

	*/ ?>

</div>