<?php
/* @var $this VitalsController */
/* @var $model Vitals */

$this->breadcrumbs = array(
    'Vitals' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Vitals', 'url' => array('index')),
    array('label' => 'Create Vitals', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#vitals-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-white">
            <div class="clearfix">
                <div class="panel-control text-right">
                    <a href="javascript:void(0);" class="btn btn-success btn btn-primary add_button_vital" ><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="add_form_vital"  style="display: none"  >
                    <?php
                    $this->renderPartial('application.views.vitals._form', array(
                        'model' => $create_vital,
                    ));
                    ?>
                </div>
            </div>
                <div class="table-responsive">

                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'vitals-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",                        
                        'filter' => $model,
                        'columns' => array(
//                            'vi_id',
//                            'vi_patient_id',
//                            'vi_blood_pressure_1',
                            'vi_heart_rate',
                            'vi_respiratory_rate',
                            'vi_spo2',
                            /*
                              'vi_temperature',
                              'vi_height',
                              'vi_weight',
                              'vi_BMI',
                              'vi_peak_flow_rate',
                              'vi_notes',
                              'vi_modify_by',
                              'vi_modify_date',
                             */
                            array(
                                'class' => 'CButtonColumn',
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(".add_button_vital").click(function () {
        $(".add_form_vital").toggle();
    });
</script>