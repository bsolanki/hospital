<?php
/* @var $this VitalsController */
/* @var $model Vitals */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'vitals-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php // echo $form->errorSummary($model);  ?>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_blood_pressure_1'); ?> <br>
                <div class="col-sm-5" style="padding: 0px;margin: 0px;">
                    <?php echo $form->textField($model, 'vi_blood_pressure_1', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control col-sm-4', 'style' => 'float:left', 'onkeypress' => "return check_digit(event,this,10);")); ?> 
                </div>
                <div class="col-sm-2">
                    /
                </div>
                <div class="col-sm-5" style="padding: 0px;margin: 0px;">
                    <?php echo $form->textField($model, 'vi_blood_pressure_2', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control col-sm-4', 'onkeypress' => "return check_digit(event,this,10);")); ?> 
                </div>


                <?php echo $form->error($model, 'vi_blood_pressure_1'); ?>
            </div>
            <div class="col-sm-4">

            </div>
        </div>

    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_heart_rate'); ?>
                <?php echo $form->textField($model, 'vi_heart_rate', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_heart_rate'); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_respiratory_rate'); ?>
                <?php echo $form->textField($model, 'vi_respiratory_rate', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_respiratory_rate'); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_spo2'); ?>
                <?php echo $form->textField($model, 'vi_spo2', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_spo2'); ?> 
            </div>

        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_spo2'); ?>
                <?php echo $form->textField($model, 'vi_spo2', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_spo2'); ?> 
            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_temperature'); ?>
                <?php echo $form->textField($model, 'vi_temperature', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_temperature'); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_height'); ?>
                <?php echo $form->textField($model, 'vi_height', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_height'); ?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_weight'); ?>
                <?php echo $form->textField($model, 'vi_weight', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_weight'); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_BMI'); ?>
                <?php echo $form->textField($model, 'vi_BMI', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'disabled' => 'disabled', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_BMI'); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $form->labelEx($model, 'vi_peak_flow_rate'); ?>
                <?php echo $form->textField($model, 'vi_peak_flow_rate', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control', 'onkeypress' => "return check_digit(event,this,10);")); ?>
                <?php echo $form->error($model, 'vi_peak_flow_rate'); ?>                
            </div>

        </div>

    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-4 ">
                <?php echo $form->labelEx($model, 'vi_notes'); ?>
                <?php echo $form->textArea($model, 'vi_notes', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'vi_notes'); ?>            
            </div>           
        </div>

    </div>

    <div class="row buttons">
        <div class="col-sm-12 text-right">
            <?php
            echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Add' : 'Save', $this->createUrl('vitals/create'), array('type' => 'POST',
                'success' => 'function(data) {
                            
                             var response= jQuery.parseJSON(data);
                             if (response.success == true ){
                             
                                    validationmsg(response.div);
                                    $("#vitals-form")[0].reset();
                                    $.fn.yiiGridView.update("vitals-grid");
                                    update_grid_view();
                                    return false;
                                    
                             }else{
                             
                                    data = JSON.parse(data);
                                    $.each(data, function(key, val) {
                                            
                                            $("#vitals-form #"+key+"_em_").text(val);                                                    
                                            $("#vitals-form #"+key+"_em_").show();
                                    }); 
                             }

                             }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>
        </div>

    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">

    $("#Vitals_vi_height , #Vitals_vi_weight").change(function () {

        var kg = $("#Vitals_vi_weight").val();

        var height = $("#Vitals_vi_height").val().toString();

        var res = height.split(".");

        if (res[1] != "") {
            res[1] = 0;
        }
        $("#Vitals_vi_BMI").val(kg / ((res[0] * 0.3048 + res[1] * 0.0254) * (res[0] * 0.3048 + res[1] * 0.0254)));







    });
</script>