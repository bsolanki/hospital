<?php
/* @var $this VitalsController */
/* @var $model Vitals */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'vi_id'); ?>
		<?php echo $form->textField($model,'vi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_patient_id'); ?>
		<?php echo $form->textField($model,'vi_patient_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_blood_pressure'); ?>
		<?php echo $form->textField($model,'vi_blood_pressure',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_heart_rate'); ?>
		<?php echo $form->textField($model,'vi_heart_rate',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_respiratory_rate'); ?>
		<?php echo $form->textField($model,'vi_respiratory_rate',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_spo2'); ?>
		<?php echo $form->textField($model,'vi_spo2',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_temperature'); ?>
		<?php echo $form->textField($model,'vi_temperature',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_height'); ?>
		<?php echo $form->textField($model,'vi_height',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_weight'); ?>
		<?php echo $form->textField($model,'vi_weight',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_BMI'); ?>
		<?php echo $form->textField($model,'vi_BMI',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_peak_flow_rate'); ?>
		<?php echo $form->textField($model,'vi_peak_flow_rate',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_notes'); ?>
		<?php echo $form->textField($model,'vi_notes',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_modify_by'); ?>
		<?php echo $form->textField($model,'vi_modify_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vi_modify_date'); ?>
		<?php echo $form->textField($model,'vi_modify_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->