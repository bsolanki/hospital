<?php
/* @var $this VitalsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vitals',
);

$this->menu=array(
	array('label'=>'Create Vitals', 'url'=>array('create')),
	array('label'=>'Manage Vitals', 'url'=>array('admin')),
);
?>

<h1>Vitals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
