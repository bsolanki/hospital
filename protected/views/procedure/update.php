<?php
/* @var $this ProcedureController */
/* @var $model Procedure */

$this->breadcrumbs=array(
	'Procedures'=>array('index'),
	$model->pro_id=>array('view','id'=>$model->pro_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedure', 'url'=>array('index')),
	array('label'=>'Create Procedure', 'url'=>array('create')),
	array('label'=>'View Procedure', 'url'=>array('view', 'id'=>$model->pro_id)),
	array('label'=>'Manage Procedure', 'url'=>array('admin')),
);
?>

<h1>Update Procedure <?php echo $model->pro_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>