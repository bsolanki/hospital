<?php
/* @var $this ProcedureController */
/* @var $model Procedure */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'procedure-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
    )
        ));
?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php // echo $form->errorSummary($model); ?>


	<div class="row">
    
		<div class="form-group col-lg-4 clear">  
		<?php echo $form->labelEx($model,'pro_name'); ?>
		<?php echo $form->textField($model,'pro_name',array('size'=>60,'maxlength'=>255, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'pro_name'); ?>
		</div>

		<div class="form-group col-lg-4 clear"> 
			<?php echo $form->labelEx($model,'pro_status'); ?>
			<?php echo $form->dropDownList($model, 'pro_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'pro_status'); ?>
		</div>
        <div class="buttons form-group col-lg-4 searchbutton">
		 <?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?> 
            <?php
        //  FB::setEnabled('false');
        //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');

        echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Submit' : 'Save', $this->createUrl('procedure/create'), array('type' => 'POST',
            'success' => 'function(data) { 	
//             alert(data);
                             var response= jQuery.parseJSON(data); 
                             if (response.success ==true){                            
				$("#statusMsg").html("");
				$("#statusMsg").append(response.div);
				$("#statusMsg").show().delay(3000).hide(0);
				$("#procedure-form")[0].reset();
				update_grid_view();
				return false;
			    }else{
				data = JSON.parse(data);
				$.each(data, function(key, val) {
				$("#procedure-form #"+key+"_em_").text(val);                                                    
				$("#procedure-form #"+key+"_em_").show();
				});     
			    } 
                             }' //success
                ), array('type' => 'submit', 'class' => 'btn btn-primary')
        );
        ?>

        </div>
    </div> 
    <?php $this->endWidget(); ?>
</div><!-- form -->