<?php
/* @var $this RouteController */
/* @var $model Route */
$this->breadcrumbs = array(
    'Route' => array('admin'),
    'Add',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Add  Route</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>
