<?php
/* @var $this RouteController */
/* @var $model Route */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'route_name'); ?>
        <?php echo $form->textField($model, 'route_name', array('class' => 'form-control')); ?>
    </div>
    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'route_status'); ?>
        <?php echo $form->dropDownList($model, 'route_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control', 'empty' => 'All')); ?>
    </div>

    <div class="row buttons form-group">

        <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary searchbutton')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->