<?php
/* @var $this HclassController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hclasses',
);

$this->menu=array(
	array('label'=>'Create Hclass', 'url'=>array('create')),
	array('label'=>'Manage Hclass', 'url'=>array('admin')),
);
?>

<h1>Hclasses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
