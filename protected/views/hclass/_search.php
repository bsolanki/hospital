<?php
/* @var $this HclassController */
/* @var $model Hclass */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
 
	<div class="form-group col-lg-3">
		<?php echo $form->label($model,'cls_name'); ?>
		<?php echo $form->textField($model, 'cls_name', array('class' => 'form-control')); ?>
	</div>

	
    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'cls_status'); ?>
        <?php echo $form->dropDownList($model, 'cls_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control', 'empty' => 'All')); ?>
    </div>

	<div class="buttons form-group col-lg-3">
        <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary searchbutton')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->