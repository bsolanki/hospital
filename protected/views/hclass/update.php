<?php
/* @var $this HclassController */
/* @var $model Hclass */

$this->breadcrumbs=array(
	'Hclasses'=>array('index'),
	$model->cls_id=>array('view','id'=>$model->cls_id),
	'Update',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Edit Class</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>
