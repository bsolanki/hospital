<?php
/* @var $this HclassController */
/* @var $model Hclass */

$this->breadcrumbs=array(
	'Hclasses'=>array('index'),
	'Add',
);

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Add Class</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>