<?php
/* @var $this HclassController */
/* @var $model Hclass */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'hclass-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
        )
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php //echo $form->errorSummary($model);  ?>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'cls_name'); ?>
            <?php echo $form->textField($model, 'cls_name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'cls_name'); ?>
        </div> 
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'cls_status'); ?>
            <?php echo $form->dropDownList($model, 'cls_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'cls_status'); ?>
        </div> 

        <!--
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
                <a href="<?php echo Yii::app()->createUrl('/hclass/admin') ?>" class="btn btn-danger" >Cancel</a>
        -->
        <div class="buttons form-group col-lg-4 searchbutton">
            <?php
            echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Submit' : 'Save', $this->createUrl('hclass/create'), array('type' => 'POST',
                'success' => 'function(data) {
                    
                            var response= jQuery.parseJSON(data);

                             if (response.success ==true){
                             //alert(response.div);
                             $("#statusMsg").html("");
                             $("#statusMsg").append(response.div);
                             $("#statusMsg").show().delay(3000).hide(0);
                             $("#allergen-form")[0].reset();
                             update_grid_view();
                             return false;
                             }else{
                                data = JSON.parse(data);
                                      $.each(data, function(key, val) {
                        $("#hclass-form #"+key+"_em_").text(val);                                                    
                        $("#hclass-form #"+key+"_em_").show();
                        });     
                                 }

                             }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>

        </div>    

    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->