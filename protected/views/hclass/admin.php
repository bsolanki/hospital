<?php
/* @var $this HclassController */
/* @var $model Hclass */

$this->breadcrumbs=array(
	'Hclasses'=>array('index'),
	'Manage Classes',
);

$this->menu=array(
	array('label'=>'List Hclass', 'url'=>array('index')),
	array('label'=>'Create Hclass', 'url'=>array('create')),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#hclass-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

function update_grid_view()
{
	$('#hclass-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}

");
?>
<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->

 <div class="col-md-12">

        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Class </h4>
                <div class="panel-control">
                    <!--<a href="<?php echo Yii::app()->createUrl('hclass/create'); ?>" class="btn btn-success btn btn-primary" ><i class="fa fa-plus"></i> Add Class </a>-->
                    <a href="#" class="btn btn-success btn btn-primary add_button" ><i class="fa fa-plus"></i> Add Class </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="add_form" style="display: none;">
                    <?php
                    $this->renderPartial('_form', array(
                        'model' => $model,
                    ));
                    ?>
                </div>
                <div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'hclass-grid',
	'dataProvider'=>$model->search(),
	'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        'columns' => array(
//                            array(
//                                'name' => 'cls_name',
//                                'htmlOptions' => array('width' => '70%'),
//                                'headerHtmlOptions' => array('width' => '70%'),
//                            ),
                            array(
                                'class' => 'editable.EditableColumn',
                                'name' => 'cls_name',
                                'headerHtmlOptions' => array('style' => 'width: 70%'),
                                'editable' => array(//editable section
                                    //   'apply'      => '$data->user_status != 4', //can't edit deleted users
                                    'url' => $this->createUrl('hclass/inlineedit'),
                                    'placement' => 'right',
                                    'mode' => 'inline'
                                )
                            ),
                            array(
                                'name' => 'cls_status',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '8%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Class?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
//                                    'edit' => array(
//                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i> Edit'),
//                                        'url' => 'Yii::app()->createUrl("/hclass/update",array("id"=>$data["cls_id"]))',
//                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
//                                        'imageUrl' => false,
//                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> Delete'),
                                        'url' => 'Yii::app()->createUrl("/hclass/delete",array("id"=>$data["cls_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                            
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>

    </div>
</div>