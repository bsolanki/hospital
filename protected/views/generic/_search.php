<?php
/* @var $this GenericController */
/* @var $model Generic */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'gn_name'); ?>
        <?php echo $form->textField($model, 'gn_name', array('class' => 'form-control')); ?>
    </div>


    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'gn_class'); ?>
        <?php echo $form->dropDownList($model, 'gn_class', $model->getclass(), array('class' => 'form-control' , 'empty' =>'All')); ?>
    </div>


    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'gn_unit_id'); ?>
        <?php echo $form->dropDownList($model, 'gn_unit_id', $model->getunit(), array('class' => 'form-control', 'empty' =>'All')); ?>
    </div>


    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'gn_diesease'); ?>
        <?php echo $form->dropDownList($model, 'gn_diesease', $model->getdiesease(), array('class' => 'form-control')); ?>
    </div>


 
    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'gn_status'); ?>
        <?php echo $form->dropDownList($model, 'gn_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control', 'empty' => 'All')); ?>
    </div>

    <div class="buttons form-group col-lg-3">
        <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary searchbutton')); ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- search-form -->