<?php
/* @var $this GenericController */
/* @var $model Generic */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'generic-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php // echo $form->errorSummary($model);  ?>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'gn_name'); ?>
            <?php echo $form->textField($model, 'gn_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'gn_name'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'gn_class'); ?>
            <?php echo $form->dropDownList($model, 'gn_class', $model->getclass(), array('class' => 'form-control' , 'empty'=>'--Select Class--')); ?>
            <?php echo $form->error($model, 'gn_class'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'gn_unit_id'); ?> 
            <?php echo $form->dropDownList($model, 'gn_unit_id', $model->getunit(), array('class' => 'form-control' , 'empty'=>'--Select Unit--')); ?>           
            <?php echo $form->error($model, 'gn_unit_id'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'gn_diesease'); ?>
            <?php
            $this->widget('ext.select2.ESelect2', array(
                'model' => $model,
                'attribute' => 'gn_diesease',
                'data' => $model->getdiesease(),
                'htmlOptions' => array(
                    'multiple' => 'multiple',
                    //'class'=>'form-control',
                    'style' => 'width:100%',
                //'disabled'=>$model->isNewRecord ? false : true
                ),
                'options' => array(
                    'placeholder' => 'diesease'
                ),
            ));
            ?> 
            <?php echo $form->error($model, 'gn_diesease'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'gn_max_dose_day'); ?>
            <?php echo $form->textField($model, 'gn_max_dose_day', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'gn_max_dose_day'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'gn_status'); ?>
            <?php echo $form->dropDownList($model, 'gn_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'gn_status'); ?>
        </div>
    </div>


    <div class="row buttons form-group col-lg-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        <a href="<?php echo Yii::app()->createUrl('/generic/admin') ?>" class="btn btn-danger" >Cancel</a>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->