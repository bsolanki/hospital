<?php
/* @var $this BrandController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_fname'); ?>
            <?php echo $form->textField($model, 'usr_fname', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        </div>

        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_medical_number'); ?>
            <?php echo $form->textField($model, 'usr_medical_number', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        </div>

        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_dis_id'); ?>
            <?php echo $form->dropDownList($model, 'usr_dis_id', $model->getdiscipline(), array('class' => 'form-control', 'empty' => 'All')); ?>
        </div>

        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_status'); ?>
            <?php echo $form->dropDownList($model, 'usr_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control', 'empty' => 'All')); ?>
        </div>   
    </div>
    <div class="row">
        <div class="buttons form-group col-lg-12 right">

            <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary searchbutton')); ?>
        </div> 
    </div>




    <?php $this->endWidget(); ?>

</div><!-- search-form -->