<?php
$this->pageTitle = Yii::app()->name . ' - Change Password';

$this->breadcrumbs = array(
    'Change Password',
);
?>


<div style="margin-top: 100px;">

</div>
<div class="row">
    <div class="col-lg-offset-3 col-md-6">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Change Password</h4>
            </div>
            <div class="panel-body" >

                <div class="form">



                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'change-password-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation' => false,
                        'htmlOptions' => array('class' => 'form-horizontal'),
                    ));
                    ?>  

                    <p class="note">Fields with <span class="required1">*</span> are required.</p>

                    <?php //echo $form->errorSummary($model); ?>
                    <div class="row-fluid row" style="margin-left: 2px">

                        <div class="form-group col-lg-6 clear">
                            <?php echo $form->labelEx($model, 'old_password'); ?>
                            <?php echo $form->passwordField($model, 'old_password', array('class' => 'form-control', 'size' => 25, 'maxlength' => 25, 'autofocus' => 'autofocus')); ?>
                            <?php echo $form->error($model, 'old_password', array('class' => 'required1')); ?>
                        </div>
                    </div>
                    <div class="row-fluid row" style="margin-left: 2px">
                        <div class="form-group col-lg-6 clear">
                            <?php echo $form->labelEx($model, 'new_password'); ?>
                            <?php echo $form->passwordField($model, 'new_password', array('class' => 'form-control', 'size' => 25, 'maxlength' => 25)); ?>
                            <?php echo $form->error($model, 'new_password', array('class' => 'required1')); ?>
                        </div>
                    </div>
                    <div class="row-fluid row" style="margin-left: 2px">

                        <div class="form-group col-lg-6 clear">
                            <?php echo $form->labelEx($model, 'confirm_password'); ?>
                            <?php echo $form->passwordField($model, 'confirm_password', array('class' => 'form-control', 'size' => 25, 'maxlength' => 25)); ?>
                            <?php echo $form->error($model, 'confirm_password', array('class' => 'required1')); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <?php echo CHtml::submitButton(Yii::t('app', 'Submit'), array('class' => 'btn btn-success')); ?>
                            <a href="<?php echo Yii::app()->createUrl('/site/index') ?>" class="btn btn-danger" >Cancel</a>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>

        </div>
    </div><!-- search-form -->
</div>
