<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->usr_id), array('view', 'id'=>$data->usr_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_password')); ?>:</b>
	<?php echo CHtml::encode($data->usr_password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_fname')); ?>:</b>
	<?php echo CHtml::encode($data->usr_fname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_lname')); ?>:</b>
	<?php echo CHtml::encode($data->usr_lname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_type')); ?>:</b>
	<?php echo CHtml::encode($data->usr_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_email')); ?>:</b>
	<?php echo CHtml::encode($data->usr_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_phone')); ?>:</b>
	<?php echo CHtml::encode($data->usr_phone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_address')); ?>:</b>
	<?php echo CHtml::encode($data->usr_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_status')); ?>:</b>
	<?php echo CHtml::encode($data->usr_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_reset_pass_token')); ?>:</b>
	<?php echo CHtml::encode($data->usr_reset_pass_token); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_reset_pass_date')); ?>:</b>
	<?php echo CHtml::encode($data->usr_reset_pass_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_created_by')); ?>:</b>
	<?php echo CHtml::encode($data->usr_created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_modified_by')); ?>:</b>
	<?php echo CHtml::encode($data->usr_modified_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_created_date')); ?>:</b>
	<?php echo CHtml::encode($data->usr_created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_modified_date')); ?>:</b>
	<?php echo CHtml::encode($data->usr_modified_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_last_login')); ?>:</b>
	<?php echo CHtml::encode($data->usr_last_login); ?>
	<br />

	*/ ?>

</div>