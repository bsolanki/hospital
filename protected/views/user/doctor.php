<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Manage Doctor',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

function update_grid_view()
{
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}

");
?>

<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Doctors </h4>

            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'user-grid',
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        'dataProvider' => $model->search(),
//                      /  'filter' => $model,
                        'columns' => array(
                            
                            array(
                                'name' => 'usr_fname',
                                'header'=>'Name',
                                'value'=>'$data->usr_fname . " " . $data->usr_lname',
                                'htmlOptions' => array('width' => '15%'),
                                'headerHtmlOptions' => array('width' => '15%'),
                            ),
                            array(
                                'name' => 'usr_email',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ),
                            array(
                                'name' => 'usr_medical_number',
                                'htmlOptions' => array('width' => '20%'),
                                'headerHtmlOptions' => array('width' => '20%'),
                            ),
                            array(
                                'name' => 'usr_status_medical_council',
                                'htmlOptions' => array('width' => '15%'),
                                'headerHtmlOptions' => array('width' => '15%'),
                            ),
                            array(
                                'name' => 'usr_dis_id',
                                'value'=>'$data->getgridDiscipline()',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ), 
                            array(
                                'name' => 'usr_status',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                            ),
//                            'usr_fname',
//                            'usr_lname',
//                            'usr_type',
//                            'usr_email',
//                            'usr_medical_number',
//                            'usr_status_medical_council',
//                            'usr_dis_id',
                            /*
                              'usr_phone',
                              'usr_address',
                              'usr_status',
                              'usr_reset_pass_token',
                              'usr_reset_pass_date',
                              'usr_created_by',
                              'usr_modified_by',
                              'usr_created_date',
                              'usr_modified_date',
                              'usr_last_login',
                             */
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '10%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to Activate this User?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
//                                    'edit' => array(
//                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i>' ),
//                                        'url' => 'Yii::app()->createUrl("/brand/update",array("id"=>$data["brnd_id"]))',
//                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
//                                        'imageUrl' => false,
//                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-check"></i> Activate '),
                                        'url' => 'Yii::app()->createUrl("/user/activate",array("id"=>$data["usr_id"]))',
                                        'options' => array('class' => 'btn btn-success', 'title' => 'Activate'),
                                        'visible'=>'$data->usr_status == "2"',
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div> 
    <?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->


