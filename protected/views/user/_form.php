<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'action'=>  Yii::app()->createAbsoluteUrl('user/create'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
        )
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'usr_fname'); ?>
            <?php echo $form->textField($model, 'usr_fname', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'usr_fname'); ?>
        </div>
        
    </div>
    
    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'usr_email'); ?>
            <?php echo $form->textField($model, 'usr_email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'usr_email'); ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'usr_type'); ?>
            <?php echo $form->dropDownList($model, 'usr_type', array(''=>'--Select Type--', 'provider' => 'Provider', 'staff' => 'Staff'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'usr_type'); ?>
        </div>
    </div>
    
    <div class="row">
        
        <div class="form-group col-lg-6 clear" style="margin-top: 25px;">
            <?php
            //  FB::setEnabled('false');
            //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');
            echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Create' : 'Save', $this->createUrl('user/create'), array('type' => 'POST',
                'success' => 'function(data) {
                                 var response= jQuery.parseJSON(data);
                                 if (response.success ==true){
                                 $("#statusMsg").html("");
                                 $("#statusMsg").append(response.div);
                                 $("#statusMsg").show().delay(3000).hide(0);
                                 $("#user-form")[0].reset();
                                 update_grid_view();
                                 $("#myModal").modal("toggle");
                                 return false;
                                 }else{
                                 data = jQuery.parseJSON(data);
                                      $.each(data, function(key, val) {
                                        $("#user-form #"+key+"_em_").text(val);                                                    
                                        $("#user-form #"+key+"_em_").show();
                                     });      
                                     }

                                 }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary' , 'id'=>'create')
            );
            ?>
            <a href="#" onclick='$("#myModal").modal("toggle");' >Close</a>
        </div>
    </div>    

    <?php $this->endWidget(); ?>

</div><!-- form -->