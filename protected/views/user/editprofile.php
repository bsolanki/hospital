<?php
$this->pageTitle = Yii::app()->name . ' - Edit Profile';

$this->breadcrumbs = array(
    'Edit Profile'
);
?>
<br><br>
<div class="row">
    <div class="col-md-6 col-lg-offset-6">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Edit Profile</h4>
            </div>
            <div class="panel-body" >



                <div class="form">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'user-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        //                'enableAjaxValidation' => true,
                        //                'clientOptions'=>array(
                        //                        'validateOnSubmit'=>true,
                        //                 ),
                        'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                    ));
                    ?>

                    <p class="note">Fields with <span class="required">*</span> are required.</p>

                    <?php //echo $form->errorSummary($model);  ?>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-8 clear">
                                <?php echo $form->labelEx($model, 'usr_fname'); ?>
                                <?php echo $form->textField($model, 'usr_fname', array('class' => 'form-control clearable', 'size' => 30, 'maxlength' => 25, 'autofocus' => 'autofocus')); ?>
                                <?php echo $form->error($model, 'usr_fname', array('class' => 'required1')); ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-8 clear">
                                <?php echo $form->labelEx($model, 'usr_lname'); ?>
                                <?php echo $form->textField($model, 'usr_lname', array('class' => 'form-control ', 'size' => 25, 'maxlength' => 25)); ?>
                                <?php echo $form->error($model, 'usr_lname', array('class' => 'required1')); ?>                                          
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-8 clear">
                                <?php echo $form->labelEx($model, 'usr_email'); ?>
                                <?php echo $form->textField($model, 'usr_email', array('class' => 'form-control field', 'size' => 100, 'maxlength' => 100)); ?>
                                <?php echo $form->error($model, 'usr_email', array('class' => 'required1')); ?>                                         
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-8 clear">
                                <?php echo $form->labelEx($model, 'usr_phone'); ?>
                                <?php echo $form->textField($model, 'usr_phone', array('class' => 'form-control field', 'size' => 25, 'maxlength' => 15, 'onkeypress' => "return check_digit(event,this,10);")); ?>
                                <?php echo $form->error($model, 'usr_phone', array('class' => 'required1')); ?>                                    
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="control-group">
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-success')); ?>
                            <a href="<?php echo Yii::app()->createUrl('/site/index') ?>" class="btn btn-danger" >Cancel</a>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div> 



            </div>

        </div>
    </div><!-- search-form -->
</div>
