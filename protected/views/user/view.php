<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->usr_id,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->usr_id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->usr_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->usr_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'usr_id',
		'usr_password',
		'usr_fname',
		'usr_lname',
		'usr_type',
		'usr_email',
		'usr_phone',
		'usr_address',
		'usr_status',
		'usr_reset_pass_token',
		'usr_reset_pass_date',
		'usr_created_by',
		'usr_modified_by',
		'usr_created_date',
		'usr_modified_date',
		'usr_last_login',
	),
)); ?>
