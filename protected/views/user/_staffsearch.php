<?php
/* @var $this BrandController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_fname'); ?>
            <?php echo $form->textField($model, 'usr_fname', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        </div>

        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_type'); ?>
            <?php echo $form->dropDownList($model, 'usr_type', array('doctor' => 'Provider', 'staff' => 'Clinic Staff'), array('class' => 'form-control', 'empty' => 'All')); ?>
        </div>
        
        <div class="form-group col-lg-3">
            <?php echo $form->label($model, 'usr_status'); ?>
            <?php echo $form->dropDownList($model, 'usr_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control', 'empty' => 'All')); ?>
        </div>   
        <div class="buttons form-group col-lg-3" style="margin-top: 25px;">

            <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary searchbutton')); ?>
        </div> 
    </div>
    <div class="row">
        
    </div>




    <?php $this->endWidget(); ?>

</div><!-- search-form -->