<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Staff'
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>

<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade in " >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 id="myModalLabel" class="modal-title">Add Staff</h4>
            </div>
            <div class="modal-body">
                <?php
                $this->renderPartial('_form', array(
                    'model' => $create_model,
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_staffsearch', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Manage Staff </h4>
                <div class="panel-control">
                    <a href="#" class="btn btn-success btn btn-primary" data-target="#myModal" data-toggle="modal" ><i class="fa fa-plus"></i> Add Staff </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'user-grid',
                        'dataProvider' => $model->staffsearch(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
                            array(
                                'name' => 'usr_fname',
                                'value' => '$data->usr_fname . " " . $data->usr_lname',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '15%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '15%', 'style' => 'text-align:center;'),
                            ),
                            'usr_type',
                            'usr_email',
                            array(
                                'name' => 'usr_status',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '3%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Staff?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                        'url' => 'Yii::app()->createUrl("/user/delete",array("id"=>$data["usr_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
//                            array(
//                                'header' => 'Options',
//                                'class' => 'CButtonColumn',
//                                'buttons' => array(
//                                    'view' =>
//                                    array(
//                                        'url' => 'Yii::app()->createUrl("user/view", array("id"=>$data->usr_id))',
//                                        'options' => array(
//                                            'ajax' => array(
//                                                'type' => 'POST',
//                                                'url' => "js:$(this).attr('href')",
//                                                'success' => 'function(data) { $("#viewModal .modal-body p").html(data); $("#viewModal").modal(); }'
//                                            ),
//                                        ),
//                                    ),
//                                ),
//                            )
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div> 
