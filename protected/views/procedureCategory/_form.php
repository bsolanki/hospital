<?php
/* @var $this ProcedureCategoryController */
/* @var $model ProcedureCategory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'procedure-category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="form-group col-lg-4 clear"> 
			<?php echo $form->labelEx($model,'pro_catname'); ?>
			<?php echo $form->textField($model,'pro_catname', array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'pro_catname'); ?>
		</div>

		<div class="form-group col-lg-4 clear"> 
			<?php echo $form->labelEx($model,'pro_catstatus'); ?>
			<?php echo $form->dropDownList($model, 'pro_catstatus', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'pro_catstatus'); ?>
		</div>

    <div class="buttons form-group col-lg-4 searchbutton">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?> 
            <?php
        //  FB::setEnabled('false');
        //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');

        echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Submit' : 'Save', $this->createUrl('procedureCategory/create'), array('type' => 'POST',
            'success' => 'function(data) {
                                
                 var response= jQuery.parseJSON(data);

                 if (response.success ==true){
                 $("#statusMsg").html("");
                 $("#statusMsg").append(response.div);
                 $("#statusMsg").show().delay(3000).hide(0);
                 $("#procedure-category-form")[0].reset();
                     update_grid_view();
                     return false;
                 }else{
                    data = JSON.parse(data);
                  	$.each(data, function(key, val) {
			            $("#procedure-category-form #"+key+"_em_").text(val);                                                    
			            $("#procedure-category-form #"+key+"_em_").show();
		            });     
             	}
             }' //success
                ), array('type' => 'submit', 'class' => 'btn btn-primary')
        );
        ?>

        </div>
    </div> 
    <?php $this->endWidget(); ?>
</div><!-- form -->