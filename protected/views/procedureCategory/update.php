<?php
/* @var $this ProcedureCategoryController */
/* @var $model ProcedureCategory */

$this->breadcrumbs=array(
	'Procedure Categories'=>array('index'),
	$model->pro_catid=>array('view','id'=>$model->pro_catid),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProcedureCategory', 'url'=>array('index')),
	array('label'=>'Create ProcedureCategory', 'url'=>array('create')),
	array('label'=>'View ProcedureCategory', 'url'=>array('view', 'id'=>$model->pro_catid)),
	array('label'=>'Manage ProcedureCategory', 'url'=>array('admin')),
);
?>

<h1>Update ProcedureCategory <?php echo $model->pro_catid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>