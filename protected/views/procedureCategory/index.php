<?php
/* @var $this ProcedureCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Procedure Categories',
);

$this->menu=array(
	array('label'=>'Create ProcedureCategory', 'url'=>array('create')),
	array('label'=>'Manage ProcedureCategory', 'url'=>array('admin')),
);
?>

<h1>Procedure Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
