<?php
/* @var $this ProcedureCategoryController */
/* @var $model ProcedureCategory */
/* @var $form CActiveForm */
?>

<div class="wide form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	)); ?>
 
	<div class="form-group col-lg-3"> 
		<?php echo $form->label($model,'pro_catname'); ?>
		<?php echo $form->textField($model,'pro_catname'); ?>
	</div>

	<div class="form-group col-lg-3"> 
		<?php echo $form->label($model,'pro_catstatus'); ?>
		<?php echo $form->dropDownList($model, 'pro_catstatus', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control' ,'empty' =>'All' )); ?>
	</div>

	<div class="row buttons form-group">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- search-form -->