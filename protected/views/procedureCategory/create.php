<?php
/* @var $this ProcedureCategoryController */
/* @var $model ProcedureCategory */

$this->breadcrumbs=array(
	'Procedure Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProcedureCategory', 'url'=>array('index')),
	array('label'=>'Manage ProcedureCategory', 'url'=>array('admin')),
);
?>

<h1>Create ProcedureCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>