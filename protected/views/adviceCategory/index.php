<?php
/* @var $this AdviceCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Advice Categories',
);

$this->menu=array(
	array('label'=>'Create AdviceCategory', 'url'=>array('create')),
	array('label'=>'Manage AdviceCategory', 'url'=>array('admin')),
);
?>

<h1>Advice Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
