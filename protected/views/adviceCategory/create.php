<?php
/* @var $this AdviceCategoryController */
/* @var $model AdviceCategory */

$this->breadcrumbs=array(
	'Advice Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdviceCategory', 'url'=>array('index')),
	array('label'=>'Manage AdviceCategory', 'url'=>array('admin')),
);
?>

<h1>Create AdviceCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>