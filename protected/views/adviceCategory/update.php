<?php
/* @var $this AdviceCategoryController */
/* @var $model AdviceCategory */

$this->breadcrumbs=array(
	'Advice Categories'=>array('index'),
	$model->ad_cat=>array('view','id'=>$model->ad_cat),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdviceCategory', 'url'=>array('index')),
	array('label'=>'Create AdviceCategory', 'url'=>array('create')),
	array('label'=>'View AdviceCategory', 'url'=>array('view', 'id'=>$model->ad_cat)),
	array('label'=>'Manage AdviceCategory', 'url'=>array('admin')),
);
?>

<h1>Update AdviceCategory <?php echo $model->ad_cat; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>