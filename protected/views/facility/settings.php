<?php
$this->pageTitle = Yii::app()->name . ' - Facility Settings';
?>


<div style="margin-top: 100px;">

</div>
<div class="row">
    <div class="col-lg-offset-1 col-md-10">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Facility Settings</h4>
            </div>
            <div class="panel-body">
                <div class="form">



                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'facility-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation' => false,
                        'htmlOptions' => array('class' => 'form-horizontal'),
                    ));
                    ?>  

                    <p class="note">Fields with <span class="required1">*</span> are required.</p>

                    <?php //echo $form->errorSummary($model);  ?>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_name', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_name', array('class' => 'form-control', "placeholder" => "Name")); ?>
                                <?php echo $form->error($model, 'fac_name'); ?>
                            </div>

                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_address', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textArea($model, 'fac_address', array('class' => 'form-control', "placeholder" => "Address")); ?>   
                                <?php echo $form->error($model, 'fac_address'); ?>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_city', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_city', array('class' => 'form-control', "placeholder" => "City")); ?>
                                <?php echo $form->error($model, 'fac_city'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_state', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_state', array('class' => 'form-control', "placeholder" => "State")); ?>
                                <?php echo $form->error($model, 'fac_state'); ?>
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_zip_code', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_zip_code', array('class' => 'form-control', "placeholder" => "ZIP code")); ?>
                                <?php echo $form->error($model, 'fac_zip_code'); ?>
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_country', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_country', array('class' => 'form-control', "placeholder" => "Country")); ?>
                                <?php echo $form->error($model, 'fac_country'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_phone', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_phone', array('class' => 'form-control', "placeholder" => "Phone Number")); ?>
                                <?php echo $form->error($model, 'fac_phone'); ?>
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_fax', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_fax', array('class' => 'form-control', "placeholder" => "Fax")); ?>
                                <?php echo $form->error($model, 'fac_fax'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_email', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_email', array('class' => 'form-control', "placeholder" => "Email")); ?>
                                <?php echo $form->error($model, 'fac_email'); ?>
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_website', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_website', array('class' => 'form-control', "placeholder" => "Website")); ?>
                                <?php echo $form->error($model, 'fac_website'); ?>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_to_timing', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_to_timing', array('class' => 'form-control', "placeholder" => "To Timing")); ?>
                                <?php echo $form->error($model, 'fac_to_timing'); ?>
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model, 'fac_from_timing', array('class' => 'col-sm-3')); ?>
                            <div class="col-sm-7">
                                <?php echo $form->textField($model, 'fac_from_timing', array('class' => 'form-control', "placeholder" => "From Timing")); ?>
                                <?php echo $form->error($model, 'fac_from_timing'); ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-5">
                            <?php echo CHtml::submitButton(Yii::t('app', 'Submit'), array('class' => 'btn btn-success')); ?>
                            <a href="<?php echo Yii::app()->createUrl('/site/index') ?>" class="btn btn-danger" >Cancel</a>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>

        </div>
    </div><!-- search-form -->
</div>
