<?php
/* @var $this FacilityController */
/* @var $model Facility */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#facility-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#facility-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>
<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade in " >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 id="myModalLabel" class="modal-title">Add Staff</h4>
            </div>
            <div class="modal-body">
                <?php
                $this->renderPartial('_staffform', array(
                    'model' => $staff_model,
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-lg-offset-1" >
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Facility </h4>
                <div class="panel-control">
                    <a href="#" class="btn btn-success btn btn-primary" data-target="#myModal" data-toggle="modal" ><i class="fa fa-plus"></i> Add Staff </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'facility-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
                            array(
                                'name'=>'fu_user_id',
                                'value'=>'$data->FcaUser->usr_fname',
                                //'type'=>'raw',
                                //'value' => 'CHtml::link($data->fac_name,Yii::app()->createUrl("facility/main",array("id"=>$data->fac_id)))',
                            ),
//                            'fac_name',
//                            'fac_address',
//                            'fac_city',
//                            'fac_state',
//                            array(
//                                'name' => 'fac_status',
//                                'value' => '$data->displayStatus()',
//                                'type' => 'raw',
//                                'htmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
//                                'headerHtmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
//                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '3%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this User?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                        'url' => 'Yii::app()->createUrl("/user/delete",array("id"=>$data["fu_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div> 
