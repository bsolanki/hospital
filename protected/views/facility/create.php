<?php
/* @var $this FacilityController */
/* @var $model Facility */

$this->breadcrumbs=array(
	'Facilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Facility', 'url'=>array('index')),
	array('label'=>'Manage Facility', 'url'=>array('admin')),
);
?>

<h1>Create Facility</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>