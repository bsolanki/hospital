<?php
/* @var $this FacilityController */
/* @var $model Facility */

$this->breadcrumbs=array(
	'Facilities'=>array('index'),
	$model->fac_id=>array('view','id'=>$model->fac_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Facility', 'url'=>array('index')),
	array('label'=>'Create Facility', 'url'=>array('create')),
	array('label'=>'View Facility', 'url'=>array('view', 'id'=>$model->fac_id)),
	array('label'=>'Manage Facility', 'url'=>array('admin')),
);
?>

<h1>Update Facility <?php echo $model->fac_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>