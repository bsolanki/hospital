<?php
/* @var $this FacilityController */
/* @var $model Facility */

$this->breadcrumbs=array(
	'Facilities'=>array('index'),
	$model->fac_id,
);

$this->menu=array(
	array('label'=>'List Facility', 'url'=>array('index')),
	array('label'=>'Create Facility', 'url'=>array('create')),
	array('label'=>'Update Facility', 'url'=>array('update', 'id'=>$model->fac_id)),
	array('label'=>'Delete Facility', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->fac_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Facility', 'url'=>array('admin')),
);
?>

<h1>View Facility #<?php echo $model->fac_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'fac_id',
		'fac_usr_id',
		'fac_name',
		'fac_address',
		'fac_city',
		'fac_state',
		'fac_zip_code',
		'fac_country',
		'fac_phone',
		'fac_fax',
		'fac_email',
		'fac_website',
		'fac_to_timing',
		'fac_from_timing',
		'fac_status',
	),
)); ?>
