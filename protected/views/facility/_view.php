<?php
/* @var $this FacilityController */
/* @var $data Facility */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fac_id), array('view', 'id'=>$data->fac_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_usr_id')); ?>:</b>
	<?php echo CHtml::encode($data->fac_usr_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_name')); ?>:</b>
	<?php echo CHtml::encode($data->fac_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_address')); ?>:</b>
	<?php echo CHtml::encode($data->fac_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_city')); ?>:</b>
	<?php echo CHtml::encode($data->fac_city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_state')); ?>:</b>
	<?php echo CHtml::encode($data->fac_state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_zip_code')); ?>:</b>
	<?php echo CHtml::encode($data->fac_zip_code); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_country')); ?>:</b>
	<?php echo CHtml::encode($data->fac_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_phone')); ?>:</b>
	<?php echo CHtml::encode($data->fac_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_fax')); ?>:</b>
	<?php echo CHtml::encode($data->fac_fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_email')); ?>:</b>
	<?php echo CHtml::encode($data->fac_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_website')); ?>:</b>
	<?php echo CHtml::encode($data->fac_website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_to_timing')); ?>:</b>
	<?php echo CHtml::encode($data->fac_to_timing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_from_timing')); ?>:</b>
	<?php echo CHtml::encode($data->fac_from_timing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fac_status')); ?>:</b>
	<?php echo CHtml::encode($data->fac_status); ?>
	<br />

	*/ ?>

</div>