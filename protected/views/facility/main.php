<br><br><br>
<div class="row">
    <div class="col-md-8 col-sm-offset-2">
        <div class="panel panel-white">
            <div class="panel-body">
                <div id="rootwizard" class="facility">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"  style="<?php echo ($model->getErrors()) ? "border: red 1px solid" : "" ; ?>" ><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Facility Information</a></li>
                        <li role="presentation"><a href="#tab2" style="<?php echo ($user_model->getErrors()) ? "border: red 1px solid" : "" ; ?>" data-toggle="tab"><i class="fa fa-truck m-r-xs"></i>Provider Details</a></li>
                        <li role="presentation"><a href="#tab3" style="<?php echo ($new_user->getErrors()) ? "border: red 1px solid" : "" ;  ?>"  data-toggle="tab"><i class="fa fa-truck m-r-xs"></i>Staff</a></li>
                        <li role="presentation"><a href="#tab4" style="<?php echo ($service->getErrors()) ? "border: red 1px solid" : "" ;  ?>" data-toggle="tab"><i class="fa fa-check m-r-xs"></i>Service</a></li>
                    </ul>


                    <div class="progress progress-sm m-t-sm">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                        </div>
                    </div>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'wizardForm',
                        
                    ));
                    ?>
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="tab1">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($model, 'fac_name'); ?>
                                    <?php echo $form->textField($model, 'fac_name', array('class' => 'form-control', "placeholder" => "Name")); ?>
                                    <?php echo $form->error($model, 'fac_name'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($model, 'fac_address'); ?>
                                    <?php echo $form->textArea($model, 'fac_address', array('class' => 'form-control', "placeholder" => "Address")); ?>
                                    <?php echo $form->error($model, 'fac_address'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($model, 'fac_city'); ?>
                                    <?php echo $form->textField($model, 'fac_city', array('class' => 'form-control', "placeholder" => "City")); ?>
                                    <?php echo $form->error($model, 'fac_city'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($model, 'fac_state'); ?>
                                    <?php echo $form->textField($model, 'fac_state', array('class' => 'form-control', "placeholder" => "State")); ?>
                                    <?php echo $form->error($model, 'fac_state'); ?>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($model, 'fac_zip_code'); ?>
                                    <?php echo $form->textField($model, 'fac_zip_code', array('class' => 'form-control', "placeholder" => "ZIP code")); ?>
                                    <?php echo $form->error($model, 'fac_zip_code'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($model, 'fac_country'); ?>
                                    <?php echo $form->textField($model, 'fac_country', array('class' => 'form-control', "placeholder" => "Country")); ?>
                                    <?php echo $form->error($model, 'fac_country'); ?>
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="tab2">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($user_model, 'usr_salutation'); ?>
                                    <?php echo $form->dropDownList($new_user, 'usr_type', array('Mr.' => 'Mr.', 'Dr.' => 'Dr.', 'Ms.'=>'Ms.'), array('class' => 'form-control' ,'empty'=>'--Select Salutation--')); ?>
                                    <?php echo $form->error($user_model, 'usr_salutation'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($user_model, 'usr_fname'); ?>
                                    <?php echo $form->textField($user_model, 'usr_fname', array('class' => 'form-control', "placeholder" => "Name")); ?>
                                    <?php echo $form->error($user_model, 'usr_fname'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($user_model, 'usr_education'); ?>
                                    <?php echo $form->textField($user_model, 'usr_education', array('class' => 'form-control', "placeholder" => "Education")); ?>
                                    <?php echo $form->error($user_model, 'usr_education'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($user_model, 'usr_medical_number'); ?>
                                    <?php echo $form->textField($user_model, 'usr_medical_number', array('class' => 'form-control', "placeholder" => "Registration Number")); ?>
                                    <?php echo $form->error($user_model, 'usr_medical_number'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <?php echo $form->labelEx($user_model, 'usr_phone', array('style' => 'width:50%')); ?>
                                    <br>
                                    <?php echo $form->textField($user_model, 'usr_phone', array('class' => 'form-control col-md-6', "placeholder" => "Name", 'style' => 'width:48%')); ?>
                                    <?php echo $form->error($user_model, 'usr_phone'); ?>
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="tab3">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($new_user, 'usr_lname'); ?>
                                    <?php echo $form->textField($new_user, 'usr_lname', array('class' => 'form-control', "placeholder" => "Full Name")); ?>
                                    <?php echo $form->error($new_user, 'usr_lname'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($new_user, 'usr_email1'); ?>
                                    <?php echo $form->textField($new_user, 'usr_email1', array('class' => 'form-control', "placeholder" => "Email Address")); ?>
                                    <?php echo $form->error($new_user, 'usr_email1'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($new_user, 'usr_type'); ?>
                                    <?php echo $form->dropDownList($new_user, 'usr_type', array('provider' => 'Provider', 'staff' => 'Staff'), array('class' => 'form-control' ,'empty'=>'--Select User Type--')); ?>
                                    <?php echo $form->error($new_user, 'usr_type'); ?>
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="tab4">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($service, 'ser_name'); ?>
                                    <?php echo $form->textField($service, 'ser_name', array('class' => 'form-control', "placeholder" => "Service Name")); ?>
                                    <?php echo $form->error($service, 'ser_name'); ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($service, 'ser_duration'); ?>
                                    <?php echo $form->textField($service, 'ser_duration', array('class' => 'form-control', "placeholder" => "Duration")); ?>
                                    <?php echo $form->error($service, 'ser_duration'); ?>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php echo $form->labelEx($service, 'ser_fees', array('style' => 'width:50%')); ?>
                                    <?php echo $form->textField($service, 'ser_fees', array('class' => 'form-control col-md-6', "placeholder" => "Fees")); ?>
                                    <?php echo $form->error($service, 'ser_fees'); ?>
                                </div>
                                <div class="form-group col-md-6 searchbutton">
                                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
                                    <a href="<?php echo Yii::app()->createUrl('/facility/admin') ?>" class="btn btn-danger" >Cancel</a>
                                </div>
                            </div>

                        </div>
                        <ul class="pager wizard">
                            <li class="previous disabled"><a href="#" class="btn btn-default">Previous</a></li>
                            <li class="next"><a href="#" class="btn btn-default">Next</a></li>

                        </ul>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>