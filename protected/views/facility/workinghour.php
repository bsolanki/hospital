<div class="col-md-12">
    <div class="panel panel-white">
        <div class="panel-heading clearfix">
            <h4 class="panel-title">Set working Hours</h4>
        </div>
        <div class="panel-body">
            <div class="table-responsive">

                <div class="form">

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'working-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation' => false,
                    ));
                    ?>
                    <?php echo $form->errorSummary($model);  ?>
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Monday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[monday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[monday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[monday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Tuesday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[tuesday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[tuesday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[tuesday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Wednesday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[wednesday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[wednesday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[wednesday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Thursday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[thursday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[thursday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[thursday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Friday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[friday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[friday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[friday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Saturday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[saturday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[saturday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[saturday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="col-sm-2 col-lg-offset-2">
                            <label class="btn btn-success btn-rounded">Sunday</label>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->checkBox($model, 'weekday[sunday][status]'); ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[sunday][start_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->dropDownList($model, 'weekday[sunday][end_time]', $model->gettime(), array('class' => 'form-control')); ?> 
                        </div>
                    </div>
                    
                    <div class="row-fluid col-sm-offset-8 col-sm-2">
                        <?php echo CHtml::submitButton('Save Changes', array('class' => 'btn btn-primary')); ?>
                    </div>

                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>
    </div>
</div> 
