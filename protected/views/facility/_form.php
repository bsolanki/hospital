<?php
/* @var $this FacilityController */
/* @var $model Facility */
/* @var $form CActiveForm */
?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'facility-form',
        'action' => Yii::app()->createAbsoluteUrl('facility/create'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
        )
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'fac_name'); ?>
            <?php echo $form->textField($model, 'fac_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'fac_name'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-6 clear" style="margin-top: 25px;">
            <?php
            //  FB::setEnabled('false');
            //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');
            echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Create' : 'Save', $this->createUrl('facility/create'), array('type' => 'POST',
                'success' => 'function(data) {
                                 var response= jQuery.parseJSON(data);
                                 if (response.success ==true){
                                 //alert(response.div);
                                 $("#statusMsg").html("");
                                 $("#statusMsg").append(response.div);
                                 $("#statusMsg").show().delay(3000).hide(0);
                                 $("#facility-form")[0].reset();
                                 update_grid_view();
                                 $("#myModal").modal("toggle");
                                 return false;
                                 }else{
                                 data = JSON.parse(data);
                                      $.each(data, function(key, val) {
                        $("#facility-form #"+key+"_em_").text(val);                                                    
                        $("#facility-form #"+key+"_em_").show();
                        });      
                                     }

                                 }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>
            
            <a href="#" onclick='$("#myModal").modal("toggle");' >Close</a>
            
        </div>

    </div>    


    <?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    $('#Facility_fac_to_timing').timepicker();
    $('#Facility_fac_from_timing').timepicker();
    
</script> 