<?php
/* @var $this AdviceController */
/* @var $model Advice */

$this->breadcrumbs=array(
	'Advices'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Advice', 'url'=>array('index')),
	array('label'=>'Create Advice', 'url'=>array('create')),
);
 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
}); 
");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});


$('.search-form form').submit(function(){
	$('#advice-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
 
function update_grid_view()
{
	$('#advice-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>

<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>

<div class="row">
    <div class="col-md-12" style="display: none;">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->


    <div class="col-md-12">
    <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Advice </h4>
                <div class="panel-control">                    
                    <a href="#" class="btn btn-success btn btn-primary add_button"  title=""  data-toggle="tooltip" data-placement="top" data-original-title="Add New Advice"  ><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
               <div class="add_form" style="display: none;">
                    <?php
                    
                    $this->renderPartial('_form', array(
                        'model' => $model,
                    ));
                    ?>
                </div>
               <div class="table-responsive">
				<?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'advice-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //  'filter' => $model,
                        'columns' => array(
                            array(
                                'class' => 'editable.EditableColumn',
                                'name' => 'adv_desc',
                                'headerHtmlOptions' => array('style' => 'width: 50%'),
                                'editable' => array(//editable section
                                    //   'apply'      => '$data->user_status != 4', //can't edit deleted users
                                    'url' => $this->createUrl('advice/inlineedit'),
                                    'placement' => 'right',
                                    'mode' => 'inline'
                                )
                            ),
                            array(
                            	'class' => 'editable.EditableColumn',
                               'name' => 'adv_cat_id',
                              // 'value' => '$data->brndVacine->brnd_name', 
                                'htmlOptions' => array('width' => '30%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '30%', 'style' => 'text-align:center;'),
                                'editable' => array(//editable section
                                    //   'apply'      => '$data->user_status != 4', //can't edit deleted users
                                     'type'     => 'select',
                                    'url' => $this->createUrl('advice/inlineedit'),
                                    'placement' => 'right',
                                    'mode' => 'inline',
                                    'source'    => $model->getAdviceCategory(),
                                )
                           ),
                            array(
                                'name' => 'adv_status',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '20%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '20%', 'style' => 'text-align:center;'),
                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '20%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Complain?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
                                    'edit' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i>'),
                                        'url' => 'Yii::app()->createUrl("/advice/update",array("id"=>$data["adv_id"]))',
                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
                                        'imageUrl' => false,
                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i>'),
                                        'url' => 'Yii::app()->createUrl("/advice/delete",array("id"=>$data["adv_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php
/*
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advice-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'adv_id',
		'adv_cat_id',
		'adv_desc',
		'adv_status',
		array(
			'class'=>'CButtonColumn',
		),
	),
));
*/
?>
