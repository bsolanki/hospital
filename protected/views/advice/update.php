<?php
/* @var $this AdviceController */
/* @var $model Advice */

$this->breadcrumbs=array(
	'Advices'=>array('index'),
	$model->adv_id=>array('view','id'=>$model->adv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Advice', 'url'=>array('index')),
	array('label'=>'Create Advice', 'url'=>array('create')),
	array('label'=>'View Advice', 'url'=>array('view', 'id'=>$model->adv_id)),
	array('label'=>'Manage Advice', 'url'=>array('admin')),
);
?>

<h1>Update Advice <?php echo $model->adv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>