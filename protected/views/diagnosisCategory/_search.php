<?php
/* @var $this DiagnosisCategoryController */
/* @var $model DiagnosisCategory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group col-lg-3"> 
		<?php echo $form->label($model,'dia_catname'); ?>
		<?php echo $form->textField($model,'dia_catname',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dia_catstatus'); ?>
		<?php echo $form->dropDownList($model, 'dia_catstatus', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control' ,'empty' =>'All' )); ?>
	</div>

	<div class="row buttons form-group">        
        <?php echo CHtml::submitButton('Search' , array('class'=>'btn btn-primary searchbutton')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->