<?php
/* @var $this DiagnosisCategoryController */
/* @var $model DiagnosisCategory */

$this->breadcrumbs=array(
	'Diagnosis Categories'=>array('index'),
	$model->dia_catid=>array('view','id'=>$model->dia_catid),
	'Update',
);

$this->menu=array(
	array('label'=>'List DiagnosisCategory', 'url'=>array('index')),
	array('label'=>'Create DiagnosisCategory', 'url'=>array('create')),
	array('label'=>'View DiagnosisCategory', 'url'=>array('view', 'id'=>$model->dia_catid)),
	array('label'=>'Manage DiagnosisCategory', 'url'=>array('admin')),
);
?>

<h1>Update DiagnosisCategory <?php echo $model->dia_catid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>