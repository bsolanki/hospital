<?php
/* @var $this DiagnosisCategoryController */
/* @var $model DiagnosisCategory */

$this->breadcrumbs=array(
	'Diagnosis Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DiagnosisCategory', 'url'=>array('index')),
	array('label'=>'Manage DiagnosisCategory', 'url'=>array('admin')),
);
?>

<h1>Create DiagnosisCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>