<?php
/* @var $this DiagnosisCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Diagnosis Categories',
);

$this->menu=array(
	array('label'=>'Create DiagnosisCategory', 'url'=>array('create')),
	array('label'=>'Manage DiagnosisCategory', 'url'=>array('admin')),
);
?>

<h1>Diagnosis Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
