<?php
/* @var $this DiagnosisCategoryController */
/* @var $model DiagnosisCategory */

$this->breadcrumbs=array(
	'Diagnosis Categories'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DiagnosisCategory', 'url'=>array('admin')),
	array('label'=>'Create DiagnosisCategory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#diagnosis-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#diagnosis-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>
<div id="statusMsg">   

<?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div class="row">
 
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->
    <div class="col-md-12">

        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Diagnosis Category</h4>
                <div class="panel-control">
                    <a href="#" class="btn btn-success btn btn-primary add_button" ><i class="fa fa-plus"></i> Add Diagnosis Category </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="add_form" style="display: none;">
                    <?php
                    $this->renderPartial('_form', array(
                        'model' => $model,
                    ));
                    ?>	
                </div> 
                <div class="table-responsive">
                <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'diagnosis-category-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
                           
                            array(
                                'class' => 'editable.EditableColumn',
                                'name' => 'dia_catname',
                                'headerHtmlOptions' => array('style' => 'width: 40%'),
                                'editable' => array(//editable section
                                    //   'apply'      => '$data->user_status != 4', //can't edit deleted users
                                    'url' => $this->createUrl('diagnosiscategory/inlineedit'),
                                    'placement' => 'right',
                                    'mode' => 'inline'
                                ) 
                            ),
                            array(
                                'name' => 'dia_catstatus',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '30%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '30%', 'style' => 'text-align:center;'),
                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{delete}',
                                'htmlOptions' => array('width' => '30%'),
                                'headerHtmlOptions' => array('width' => '30%', 'style' => 'text-align:left;'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Diagnosis Category?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array( 
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i>'),
                                        'url' => 'Yii::app()->createUrl("/diagnosiscategory/delete",array("id"=>$data["dia_catid"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>  
