<?php
/* @var $this ExaminationController */
/* @var $model Examination */

$this->breadcrumbs=array(
	'Examinations'=>array('index'),
	$model->exam_id=>array('view','id'=>$model->exam_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Examination', 'url'=>array('index')),
	array('label'=>'Create Examination', 'url'=>array('create')),
	array('label'=>'View Examination', 'url'=>array('view', 'id'=>$model->exam_id)),
	array('label'=>'Manage Examination', 'url'=>array('admin')),
);
?>

<h1>Update Examination <?php echo $model->exam_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>