<?php
/* @var $this ExaminationController */
/* @var $model Examination */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group col-lg-3"> 
		<?php echo $form->label($model,'exam_name'); ?>
		<?php echo $form->textField($model,'exam_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="form-group col-lg-3"> 
		<?php echo $form->label($model,'exam_status'); ?>
		<?php echo $form->dropDownList($model, 'exam_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control' ,'empty' =>'All' )); ?>
	</div>
	
    <div class="row buttons form-group">        
        <?php echo CHtml::submitButton('Search' , array('class'=>'btn btn-primary searchbutton')); ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- search-form -->