<?php
/* @var $this ServicesController */
/* @var $data Services */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ser_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ser_id), array('view', 'id'=>$data->ser_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ser_name')); ?>:</b>
	<?php echo CHtml::encode($data->ser_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ser_duration')); ?>:</b>
	<?php echo CHtml::encode($data->ser_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ser_fees')); ?>:</b>
	<?php echo CHtml::encode($data->ser_fees); ?>
	<br />


</div>