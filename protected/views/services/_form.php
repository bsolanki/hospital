<?php
/* @var $this ServicesController */
/* @var $model Services */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'services-form',
        'action' => Yii::app()->createAbsoluteUrl('services/create'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnBlur'=>false,
            'validateOnType'=>false,
            'validationDelay'=> 1500
        //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
        )
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php // echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'ser_name'); ?>
            <?php echo $form->textField($model, 'ser_name', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'ser_name'); ?>
        </div>

    </div>

    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'ser_duration'); ?>
            <?php echo $form->textField($model, 'ser_duration', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'ser_duration'); ?>
        </div>

    </div>

    <div class="row">
        <div class="form-group col-lg-12 clear">
            <?php echo $form->labelEx($model, 'ser_fees'); ?>
            <?php echo $form->textField($model, 'ser_fees', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'ser_fees'); ?>
        </div>

    </div>

    <div class="row">
        <div class="form-group col-lg-6 clear" style="margin-top: 25px;">
            <?php
            //  FB::setEnabled('false');
            //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');
            echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Create' : 'Save', $this->createUrl('services/create'), array('type' => 'POST',
                'success' => 'function(data) {
                                 var response= jQuery.parseJSON(data);
                                 if (response.success ==true){
                                 //alert(response.div);
                                 $("#statusMsg").html("");
                                 $("#statusMsg").append(response.div);
                                 $("#statusMsg").show().delay(3000).hide(0);
                                 $("#services-form")[0].reset();
                                 update_grid_view();
                                 $("#myModal").modal("toggle");
                                 return false;
                                 }else{
                                 data = JSON.parse(data);
                                      $.each(data, function(key, val) {
                        $("#services-form #"+key+"_em_").text(val);                                                    
                        $("#services-form #"+key+"_em_").show();
                        });      
                                     }

                                 }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>

            <a href="#" onclick='$("#myModal").modal("toggle");' >Close</a>

        </div>


        <?php $this->endWidget(); ?>
    </div>
    </div><!-- form -->