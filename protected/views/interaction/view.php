<?php
/* @var $this InteractionController */
/* @var $model Interaction */

$this->breadcrumbs=array(
	'Interactions'=>array('index'),
	$model->intr_id,
);

$this->menu=array(
	array('label'=>'List Interaction', 'url'=>array('index')),
	array('label'=>'Create Interaction', 'url'=>array('create')),
	array('label'=>'Update Interaction', 'url'=>array('update', 'id'=>$model->intr_id)),
	array('label'=>'Delete Interaction', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->intr_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Interaction', 'url'=>array('admin')),
);
?>

<h1>View Interaction #<?php echo $model->intr_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'intr_id',
		'intr_generic_1',
		'intr_generic_2',
		'intr_severity_of_interaction',
		'intr_mechanisam',
		'intr_commnets',
	),
)); ?>
