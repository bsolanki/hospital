<?php
/* @var $this InteractionController */
/* @var $model Interaction */

$this->breadcrumbs=array(
	'Interactions'=>array('admin'),
	'Add',
);
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Add Interaction</h4>
            </div>
            <div class="panel-body" >
                
                <?php $this->renderPartial('_form', array('model' => $model)); ?>
                
            </div>

        </div>
    </div><!-- search-form -->
</div>