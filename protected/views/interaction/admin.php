<?php
/* @var $this InteractionController */
/* @var $model Interaction */

$this->breadcrumbs=array(
	'Interactions'=>array('index'),
	'Manage Interactions',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#interaction-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#interaction-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>
<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div><!-- search-form -->

   <div class="col-md-12">
    <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Interaction </h4>
                <div class="panel-control">
                    <a href="<?php echo Yii::app()->createUrl('interaction/create'); ?>" class="btn btn-success btn btn-primary" ><i class="fa fa-plus"></i> Add Interaction </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'interaction-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
                           
                            array(
                                'name' => 'intr_generic_1',
                                'value'=>'$data->getGenericDetails(1)',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ),
                            array(
                                'name' => 'intr_generic_2',
                                'value'=>'$data->getGenericDetails(0)',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ),
                            array(
                                'name' => 'intr_severity_of_interaction',                                                             
                                'htmlOptions' => array('width' => '20%'),
                                'headerHtmlOptions' => array('width' => '20%'),
                            ),
                            array(
                                'name' => 'intr_mechanisam',                                
                                'htmlOptions' => array('width' => '25%'),
                                'headerHtmlOptions' => array('width' => '25%'),
                            ),
                            array(
                                'name' => 'intr_commnets',                                
                                'htmlOptions' => array('width' => '25%'),
                                'headerHtmlOptions' => array('width' => '25%'),
                            ),                           
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{edit} {delete}',
                                'htmlOptions' => array('width' => '10%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this interaction?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
                                    'edit' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i>' ),
                                        'url' => 'Yii::app()->createUrl("/interaction/update",array("id"=>$data["intr_id"]))',
                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
                                        'imageUrl' => false,
                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                        'url' => 'Yii::app()->createUrl("/interaction/delete",array("id"=>$data["intr_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div> 