<?php
/* @var $this InteractionController */
/* @var $model Interaction */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'intr_id'); ?>
		<?php echo $form->textField($model,'intr_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'intr_generic_1'); ?>
		<?php echo $form->textField($model,'intr_generic_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'intr_generic_2'); ?>
		<?php echo $form->textField($model,'intr_generic_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'intr_severity_of_interaction'); ?>
		<?php echo $form->textField($model,'intr_severity_of_interaction',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'intr_mechanisam'); ?>
		<?php echo $form->textArea($model,'intr_mechanisam',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'intr_commnets'); ?>
		<?php echo $form->textArea($model,'intr_commnets',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->