<?php
/* @var $this InteractionController */
/* @var $model Interaction */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'interaction-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
	  <div class="form-group col-lg-4 clear">
	      <?php echo $form->labelEx($model, 'intr_generic_1'); ?>	     
	      <?php echo $form->dropDownList($model, 'intr_generic_1', $model->getGeneric(), array('class' => 'form-control', 'empty' => '--Select Generic--', 'style' => '')); ?> 
	      <?php echo $form->error($model, 'intr_generic_1'); ?>
	  </div>
	  <div class="form-group col-lg-4 clear">
	     <?php echo $form->labelEx($model, 'intr_generic_2'); ?>
	     <?php echo $form->dropDownList($model, 'intr_generic_2', $model->getGeneric(), array('class' => 'form-control', 'empty' => '--Select Generic--', 'style' => '')); ?>
	     <?php echo $form->error($model, 'intr_generic_2'); ?>
	  </div>
	</div>
	
	<div class="row">
	  <div class="form-group col-lg-4 clear">
	      <?php echo $form->labelEx($model, 'intr_severity_of_interaction'); ?>
	      <?php echo $form->textField($model, 'intr_severity_of_interaction', array('size' => 60, 'maxlength' => 500, 'class' => 'form-control')); ?>
	      <?php echo $form->error($model, 'intr_severity_of_interaction'); ?>
	  </div>
	  <div class="form-group col-lg-4 clear">
	      <?php echo $form->labelEx($model, 'intr_mechanisam'); ?>
	      <?php echo $form->textArea($model, 'intr_mechanisam', array('class' => 'form-control' ,'row'=>'10' , 'style'=>'height:70px;')); ?>
	      <?php echo $form->error($model, 'intr_mechanisam'); ?>
	  </div>
	</div>	


	<div class="row">
	  <div class="form-group col-lg-4 clear">
	    <?php echo $form->labelEx($model, 'intr_commnets'); ?>
	    <?php echo $form->textArea($model, 'intr_commnets', array('class' => 'form-control' ,'row'=>'10' , 'style'=>'height:70px;')); ?>
	    <?php echo $form->error($model, 'intr_commnets'); ?>
	  </div>		
	</div>	
	
	<div class="row buttons form-group col-lg-12">
	  <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
	  <a href="<?php echo Yii::app()->createUrl('/interaction/admin') ?>" class="btn btn-danger" >Cancel</a>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->