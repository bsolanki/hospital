<?php
/* @var $this InteractionController */
/* @var $data Interaction */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('intr_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->intr_id), array('view', 'id'=>$data->intr_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intr_generic_1')); ?>:</b>
	<?php echo CHtml::encode($data->intr_generic_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intr_generic_2')); ?>:</b>
	<?php echo CHtml::encode($data->intr_generic_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intr_severity_of_interaction')); ?>:</b>
	<?php echo CHtml::encode($data->intr_severity_of_interaction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intr_mechanisam')); ?>:</b>
	<?php echo CHtml::encode($data->intr_mechanisam); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intr_commnets')); ?>:</b>
	<?php echo CHtml::encode($data->intr_commnets); ?>
	<br />


</div>