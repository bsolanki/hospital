<?php
/* @var $this DisciplineController */
/* @var $model Discipline */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'dis_name'); ?>
        <?php echo $form->textField($model, 'dis_name', array('class' => 'form-control')); ?>
    </div>
    <div class="form-group col-lg-3">
        <?php echo $form->label($model, 'dis_status'); ?>
        <?php echo $form->dropDownList($model, 'dis_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control' ,'empty' =>'All' )); ?>
       </div>

    <div class="row buttons form-group">
        
        <?php echo CHtml::submitButton('Search' , array('class'=>'btn btn-primary searchbutton')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->