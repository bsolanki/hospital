<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Error',
);
?>


<div class="col-md-4 center" style="min-height: 400px;margin-top: 200px;">
    <h1 class="text-xxl text-primary text-center">404</h1>
    <div class="details">
        <h3>Oops ! Something went wrong</h3>
        <p>We can't find the page you're looking for. Return <a href="<?php Yii::app()->createAbsoluteUrl('site/index'); ?>">Home</a> or search.</p>
    </div>
</div>
