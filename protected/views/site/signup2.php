<?php
$this->pageTitle = Yii::app()->name . ' - Clinic information';
?>

<body class="page-register profile-cover">
    <div id="main-wrapper">
        <div class="row">
            <div class="col-md-9 center">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div id="rootwizard">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="#tab1" data-toggle="tab" style="pointer-events: none; cursor: default;" ><i class="fa fa-user m-r-xs"></i>Login Info</a></li>
                                <li role="presentation" class="active"><a href="#" data-toggle="tab" style="pointer-events: none; cursor: default;"><i class="fa fa-truck m-r-xs"></i>Clinic Info</a></li>
                                <li role="presentation"><a href="#" data-toggle="tab" style="pointer-events: none; cursor: default;"><i class="fa fa-check m-r-xs"></i>Finish</a></li>
                            </ul>


                            <div class="progress progress-sm m-t-sm">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="tab1">
                                    <div class="row m-b-lg">
                                        <div class="col-md-6 center">
                                            <div class="row">
                                                <?php
                                                $form = $this->beginWidget('CActiveForm', array(
                                                    'id' => 'signup-form',
                                                    'enableClientValidation' => false,
                                                ));
                                                ?>
                                                <div class="form-group col-md-12">
                                                    <?php echo $form->labelEx($model, 'usr_dis_id'); ?>
                                                    <br><br>
                                                    <?php
                                                    $this->widget('ext.select2.ESelect2', array(
                                                        'model' => $model,
                                                        'attribute' => 'usr_dis_id',
                                                        'data' => $model->getdiscipline(),
                                                        'htmlOptions' => array(
                                                            'multiple' => 'multiple',
                                                            //'class'=>'form-control'
                                                            'style' => 'width:100%',
                                                        //'disabled'=>$model->isNewRecord ? false : true
                                                        ),
                                                        'options' => array(
                                                            'placeholder' => 'Discipline'
                                                        ),
                                                    ));
                                                    ?> 
                                                    <?php echo $form->error($model, 'usr_dis_id'); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <?php echo $form->labelEx($model, 'usr_medical_number'); ?>
                                                    <?php echo $form->textField($model, 'usr_medical_number', array('class' => 'form-control', "placeholder" => "Medical Registration Number")); ?>
                                                    <?php echo $form->error($model, 'usr_medical_number'); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <?php echo $form->labelEx($model, 'usr_status_medical_council'); ?>
                                                    <?php echo $form->textField($model, 'usr_status_medical_council', array('class' => 'form-control', "placeholder" => "State Medical Council")); ?>
                                                    <?php echo $form->error($model, 'usr_status_medical_council'); ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <ul class="pager wizard">
                                    <li class="previous"><?php echo CHtml::submitButton('Previous', array('class' => 'btn btn-default', 'style' => '  display: inline-block;  padding: 5px 14px;  background-color: #fff;  border: 1px solid #ddd;  border-radius: 15px;float:left')); ?></li>
                                    <li class="next"><?php echo CHtml::submitButton('Next', array('class' => 'btn btn-default', 'style' => '  display: inline-block;  padding: 5px 14px;  background-color: #fff;  border: 1px solid #ddd;  border-radius: 15px;float:right')); ?></li>
                                </ul>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>