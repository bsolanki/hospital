
<!DOCTYPE html>
<html>
    <head>
        <!-- Title -->
        <title>Modern | Main Page</title>
        <!-- Styles -->
        
        <?php $baseUrl = Yii::app()->theme->baseUrl;  ?>
<!--        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:500,400,300' rel='stylesheet' type='text/css'>-->
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/animate/animate.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/tabstylesinspiration/css/tabs.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/tabstylesinspiration/css/tabstyles.css" rel="stylesheet" type="text/css">	
        <link href="<?php echo $baseUrl; ?>/landing/assets/plugins/pricing-tables/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $baseUrl; ?>/landing/assets/css/landing.css" rel="stylesheet" type="text/css"/>
        
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/pricing-tables/js/modernizr.js"></script>
     
    </head>
    <body data-spy="scroll" data-target="#header">
               
        <div class="home" id="home">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="home-text col-md-8">
                        <h1 class="wow fadeInDown" data-wow-delay="1.5s" data-wow-duration="1.5s" data-wow-offset="10">Hospital</h1>
                        <p class="lead wow fadeInDown" data-wow-delay="2s" data-wow-duration="1.5s" data-wow-offset="10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<br>Aenean commodo ligula eget dolor.</p>
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('site/signup'); ?>" class="btn btn-default btn-rounded btn-lg wow fadeInUp" data-wow-delay="2.5s" data-wow-duration="1.5s" data-wow-offset="10">Sign Up</a>
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('site/login'); ?>" class="btn btn-success btn-rounded btn-lg wow fadeInUp" data-wow-delay="2.5s" data-wow-duration="1.5s" data-wow-offset="10">Login</a>
                    </div>
                    
                </div>
            </div>
        </div>
	 <!-- Javascripts -->
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/pace-master/pace.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/wow/wow.min.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/tabstylesinspiration/js/cbpfwtabs.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/plugins/pricing-tables/js/main.js"></script>
        <script src="<?php echo $baseUrl; ?>/landing/assets/js/landing.js"></script>
        
    </body>
</html>