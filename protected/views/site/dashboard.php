<?php $baseUrl = Yii::app()->theme->baseUrl; ?>
<style type="text/css">
    .profile-image-container {
        position:relative;
        display: inline-block
    }
    .profile-image-container:hover .edit{display:block}
    .edit{
        position : absolute;
        display:none;
        top:60%; 
        width:40px;
        margin:0 auto;
        right:0px;
        z-index:100
    } 
</style>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade in" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 id="myModalLabel" class="modal-title">Upload Profile Image</h4>
            </div>
            <div class="modal-body">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'Image-form',
                    'action' => Yii::app()->createAbsoluteUrl('site/profilepic'),
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                    ),
                ));
                ?>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'usr_profile_pic', array('class' => 'col-sm-3 control-label')); ?>

                    <div class="col-sm-5">
                        <?php echo CHtml::activeFileField($model, 'usr_profile_pic'); ?>     
                        <?php echo $form->error($model, 'usr_profile_pic'); ?>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <?php echo CHtml::link('Upload', '#', array("onclick" => "upload_file()")); ?>

            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<div class="profile-cover">
    <div class="row">
        <div class="col-md-3 profile-image">
            <div class="profile-image-container">
                <img src="<?php echo $baseUrl; ?>/assets/images/profile-picture.png" alt="">
                <div class="edit"><a href="#" data-target="#myModal" data-toggle="modal" ><i class="fa fa-picture-o fa-lg"></i></a></div>
            </div>
        </div>
        <div class="col-md-12 profile-info">
            <div class=" profile-info-value">
                <h3>20</h3>
                <p>Staff Member</p>
            </div>
            <div class=" profile-info-value">
                <h3>8</h3>
                <p>Patient</p>
            </div>
        </div>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-3 user-profile">
            <h3 class="text-center"><?php echo $model->usr_fname . " " . $model->usr_lname; ?></h3>
            <p class="text-center"><?php echo $model->usr_type; ?></p>
            <hr>
            <ul class="list-unstyled text-center">
                <li><p><i class="fa fa-map-marker m-r-xs"></i><a href="tel:<?php echo $model->usr_phone ?>"><?php echo $model->usr_phone; ?></a></p></li>
                <li><p><i class="fa fa-envelope m-r-xs"></i><a href="mail:<?php echo $model->usr_email ?>"><?php echo $model->usr_email ?></a></p></li>
            </ul>
            <hr>
            <a href="<?php echo Yii::app()->createAbsoluteUrl('user/editprofile') ?>" class="btn btn-primary btn-block" ><i class="fa fa-plus m-r-xs"></i>Edit Profile</a>
        </div>

        <div class="col-md-6 m-t-lg">  

            <div class="panel-body todo" style="clear:both;">
                <form action="javascript:void(0);">
                    <input type="text" name="add_task_txt" id="add_task_txt" placeholder="New Task..." class="form-control add-task" />
                </form> 
                <div class="row buttons form-group col-lg-12 hide_ele">
                    <input type="button" value="Save" id="add_task_btn" name="add_task_btn" class="btn btn-primary" />
                </div>

                <div class="todo-list" id="todo_list" style="clear:both;">
                    
                </div>
                <!--
               <input type="checkbox" id="all-complete">
               <span>Mark all as complete</span>
               -->
            </div>

        </div>
   
        <div class="col-md-3 m-t-lg">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <div class="panel-title">Team</div>
                </div>
                <div class="panel-body">
                    <div class="team">
                        <div class="team-member">
                            <div class="online on"></div>
                            <img src="assets/images/avatar1.png" alt="">
                        </div>
                        <div class="team-member">
                            <div class="online off"></div>
                            <img src="assets/images/avatar2.png" alt="">
                        </div>
                        <div class="team-member">
                            <div class="online on"></div>
                            <img src="assets/images/avatar3.png" alt="">
                        </div>
                        <div class="team-member">
                            <div class="online on"></div>
                            <img src="assets/images/avatar5.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-white">
                <div class="panel-heading">
                    <div class="panel-title">Some Info</div>
                </div>
                <div class="panel-body">
                    <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">    

        function upload_file() {

            var fd = new FormData();
            var file_data = $('#User_usr_profile_pic').prop('files');
            fd.append('file', file_data);

            $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl('site/profilepic') ?>",
                type: 'POST',
                cache: false,
                data: fd,
                processData: false,
                contentType: false,
                success: function (data) {

                },
                error: function () {
                    alert("ERROR in upload");
                }
            });
        }

        
    $("#add_task_btn").click(function () {
         var add_task_txt = $('#add_task_txt').val();
        $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl('site/addtask') ?>",
                type: 'POST',
                cache: false,
                data: "task_name="+add_task_txt, 
                success: function (data) {
                    if(data=="empty"){
                        alert("Please enter task details ");
                    }
                    else{
                        alert("Task successfully added ");   
                        gettasklist(); 
                        $('#add_task_txt').val("");
                    }
                },
                error: function () {
                    alert("ERROR in upload");
                }
            });
    });
       

     function removetask(obj){
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('site/deletetask') ?>",
            type: 'POST',
            cache: false, 
            data:"taskid="+$(obj).parent().find("input").val(),
            success: function (data) { 
                $(obj).parent().remove();  
                alert("Task deleted successfully") 
            },
            error: function () {
                alert("ERROR in fetching task list");
            }
        });
    }

    function gettasklist(){

        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('site/listtask') ?>",
            type: 'POST',
            cache: false, 
            success: function (data) {
                $("#todo_list").html(data);
            },
            error: function () {
                alert("ERROR in fetching task list");
            }
        });   
    }

    gettasklist();
    </script>


    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        ?>
        <script type="text/javascript">
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                    timeOut: 5000
                };
                toastr.success('<?php echo $message; ?>', '');
            }, 1000);
        </script>    
        <?php
    }
    ?> 