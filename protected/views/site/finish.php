<?php
$this->pageTitle = Yii::app()->name . ' - Finish Registartion';
?>
<body class="page-register profile-cover">
    <div id="main-wrapper">
        <div class="row">
            <div class="col-md-9 center">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div id="rootwizard">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="#tab1" data-toggle="tab" style="pointer-events: none; cursor: default;" ><i class="fa fa-user m-r-xs"></i>Login Info</a></li>
                                <li role="presentation"><a href="#" data-toggle="tab" style="pointer-events: none; cursor: default;"><i class="fa fa-truck m-r-xs"></i>Clinic Info</a></li>
                                <li role="presentation" class="active"><a href="#" data-toggle="tab" style="pointer-events: none; cursor: default;"><i class="fa fa-check m-r-xs"></i>Finish</a></li>
                            </ul>


                            <div class="progress progress-sm m-t-sm">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="tab1">
                                     <h2 class="no-s">Thank You !</h2>
                                                    <div role="alert" class="alert alert-info m-t-sm m-b-lg">
                                                        Congratulations ! User has been added successfully we will verify registration number and then will send you link on  register email.
                                                    </div>
                                <ul class="pager wizard">
                                    <li class="next"><a class="btn btn-default" href="<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>">Go to Home page</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>