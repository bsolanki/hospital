<?php 

$this->pageTitle = Yii::app()->name . ' - Login information';
?>
<style type="text/css">
	li.activeerror{border:1px solid red;}
	li.activeerror a, li.activeerror.active  a{color: red;}
</style>
<body class="page-register">


    <div class="navbar">
            <div class="navbar-inner">
                <div class="sidebar-pusher">
                    <a class="waves-effect waves-button waves-classic push-sidebar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="logo-box">
                    <a class="logo-text" href=""><span><?php echo Yii::app()->name; ?></span></a>
                </div><!-- Logo Box -->
                <div class="container topmenu-outer">
                    <div class="top-menu"> 


        <div class="nav navbar-nav navbar-right hidden-xs">
                <p class="navbar-text text-right">
                    <a href="<?php echo Yii::app()->createUrl('/site/login'); ?>">Login</a>
                </p>
            </div>

                    </div><!-- Top Menu -->
                </div>
            </div>
        </div>



<div class="page-inner">

    <div id="main-wrapper">
        <div class="row">




            <div class="col-md-4 center">
                <div class="login-box">
  
<p class="text-center m-t-md lead">Create a free <?php echo Yii::app()->name; ?>'s account</p>
 
                    <div class="panel-body">
                        <div id="rootwizard o-signup-form-labels eo-tabset">
                            <ul class="nav nav-tabs nav-justified" id="signup_info_tabs" role="tablist">
                                <li role="presentation" id="tab1_lnk" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Login Info</a></li>
                                <li role="presentation" id="tab2_lnk" ><a href="#tab2" data-toggle="tab" style="border-left: none;" ><i class="fa fa-truck m-r-xs"></i>Medical Info</a></li>
                            </ul>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'wizardForm',
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                //  'afterSubmit'=>'js:yiiFix.ajaxSubmit.afterValidate'
                                )
                            ));
                            ?>
                            <div class="tab-content">
                                <div class="tab-pane active fade in <?php if ($model->hasErrors('usr_fname') !== null || $model->hasErrors('usr_email') !== null || $model->hasErrors('usr_password') !== null) echo "error"; ?>" id="tab1">
                                    <div class="row m-b-lg">
                                        <div class="col-md-12 center">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <?php // echo $form->labelEx($model, 'usr_fname'); ?>
                                                    <?php echo $form->textField($model, 'usr_fname', array('class' => 'form-control', "placeholder" => "Name")); ?>
                                                    <?php echo $form->error($model, 'usr_fname'); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <?php //echo $form->labelEx($model, 'usr_email'); ?>
                                                    <?php echo $form->textField($model, 'usr_email', array('class' => 'form-control', "placeholder" => "Email")); ?>
                                                    <?php echo $form->error($model, 'usr_email'); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <?php // echo $form->labelEx($model, 'usr_password'); ?>
                                                    <?php echo $form->passwordField($model, 'usr_password', array('class' => 'form-control', "placeholder" => "Password")); ?>
                                                    <?php echo $form->error($model, 'usr_password'); ?>
                                                </div> 
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade in <?php if ($model->hasErrors('usr_dis_id') !== null || $model->hasErrors('usr_medical_number') !== null || $model->hasErrors('usr_status_medical_council') !== null) echo "error"; ?>" id="tab2">
                                    <div class="row m-b-lg">
                                        <div class="col-md-12 center">
                                            <div class="row">

                                                <div class="form-group col-md-12">
                                                    <?php // echo $form->labelEx($model, 'usr_dis_id'); ?>
                                                    <?php
                                                    $this->widget('ext.select2.ESelect2', array(
                                                        'model' => $model,
                                                        'attribute' => 'usr_dis_id',
                                                        'data' => $model->getdiscipline(),
                                                        'htmlOptions' => array(
                                                            'multiple' => 'multiple',
                                                            //'class'=>'form-control'
                                                            'style' => 'width:100%',
                                                        //'disabled'=>$model->isNewRecord ? false : true
                                                        ),
                                                        'options' => array(
                                                            'placeholder' => 'Discipline'
                                                        ),
                                                    ));
                                                    ?> 
                                                    <?php echo $form->error($model, 'usr_dis_id'); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <?php //echo $form->labelEx($model, 'usr_medical_number'); ?>
                                                    <?php echo $form->textField($model, 'usr_medical_number', array('class' => 'form-control', "placeholder" => "Medical Registration Number")); ?>
                                                    <?php echo $form->error($model, 'usr_medical_number'); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <?php //echo $form->labelEx($model, 'usr_status_medical_council'); ?>
                                                    <?php echo $form->textField($model, 'usr_status_medical_council', array('class' => 'form-control', "placeholder" => "State Medical Council")); ?>
                                                    <?php echo $form->error($model, 'usr_status_medical_council'); ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <?php echo CHtml::submitButton('Register', array('class' => 'btn btn-success col-md-12 center')); ?>
                                </div>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">
	
	$(document).ready(function(){
	    $(".btn-success").click(function(){
	        if($("#User_usr_dis_id_em_.errorMessage").html()!="" || $("#User_usr_medical_number_em_.errorMessage").html()!="" || $("#User_usr_status_medical_council_em_.errorMessage").html()!=""){
	        	$("#tab2").toggleClass("activeerror");
	        	$("#tab2_lnk").toggleClass("activeerror");
	      	}
	      	else{
	      		$("#tab2").toggleClass("activeerror");
	      		$("#tab2_lnk").toggleClass("activeerror");
	      	}
	      	if($("#User_usr_fname_em_.errorMessage").html()!="" || $("#User_usr_email_em_.errorMessage").html()!="" || $("#User_usr_password_em_.errorMessage").html()!=""){
	      		$("#tab1").toggleClass("activeerror");
	      		$("#tab1_lnk").toggleClass("activeerror");
	      	}
	      	else{
	      		$("#tab1").toggleClass("activeerror");
	      		$("#tab1_lnk").toggleClass("activeerror");
	      	}	
	    });
	});   

</script>
</body>
