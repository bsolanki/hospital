<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Reset Password';
$this->breadcrumbs = array(
    'Reset Password',
);
$baseUrl = Yii::app()->theme->baseUrl;
?>
<body class="page-login login-alt">
    <main class="page-content">
        <div class="page-inner">
            <div id="main-wrapper">
                <div class="row">
                    <div class="col-md-6 center">
                        <div class="login-box panel panel-white">
                            <div class="panel-body">
                                <div id="statusMsg">    
                                    <?php
                                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                                        echo '<div class="alert alert-' . $key . '">'
                                        . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                        . $message . "</div>\n";
                                    }
                                    ?>    
                                </div>
                                <?php
                                Yii::app()->clientScript->registerScript(
                                        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");;', CClientScript::POS_READY
                                );
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="index.html" class="logo-name text-lg">Modern</a>
                                        <p class="login-info">The Modern UI Framework is a premium Web Application Admin Dashboard built on top of Twitter Bootstrap 3.3.4 Framework.<br> It was created to be the most functional, clean and well designed theme for any types of backend applications.We have carefully designed all common elements.</p>
                                        <div class="btn-group btn-group-justified m-t-sm" role="group" aria-label="Justified button group">
                                            <a href="#" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                                            <a href="#" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
                                            <a href="#" class="btn btn-google"><i class="fa fa-google-plus"></i> Google+</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        $form = $this->beginWidget('CActiveForm', array(
                                            'id' => 'reset-password-form',
                                            // Please note: When you enable ajax validation, make sure the corresponding
                                            // controller action is handling ajax validation correctly.
                                            // See class documentation of CActiveForm for details on this,
                                            // you need to use the performAjaxValidation()-method described there.
                                            'enableAjaxValidation' => false,
                                        ));
                                        ?>
                                        <div class="form-group">
                                            <?php echo $form->passwordField($model, 'new_password' , array('class'=>'form-control' ,  "placeholder"=>"New Password" )); ?>
                                            <?php echo $form->error($model, 'new_password'); ?>
                                        </div>
                                        
                                        <div class="form-group">
                                            <?php echo $form->passwordField($model, 'confirm_password' , array('class'=>'form-control' ,  "placeholder"=>"Retype Password" )); ?>
                                            <?php echo $form->error($model, 'confirm_password'); ?>
                                        </div>
                                        
                                        <?php echo CHtml::submitButton('Reset Password', array('class' => 'btn btn-success btn-block')); ?>
                                        <a href="<?php echo Yii::app()->createUrl('site/login'); ?>" class="display-block text-center m-t-md text-sm">Return to login page</a>
                                        <?php $this->endWidget(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Row -->
            </div><!-- Main Wrapper -->
        </div><!-- Page Inner -->
    </main><!-- Page Content -->


