<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>

<body class="page-login login-alt">
    
    <div class="navbar">
            <div class="navbar-inner">
                <div class="sidebar-pusher">
                    <a class="waves-effect waves-button waves-classic push-sidebar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="logo-box">
                    <a class="logo-text" href=""><span><?php echo Yii::app()->name; ?></span></a>
                </div><!-- Logo Box -->
                <div class="container topmenu-outer">
                    <div class="top-menu"> 


        <div class="nav navbar-nav navbar-right hidden-xs">
                <p class="navbar-text text-right">
                    <a href="<?php echo Yii::app()->createUrl('/site/Signup'); ?>">Sign up</a>
                </p>
            </div>

                    </div><!-- Top Menu -->
                </div>
            </div>
        </div>

    <main class="page-content">
        <div class="page-inner">
            <div id="main-wrapper">
                <div class="row">
                    <div class="col-md-4 center">
                       

<div class="login-box">
    <p class="text-center m-t-md" style="font-size: 22px;">Please login into your account.</p><br/>
    <div id="statusMsg">    
        <?php
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . '">'
            . '<button type="button" class="close" data-dismiss="alert">×</button>'
            . $message . "</div>\n";
        }
        ?>    
    </div>
    <?php
        Yii::app()->clientScript->registerScript(
                'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");;', CClientScript::POS_READY
        );
        ?>
    <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
    ?>
        <div class="form-group">
            <?php echo $form->textField($model, 'username', array('class' => 'form-control', "required" => "", "placeholder" => "Email")); ?>
            <?php echo $form->error($model, 'username'); ?>
        </div>
 
        <div class="form-group">
            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', "required" => "", "placeholder" => "Password")); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>
        
<div class="form-inline">
                <div class="form-group m-lg-right hidden-xs">
                   
   <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-block btn-primary p-lg-left-right')); ?>
  
                </div>

                <div class="form-group pull-right"> 
 <strong>
        <a href="<?php echo Yii::app()->createUrl('site/lostpassword'); ?>" class="display-block">Forgot Password?</a>
    </strong> 

                </div>
            </div>

<?php 
    /*
        <div class="form-inline">
            <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-success btn btn-block btn-primary p-lg-left-right')); ?>
            <a href="<?php echo Yii::app()->createUrl('site/lostpassword'); ?>" class="display-block m-t-md text-sm">Forgot Password?</a>
        </div>
    
        <p class="text-center m-t-xs text-sm">Do not have an account?</p>
        <a href="<?php echo Yii::app()->createUrl('site/Signup'); ?>" class="btn btn-default btn-block m-t-md">Create an account</a>
     */ 
?>
        <?php $this->endWidget(); ?>

        <p class="text-center m-t-xs text-sm"><?php echo date('Y'); ?> &copy; <?php echo Yii::app()->name; ?>.</p>
</div>

                    </div>
                </div><!-- Row -->
            </div><!-- Main Wrapper -->
        </div><!-- Page Inner -->





    </main><!-- Page Content -->



