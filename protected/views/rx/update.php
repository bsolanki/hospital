<?php
/* @var $this RxController */
/* @var $model Rx */

$this->breadcrumbs=array(
	'Rxes'=>array('index'),
	$model->rx_id=>array('view','id'=>$model->rx_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Rx', 'url'=>array('index')),
	array('label'=>'Create Rx', 'url'=>array('create')),
	array('label'=>'View Rx', 'url'=>array('view', 'id'=>$model->rx_id)),
	array('label'=>'Manage Rx', 'url'=>array('admin')),
);
?>

<h1>Update Rx <?php echo $model->rx_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>