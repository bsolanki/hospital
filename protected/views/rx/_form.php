<?php
/* @var $this RxController */
/* @var $model Rx */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'rx-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'rx_brand_id'); ?>
            <?php
            $this->widget('ext.select2.ESelect2', array(
                'model' => $model,
                'attribute' => 'rx_brand_id',
                'data' => $model->getbrand(),
                'htmlOptions' => array(
//                                'multiple' => 'multiple',
                    //'class' => 'form-control',
                    'style' => 'width:100%',
                //'disabled'=>$model->isNewRecord ? false : true
                ),
                'options' => array(
                    'placeholder' => 'Select Brand'
                ),
            ));
            ?>
            <?php echo $form->error($model, 'rx_brand_id'); ?>
        </div>

    </div>

    <div class="row">
        <div class="form-group col-lg-2  text-center clear">
            <?php echo $form->labelEx($model, 'rx_dose'); ?><br>
            <?php echo $form->textField($model, 'rx_dose', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200, 'style' => 'width:32%;margin-right:2%;')); ?>
            <?php
                $this->widget('ext.select2.ESelect2', array(
                    'model' => $model,
                    'attribute' => 'rx_unit',
                    'data' => CHtml::listData(Units::model()->findAll(), 'unt_id', 'unt_name') ,
                    'htmlOptions' => array(
    //                                'multiple' => 'multiple',
                        //'class' => 'form-control',
                        'style' => 'width:50%',
                    //'disabled'=>$model->isNewRecord ? false : true
                    ),
                    'options' => array(
                        'placeholder' => 'Select unit'
                    ),
                ));
            ?>
            <?php echo $form->error($model, 'rx_dose'); ?>
            <?php echo $form->labelEx($model, 'rx_duration'); ?><br>
            <?php echo $form->textField($model, 'rx_duration', array('class' => 'form-control', 'size' => 20, 'maxlength' => 200, 'style' => 'margin-left:10px;')); ?>
            <?php echo $form->error($model, 'rx_duration'); ?>
        </div>
        
        <div class="form-group col-lg-2 clear">
            <?php echo $form->labelEx($model, 'rx_frequency'); ?><br>
            <?php echo $form->dropDownList($model, 'rx_frequency', CHtml::listData(Frequency::model()->findAll(), 'frqcy_id', 'frqcy_name'), array('size' => 4, 'maxlength' => 200 ,'class'=>'select2')); ?>
            <?php echo $form->error($model, 'rx_frequency'); ?>
        </div>
        <div class="form-group col-lg-3 clear">
            <?php echo $form->labelEx($model, 'rx_instruction'); ?><br>
            <?php echo $form->dropDownList($model, 'rx_instruction', CHtml::listData(Instruction::model()->findAll(), 'inst_id', 'inst_name'), array('size' => 4, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'rx_instruction'); ?>
        </div>
        <div class="form-group col-lg-2 clear">
            <?php echo $form->labelEx($model, 'rx_duration'); ?><br>
            <?php echo $form->textField($model, 'rx_duration', array('class' => 'form-control', 'size' => 20, 'maxlength' => 200, 'style' => 'margin-left:10px;')); ?>
            <?php echo $form->error($model, 'rx_duration'); ?>
        </div>
        <div class="form-group col-lg-2 clear">
            <?php echo $form->labelEx($model, 'rx_quantity'); ?><br>
            <?php echo $form->textField($model, 'rx_quantity', array('class' => 'form-control', 'size' => 20, 'maxlength' => 200)); ?>
            <?php echo $form->error($model, 'rx_quantity'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'rx_condition'); ?><br>
            <?php
            $this->widget('ext.select2.ESelect2', array(
                'model' => $model,
                'attribute' => 'rx_condition',
                'data' => CHtml::listData(Disease::model()->findAll(), 'dis_id', 'dis_name'),
                'htmlOptions' => array(
                    //'multiple' => 'multiple',
                    //'class' => 'form-control',
                    'style' => 'width:100%',
                //'disabled'=>$model->isNewRecord ? false : true
                ),
                'options' => array(
                    'placeholder' => 'Select Condition'
                ),
            ));
            ?>
            <?php echo $form->error($model, 'rx_condition'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'rx_note'); ?><br>
            <?php echo $form->textArea($model, 'rx_note', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200)); ?>
            <?php echo $form->error($model, 'rx_note'); ?>
        </div>

    </div>

    <div class="row">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title" class="panel-collapse">Advance</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none">
                <div class="form-group col-lg-4 clear">
                    <?php echo $form->labelEx($model, 'rx_refill'); ?><br>
                    <?php echo $form->textField($model, 'rx_refill', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200)); ?>
                    <?php echo $form->error($model, 'rx_refill'); ?>
                </div>
                <div class="form-group col-lg-4 clear">
                    <?php echo $form->labelEx($model, 'rx_set_time'); ?><br>
                    <?php echo $form->textField($model, 'rx_set_time', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200)); ?>
                    <?php echo $form->error($model, 'rx_set_time'); ?>
                </div>
                <div class="form-group col-lg-4 clear">
                    <?php echo $form->labelEx($model, 'rx_refill_date'); ?><br>
                    <?php echo $form->textField($model, 'rx_refill_date', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200)); ?>
                    <?php echo $form->error($model, 'rx_refill_date'); ?>
                </div>
                <div class="form-group col-lg-4 clear">
                    <?php echo $form->labelEx($model, 'rx_start_date'); ?><br>
                    <?php echo $form->textField($model, 'rx_start_date', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200)); ?>
                    <?php echo $form->error($model, 'rx_start_date'); ?>
                </div>
                <div class="form-group col-lg-4 clear">
                    <?php echo $form->labelEx($model, 'rx_end_date'); ?><br>
                    <?php echo $form->textField($model, 'rx_end_date', array('size' => 60, 'class' => 'form-control col-lg-2', 'maxlength' => 200)); ?>
                    <?php echo $form->error($model, 'rx_end_date'); ?>
                </div>
                <div class="form-group col-lg-4 clear">
                    <?php echo $form->labelEx($model, 'rx_therapy'); ?><br>
                    <?php
                    $this->widget('ext.select2.ESelect2', array(
                        'model' => $model,
                        'attribute' => 'rx_therapy',
                        'data' => array('Acute' => 'Acute', 'Maintenance' => 'Maintenance'),
                        'htmlOptions' => array(
                            //                                'multiple' => 'multiple',
                            //'class' => 'form-control',
                            'style' => 'width:100%',
                        //'disabled'=>$model->isNewRecord ? false : true
                        ),
                        'options' => array(
                            'placeholder' => 'Select Therapy'
                        ),
                    ));
                    ?>
                    <?php echo $form->error($model, 'rx_therapy'); ?>
                </div>
            </div>


        </div>
    </div>
    
    <div class="row">
        <div class="form-group col-lg-6 clear" style="margin-top: 25px;">
            <?php
            //  FB::setEnabled('false');
            //  echo  CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save');
            echo CHtml::ajaxSubmitButton('Add', $this->createUrl('rx/create'), array('type' => 'POST',
                'success' => 'function(data) {
                                 var response= jQuery.parseJSON(data);
                                 if (response.success ==true){
                                 validationmsg("Rx has been created successfully.");
                                 if(response.commnet != ""){
                                     Faildedmsg(response.comment);
                                 }
                                 $("#rx-form")[0].reset();
                                 update_grid_view();
                                 //Faildedmsg
                                 return false;
                                 }else{
                                 data = JSON.parse(data);
                                     $.each(data, function(key, val) {
                                        $("#"+key+"_em_").text(val);                                                    
                                        $("#"+key+"_em_").show();
                                     });      
                                     }

                                 }' //success
                    ), array('type' => 'submit', 'class' => 'btn btn-primary')
            );
            ?>

        </div>
    </div>    
    <?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    $(".add_button_rx").click(function () {
        $(".add_form_rx").toggle();
    });
    $('select').select2();
</script>