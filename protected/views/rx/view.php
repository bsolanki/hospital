<?php
/* @var $this RxController */
/* @var $model Rx */

$this->breadcrumbs=array(
	'Rxes'=>array('index'),
	$model->rx_id,
);

$this->menu=array(
	array('label'=>'List Rx', 'url'=>array('index')),
	array('label'=>'Create Rx', 'url'=>array('create')),
	array('label'=>'Update Rx', 'url'=>array('update', 'id'=>$model->rx_id)),
	array('label'=>'Delete Rx', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->rx_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Rx', 'url'=>array('admin')),
);
?>

<h1>View Rx #<?php echo $model->rx_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'rx_id',
		'rx_brand_id',
		'rx_dose',
		'rx_frequency',
		'rx_instruction',
		'rx_duration',
		'rx_quantity',
		'rx_condition',
		'rx_note',
		'rx_preview',
		'rx_refill',
		'rx_refill_date',
		'rx_start_date',
		'rx_end_date',
		'rx_therapy',
	),
)); ?>
