<?php
/* @var $this RxController */
/* @var $data Rx */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rx_id), array('view', 'id'=>$data->rx_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_brand_id')); ?>:</b>
	<?php echo CHtml::encode($data->rx_brand_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_dose')); ?>:</b>
	<?php echo CHtml::encode($data->rx_dose); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_frequency')); ?>:</b>
	<?php echo CHtml::encode($data->rx_frequency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_instruction')); ?>:</b>
	<?php echo CHtml::encode($data->rx_instruction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_duration')); ?>:</b>
	<?php echo CHtml::encode($data->rx_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_quantity')); ?>:</b>
	<?php echo CHtml::encode($data->rx_quantity); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_condition')); ?>:</b>
	<?php echo CHtml::encode($data->rx_condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_note')); ?>:</b>
	<?php echo CHtml::encode($data->rx_note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_preview')); ?>:</b>
	<?php echo CHtml::encode($data->rx_preview); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_refill')); ?>:</b>
	<?php echo CHtml::encode($data->rx_refill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_refill_date')); ?>:</b>
	<?php echo CHtml::encode($data->rx_refill_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_start_date')); ?>:</b>
	<?php echo CHtml::encode($data->rx_start_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_end_date')); ?>:</b>
	<?php echo CHtml::encode($data->rx_end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rx_therapy')); ?>:</b>
	<?php echo CHtml::encode($data->rx_therapy); ?>
	<br />

	*/ ?>

</div>