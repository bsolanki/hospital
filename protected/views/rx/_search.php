<?php
/* @var $this RxController */
/* @var $model Rx */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'rx_id'); ?>
		<?php echo $form->textField($model,'rx_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_brand_id'); ?>
		<?php echo $form->textField($model,'rx_brand_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_dose'); ?>
		<?php echo $form->textField($model,'rx_dose',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_frequency'); ?>
		<?php echo $form->textField($model,'rx_frequency',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_instruction'); ?>
		<?php echo $form->textField($model,'rx_instruction',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_duration'); ?>
		<?php echo $form->textField($model,'rx_duration',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_quantity'); ?>
		<?php echo $form->textField($model,'rx_quantity',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_condition'); ?>
		<?php echo $form->textField($model,'rx_condition',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_note'); ?>
		<?php echo $form->textArea($model,'rx_note',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_preview'); ?>
		<?php echo $form->textArea($model,'rx_preview',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_refill'); ?>
		<?php echo $form->textArea($model,'rx_refill',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_refill_date'); ?>
		<?php echo $form->textField($model,'rx_refill_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_start_date'); ?>
		<?php echo $form->textField($model,'rx_start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_end_date'); ?>
		<?php echo $form->textField($model,'rx_end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rx_therapy'); ?>
		<?php echo $form->textField($model,'rx_therapy',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->