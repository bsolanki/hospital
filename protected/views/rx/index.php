<?php
/* @var $this RxController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rxes',
);

$this->menu=array(
	array('label'=>'Create Rx', 'url'=>array('create')),
	array('label'=>'Manage Rx', 'url'=>array('admin')),
);
?>

<h1>Rxes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
