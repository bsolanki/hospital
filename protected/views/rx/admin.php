<?php
/* @var $this RxController */
/* @var $model Rx */

$this->breadcrumbs = array(
    'Rxes' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Rx', 'url' => array('index')),
    array('label' => 'Create Rx', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#rx-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#rx-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>
<div class="row">
   <div class="col-md-12">

        <div class="panel panel-white">
            <div class="clearfix">
                <div class="panel-control text-right">
                    <a href="javascript:void(0);" class="btn btn-success btn btn-primary add_button_rx" ><i class="fa fa-plus"></i></a>
                </div>
            </div>

            <div class="panel-body">
                <div class="add_form_rx" style="display: none;">
                    <?php
                    $this->renderPartial('_form', array(
                        'model' => $create_model,
                    ));
                    ?>
                </div>
                <div class="table-responsive">

                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'rx-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        //'filter' => $model,
                        'columns' => array(
                            //'rx_id',
                            array(
                                'type' => 'raw',
                                'header' => 'Brand Name',
                                'name' => 'rx_brand_id',
                                'value' => '$data->brrx->brnd_name',
                            ),
                            //'rx_brand_id',
                            'rx_dose',
                            array(
                                'type' => 'raw',
                                'name' => 'rx_frequency',
                                'value' => '$data->brfre->frqcy_name',
                            ),
                            array(
                                'type' => 'raw',
                                'name' => 'rx_instruction',
                                'value' => '$data->brins->inst_name',
                            ),
                            'rx_duration',
                            /*
                              'rx_quantity',
                              'rx_condition',
                              'rx_note',
                              'rx_preview',
                              'rx_refill',
                              'rx_refill_date',
                              'rx_start_date',
                              'rx_end_date',
                              'rx_therapy',
                             */
//                            array(
//                                'class' => 'CButtonColumn',
//                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>