<?php
/* @var $this RxController */
/* @var $model Rx */

$this->breadcrumbs=array(
	'Rxes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Rx', 'url'=>array('index')),
	array('label'=>'Manage Rx', 'url'=>array('admin')),
);
?>

<h1>Create Rx</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>