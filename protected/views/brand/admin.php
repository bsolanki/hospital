<?php
/* @var $this BrandController */
/* @var $model Brand */

$this->breadcrumbs=array(
	'Brands'=>array('admin'),
	'Manage Brand',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-f').toggle();
	return false;
});
$('.search-f form').submit(function(){
	$('#brand-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
function update_grid_view()
{
	$('#brand-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
}
");
?>
<div id="statusMsg">    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">'
        . '<button type="button" class="close" data-dismiss="alert">×</button>'
        . $message . "</div>\n";
    }
    ?>    
</div>
<?php
Yii::app()->clientScript->registerScript(
        'flashMassageEffect', '$("#statusMsg").animate({opacity: 1.0}, ' . Yii::app()->params["flasMsgTimeOut"] . ').fadeOut("slow");', CClientScript::POS_READY
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Search</h4>
                <div class="panel-control">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" class="panel-collapse" data-original-title="Expand/Collapse"><i class="icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="search-f">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>  
            </div>

        </div>
    </div><!-- search-form -->
    <div class="col-md-12">
    <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Brand </h4>
                <div class="panel-control">
                    <a href="<?php echo Yii::app()->createUrl('brand/create'); ?>" class="btn btn-success btn btn-primary" ><i class="fa fa-plus"></i> Add Brand </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'brand-grid',
                        'dataProvider' => $model->search(),
                        'itemsCssClass' => 'table table-hover table-striped',
                        'template' => "{items}\n{summary}{pager}",
                        //'filter' => $model,
                        'columns' => array(
                           
                            array(
                                'name' => 'brnd_name',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ),
                            array(
                                'name' => 'generic_name',
                                'header'=>'Generic name',
                                'value'=>'$data->getGenericDetails(0)',
                                'htmlOptions' => array('width' => '15%'),
                                'headerHtmlOptions' => array('width' => '15%'),
                            ),
                            array(
//                                'name' => 'generic_name',
                                'header'=>'Generic Strength (Unit)',
                                'value'=>'$data->getGenericDetails(1)',
                                'htmlOptions' => array('width' => '16%'),
                                'headerHtmlOptions' => array('width' => '16%'),
                            ),
                            array(
                                'name' => 'brnd_dosage_form',
                                'value'=>'$data->brDosage->ds_name',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ),
                            array(
                                'name' => 'brnd_company',
                                'value'=>'$data->brCompany->cmp_name',
                                'htmlOptions' => array('width' => '10%'),
                                'headerHtmlOptions' => array('width' => '10%'),
                            ),
                            array(
                                'name' => 'brnd_volume',
                                'htmlOptions' => array('width' => '8%'),
                                'headerHtmlOptions' => array('width' => '8%'),
                            ),
                            array(
                                'name' => 'brnd_notes',
                                'htmlOptions' => array('width' => '8%'),
                                'headerHtmlOptions' => array('width' => '8%'),
                            ),
                            array(
                                'name' => 'brnd_status',
                                'value' => '$data->displayStatus()',
                                'type' => 'raw',
                                'htmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                                'headerHtmlOptions' => array('width' => '8%', 'style' => 'text-align:center;'),
                            ),
                            array(
                                'header' => 'Actions',
                                'class' => 'CButtonColumn',
                                'template' => '{edit} {delete}',
                                'htmlOptions' => array('width' => '10%'),
                                'deleteConfirmation' => Yii::t('warnings', 'Are you sure you want to delete this Brand?'),
                                'afterDelete' => 'function(link,success,data){$("#statusMsg").html(data).show();}',
                                'buttons' => array(
                                    'edit' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-edit"></i>' ),
                                        'url' => 'Yii::app()->createUrl("/brand/update",array("id"=>$data["brnd_id"]))',
                                        'options' => array('class' => 'btn btn-primary', 'title' => 'Edit'),
                                        'imageUrl' => false,
                                    ),
                                    'delete' => array(
                                        'label' => Yii::t('common', '<i class="fa fa-remove"></i> '),
                                        'url' => 'Yii::app()->createUrl("/brand/delete",array("id"=>$data["brnd_id"]))',
                                        'options' => array('class' => 'btn btn-danger', 'title' => 'Delete'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div> 