<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'brand-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>



    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model);  ?>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'brnd_name'); ?>
            <?php echo $form->textField($model, 'brnd_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'brnd_name'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'brnd_dosage_form'); ?>
            <?php echo $form->dropDownList($model, 'brnd_dosage_form', $model->getDosage(), array('class' => 'form-control', 'empty' => '--Select Dosage--')); ?>
            <?php echo $form->error($model, 'brnd_dosage_form'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'brnd_volume'); ?>
            <?php echo $form->textField($model, 'brnd_volume', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control' , 'onkeypress'=>'return isNumber(event)')); ?>
            <?php echo $form->error($model, 'brnd_volume'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'brnd_company'); ?>
            <?php echo $form->dropDownList($model, 'brnd_company', $model->getCompany(), array('class' => 'form-control', 'empty' => '--Select Company--')); ?>
            <?php echo $form->error($model, 'brnd_company'); ?>
        </div>
    </div>

    <div class="row">

    </div>

    <div class="row">

        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'generic_name', array('class' => 'control-label', 'style' => 'float:left;margin-right:180px;')); ?> 
            <div class="clear">
                <input type="button" id="btnAdd" value="+"  style="width:10px;float:left;margin-right:10px;height: 33px;" class="btn btn-success" />
                <input type="button" id="btnDel" value="-" style="width:10px;height: 33px;" class="btn btn-danger" /></div>
            <br>
            <div id="input1" style="margin-bottom:4px;margin-top:4px;" class="clonedInput clear">
                <?php echo $form->dropDownList($model, 'generic_name', $model->getGeneric(), array('class' => 'form-control', 'empty' => '--Select Generic--', 'style' => 'width:49%!important;float:left;margin-right:5px;')); ?> 
                <?php echo $form->textField($model, 'strength', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'style' => 'width:49%!important;', 'placeholder' => 'Strength', 'onkeypress'=>'return isNumber(event)')); ?> 
            </div>
            <?php
            if (isset($temp_generic) && !empty($temp_generic)) {
                foreach ($temp_generic as $k => $v) {
                   
                    ?>
                    <div class = "clonedInput clear" style = "margin-bottom:4px;margin-top:4px;" id = "input<?php echo $k; ?>">
                        <select id = "generic_name<?php echo $k; ?>" name = "generic_name<?php echo $k; ?>" style = "width:49%!important;float:left;margin-right:5px;" class = "form-control">
                            <option value = ""> --Select Generic--</option>
                            <?php
                            
                            foreach ($model->getGeneric() as $key => $val) {
                                ?>
                                <option <?php echo ($key == $v['generic_name']) ? 'selected = "selected"':''; ?>  value = "<?php echo $key; ?>"><?php echo $val; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input type = "text" value = "<?php echo $v['strength']; ?>" id = "strength<?php echo $k; ?>" name = "strength<?php echo $k; ?>" placeholder = "Strength" style = "width:49%!important;" class = "form-control" maxlength = "255" size = "60" onkeypress="return isNumber(event)" >
                    </div>
                    <?php
                }
            }
            ?>

            <?php echo $form->error($model, 'generic_name'); ?>
        </div>
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'brnd_notes'); ?>
            <?php echo $form->textArea($model, 'brnd_notes', array('class' => 'form-control' ,'row'=>'10' , 'style'=>'height:70px;')); ?>
            <?php echo $form->error($model, 'brnd_notes'); ?>
        </div>
    </div>
    
    <div class="row">
<!--        <div class="form-group col-lg-4 clear">
            <?php // echo $form->labelEx($model, 'brnd_unit_id'); ?>
            <?php // echo $form->dropDownList($model, 'brnd_unit_id', $model->getUnit(), array('class' => 'form-control', 'empty' => '--Select Unit--')); ?>
            <?php // echo $form->error($model, 'brnd_unit_id'); ?>
        </div>-->
        <div class="form-group col-lg-4 clear">
            <?php echo $form->labelEx($model, 'brnd_status'); ?>
            <?php echo $form->dropDownList($model, 'brnd_status', array('1' => 'Active', '0' => 'In-Active'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'brnd_status'); ?>
        </div>
        
    </div>

    
    <div class="row buttons form-group col-lg-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        <a href="<?php echo Yii::app()->createUrl('/brand/admin') ?>" class="btn btn-danger" >Cancel</a>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
            //alert(num);
            var newNum = new Number(num + 1);      // the numeric ID of the new input field being added

            // create the new element via clone(), and manipulate it's ID using newNum value
            var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);

            // manipulate the name/id values of the input inside the new element
            newElem.children(':first').attr('id', 'generic_name' + newNum).attr('name', 'generic_name' + newNum);
            newElem.children(':last').attr('id', 'strength' + newNum).attr('name', 'strength' + newNum);

            newElem.children(':first').val("");
            newElem.children(':last').val("");
            // insert the new element after the last "duplicatable" input field
            $('#input' + num).after(newElem);

            // enable the "remove" button
            //$('#btnDel').attr('disabled', '');
            $('#btnDel').prop("disabled", false);
            // business rule: you can only add 5 names
            if (newNum == 5)
                $('#btnAdd').attr('disabled', 'disabled');
        });

        $('#btnDel').click(function () {
            //  alert("in delete");
            var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
            $('#input' + num).remove();     // remove the last element

            // enable the "add" button
            //$('#btnAdd').attr('disabled', '');
            $('#btnAdd').prop("disabled", false);

            // if only one element remains, disable the "remove" button
            if (num - 1 == 1)
                $('#btnDel').attr('disabled', 'disabled');
        });

        var num = $('.clonedInput').length;
        if (num == 1)
            $('#btnDel').attr('disabled', 'disabled');

    });
</script>