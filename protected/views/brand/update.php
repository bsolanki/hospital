<?php
/* @var $this BrandController */
/* @var $model Brand */

$this->breadcrumbs=array(
	'Brands'=>array('index'),
	$model->brnd_id=>array('view','id'=>$model->brnd_id),
	'Update',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Edit Brand</h4>
            </div>
            <div class="panel-body" >
                <?php $this->renderPartial('_form', array('model' => $model,'temp_generic'=>$temp_generic)); ?>
            </div>

        </div>
    </div><!-- search-form -->
</div>
