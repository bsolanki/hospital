<?php

class AdviceController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'ChangeStatus', 'Inlineedit', 'fetchadvice'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionfetchadvice() {
        
        if(isset($_POST['cat_id']) && !empty($_POST['cat_id'])){
            
            $id = $_POST['cat_id'];
            
        }else{
            
            $id = null;
        }
        
        $data = FcAdvice::model()->fetchadvice($_POST['cat_id']); 

        if(!empty($data)){

            echo json_encode($data);
            Yii::app()->end();

        }

        
         Yii::app()->end();
    }

    public function actionChangeStatus($id) {

        $model = $this->loadModel($id);

        if ($model->adv_status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }

        $model->adv_status = $status;

        $model->saveAttributes(array('adv_status'));

        Yii::app()->end();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionInlineedit() {

        $es = new EditableSaver('Advice');
        try {
            $es->update();
        } catch (CException $e) {
            echo CJSON::encode(array('success' => false, 'msg' => $e->getMessage()));
            return;
        }
        echo CJSON::encode(array('success' => true));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Advice;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Advice'])) {
            $model->attributes = $_POST['Advice'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Advice has been created successfully.');

                if (Yii::app()->request->isAjaxRequest) {
                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'div' => $msg
                    ));
                    exit;
                }
                Yii::app()->end();
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Advice'])) {
            $model->attributes = $_POST['Advice'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Complain has been updated successfully.');
                $this->redirect(array('/advice/admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $result = $this->loadModel($id)->delete();

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> Advice has been deleted successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Advice has been not deleted successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Advice');
        $this->render('admin', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Advice('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Advice']))
            $model->attributes = $_GET['Advice'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Advice the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Advice::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Advice $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'advice-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
