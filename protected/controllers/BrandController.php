<?php

class BrandController extends RController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'ChangeStatus','autocomplete'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionautocomplete(){
        
        
        if(!empty($_GET['term'])){
            
            $data = Brand::model()->findAll('brnd_name like "%'.$_GET['term'].'%"');
            
            print_r($data);
            exit;
        }
    }

    public function actionChangeStatus($id) {
        $model = $this->loadModel($id);

        if ($model->brnd_status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }

        $model->brnd_status = $status;

        $model->saveAttributes(array('brnd_status'));

        Yii::app()->end();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Brand;
        //$generic = array();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $temp_generic = array();
        if (isset($_POST['Brand'])) {

            $model->attributes = $_POST['Brand'];

            $temp_generic = array();
            foreach ($_POST as $key => $value) {
                if (strpos($key, 'generic_name') === 0) {
                    $temp_generic[str_replace("generic_name", "", $key)]['generic_name'] = $value;
                }
                if (strpos($key, 'strength') === 0) {
                    $temp_generic[str_replace("strength", "", $key)]['strength'] = $value;
                }
            }

            if ($model->validate()) {

                $generic[$model->generic_name] = $model->strength;
                if (!empty($temp_generic)) {
                    foreach ($temp_generic as $key => $val) {
                        if (empty($val['strength']))
                            $val['strength'] = 0;
                        $generic[$val['generic_name']] = $val['strength'];
                    }
                }

                $model->brnd_generic_details = serialize($generic);

                if ($model->save()) {
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Brand has been created successfully.');
                    $this->redirect(array('/brand/admin'));
                }
            }
        }
        $this->render('create', array(
            'model' => $model,
            'temp_generic' => $temp_generic,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Brand'])) {
            $model->attributes = $_POST['Brand'];

            $temp_generic = array();
            foreach ($_POST as $key => $value) {
                if (strpos($key, 'generic_name') === 0) {
                    $temp_generic[str_replace("generic_name", "", $key)]['generic_name'] = $value;
                }
                if (strpos($key, 'strength') === 0) {
                    $temp_generic[str_replace("strength", "", $key)]['strength'] = $value;
                }
            }


            if ($model->validate()) {

                $generic[$model->generic_name] = $model->strength;
                if (!empty($temp_generic)) {
                    foreach ($temp_generic as $key => $val) {
                        if (empty($val['strength']))
                            $val['strength'] = 0;
                        $generic[$val['generic_name']] = $val['strength'];
                    }
                }

                $model->brnd_generic_details = serialize($generic);

                if ($model->save()) {
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Brand has been updated successfully.');
                    $this->redirect(array('/brand/admin'));
                }
            }
        } else {
            $generic = unserialize($model->brnd_generic_details);

            $temp_generic = array();
            $i = 1;
            foreach ($generic as $k => $v) {
                if ($i == 1) {
                    $model->generic_name = $k;
                    $model->strength = $v;
                } else {
                    $temp_generic[$i]['generic_name'] = $k;
                    $temp_generic[$i]['strength'] = $v;
                }
                $i++;
            }
        }

        $this->render('update', array(
            'model' => $model,
            'temp_generic' => $temp_generic,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        $result = $this->loadModel($id)->delete();

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> Brand has been deleted successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Brand has been not deleted successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Brand');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Brand('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Brand']))
            $model->attributes = $_GET['Brand'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Brand the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Brand::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Brand $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'brand-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
