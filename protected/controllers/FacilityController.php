<?php

class FacilityController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions{
                'actions' => array('create', 'createstaff', 'update', 'admin', 'delete', 'ChangeStatus', 'main','settings' ,'staff' , 'workinghour'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array(),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionWorkinghour(){
        
        $model = Workinghour::model()->findByAttributes(array('wh_facility_id'=>  Yii::app()->user->getState('facility')));

        if(empty($model)){
            $model = new Workinghour;
            $model->weekday['monday']['status'] = TRUE;
            $model->weekday['tuesday']['status'] = TRUE;
            $model->weekday['wednesday']['status'] = TRUE;
            $model->weekday['monday']['status'] = TRUE;
            $model->weekday['thursday']['status'] = TRUE;
            $model->weekday['friday']['status'] = TRUE;
            $model->weekday['saturday']['status'] = TRUE;
            $model->weekday['sunday']['status'] = TRUE;
        }else{
            
            $model->weekday = unserialize($model->wh_facility_data);
            
        }    

        if(isset($_POST['Workinghour'])){
            
            $model->weekday = $_POST['Workinghour']['weekday'];
            $attributes = array('wh_facility_id'=>  Yii::app()->user->getState('facility'));
            $count = Workinghour::model()->countByAttributes($attributes);
            $model->weekday = serialize($model->weekday);
            if($count != 0){
            
                $condition = 'wh_facility_id = '. Yii::app()->user->getState('facility');
                try{
                    Workinghour::model()->updateAll(array('wh_facility_data'=> $model->weekday), $condition);
                    $this->redirect(array('facility/workinghour'));
                }  catch (Exception $e){
                    echo $e->getMessage();
                    exit;
                }
                
                
                
            }else{
                
                $save_model= new Workinghour();
                $save_model->wh_facility_id= Yii::app()->user->getState('facility');
                $save_model->wh_facility_data= $model->weekday;
                if($save_model->save()){
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> working hours has been updated successfully!');
                    $this->redirect(array('site/dashboard'));
                }
                
            }
            
        }
        
        
        
//        echo "<pre>";
//        print_r($model->weekday);
//        exit;
        
        
        $this->render('workinghour', array(
            'model' => $model,
            //'staff_model' => $staff_model,
        ));
        
    }


    public function actionstaff(){
        
        $model = new FcUser('search');
        
        $staff_model = new User('settings');

        $model->unsetAttributes();  // clear any default values
        $model->fu_facility_id = Yii::app()->user->getState('facility');
        
        if (isset($_GET['FcUser']))
            $model->attributes = $_GET['FcUser'];

        
        $this->render('staff', array(
            'model' => $model,
            'staff_model' => $staff_model,
        ));
        
    }
    
    
    public function actionsettings(){
        
        
        $model = $this->loadModel(Yii::app()->user->getState('facility'));
        $model->scenario = 'settings';
        
        if(isset($_POST['Facility'])){
            
            $model->attributes = $_POST['Facility'];    
            
            if($model->validate()){
                
                if($model->save(false)){
                    
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Facility settings has been updated successfully!');
                    $this->redirect(array('site/dashboard'));
                    
                }
            }
            
        }
        
        $this->render('settings', array(
            'model' => $model,
        ));
      
        
        
        
    }

    public function actionMain($id) {

        $model = $this->loadModel($id);
        
        if($model->fac_setup){
            Yii::app()->user->setState('facility' , $id);
            $this->redirect(array('site/dashboard'));
            
        }
        
        $model->scenario = 'setup';
        
        $user_model = User::model()->findByAttributes(array('usr_id'=> Yii::app()->user->id));
        $user_model->scenario = 'setup';
        
        $new_user = new User('newuser');
        
        $service = new Services();
        
        
        if(isset($_POST['Facility']) && !empty($_POST['Facility'])){
            
            $model->attributes = $_POST['Facility'];
            
            $user_model->attributes = $_POST['User'];
            
            if($model->validate() && $user_model->validate()){
                
                if($model->save() && $user_model->save()){
                    
                    if(!empty($user_model->usr_lname) && !empty($user_model->usr_email)){
                        
                        $new_user = new User('newuser');
                        
                        $new_user->usr_fname = $user_model->usr_lname;
                        $new_user->usr_email = $_POST['User']['usr_email1'];
                        $new_user->usr_type  = $user_model->usr_type;
                        $password = Utility::getToken(8);
                        $new_user->usr_password = $new_user->hashPassword($password);
                        $new_user->usr_original_password = $password;
                        
                        if($new_user->save(false)){
                            
                            $fc_user = new FcUser();
                            
                            $fc_user->fu_facility_id = $id;
                            $fc_user->fu_user_id = $new_user->usr_id;
                            
                            $fc_user->save(false);
                            
                            
                        }
                        
                    }
                    
                    if(isset($_POST['Services']) && !empty($_POST['Services']['ser_name'])){
                        
                        $service_model = new Services();
                        $service_model = $_POST['Services'];
                        $service_model->ser_fac_id = $id;
                        
                        $service->save(false);
                    }
                    
                    $attributes = array('fac_setup'=>'1');
                    Facility::model()->updateByPk($id, $attributes);
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Facility settings has been updated successfully!');
                    $this->redirect(array('admin'));
                    
                    
                    
                }
            }
            $user_model->validate();
            
            
        }

        $this->render('main', array(
            'model' => $model,
            'user_model'=>$user_model,
            'new_user'=>$new_user,
            'service'=>$service
        ));
    }

    public function actionChangeStatus($id) {
        $model = $this->loadModel($id);

        if ($model->fac_status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }

        $model->fac_status = $status;

        $model->saveAttributes(array('fac_status'));

        Yii::app()->end();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new Facility('create');

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Facility'])) {
            $model->attributes = $_POST['Facility'];
            if ($model->validate()) {
                $model->fac_usr_id = Yii::app()->user->id;
                $model->save(false);
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Facility has been created successfully.');
                if (Yii::app()->request->isAjaxRequest) {
                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'div' => $msg
                    ));
                    exit;
                }
                Yii::app()->end();
                #$this->redirect(array('/discipline/admin'));
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }
    
    /**
     * Create Staff a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreatestaff() {

        $model = new User('settings');
        
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->validate()) {
                
                //Add new user
                $token = Utility::getToken(8);
                $model->usr_password = $model->hashPassword($token);
                $model->usr_original_password = $token;
                $model->save(false);
                
                //Mapping with Facility
                $fc_model = new FcUser();
                
                $fc_model->fu_facility_id = Yii::app()->user->getState('facility');
                $fc_model->fu_user_id = $model->usr_id;
                
                $fc_model->save(false);
                
                //Entry in rights module
                Rights::assign($model->usr_type, $model->usr_id);
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Staff has been added successfully.');
                if (Yii::app()->request->isAjaxRequest) {
                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'div' => $msg
                    ));
                    exit;
                }
                Yii::app()->end();
                #$this->redirect(array('/discipline/admin'));
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Facility'])) {
            $model->attributes = $_POST['Facility'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->fac_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        $result = $this->loadModel($id)->delete();

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> Facility has been deleted successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Facility has been not deleted successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Facility');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Facility('search');

        $create_model = new Facility('create');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Facility']))
            $model->attributes = $_GET['Facility'];
            
        $this->render('admin', array(
            'model' => $model,
            'create_model' => $create_model
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Facility the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Facility::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Facility $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'facility-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
