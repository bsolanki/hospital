<?php

class ComplainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'ChangeStatus', 'Inlineedit', 'loadcomplain','customizecomplains' ,'add'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionadd(){
        
        if(isset($_POST['PtComplain']) && !empty($_POST['PtComplain'])){
            
            $model = new PtComplain();
            $model->attributes = $_POST['PtComplain'];
            $model->pc_patient_id = Yii::app()->user->getState('rx_patient_id');
            $model->pc_complain_id = serialize($model->pc_complain_id);
            $model->save(false);
            
            echo CJSON::encode(array('success' => true));
            
        }
    }

    public function actionloadcomplain() {

        //Current Patient Id 
        $patient_id = Yii::app()->user->getState('rx_patient_id');
        // Current facility id
        $facility_id = Yii::app()->user->getState('facility');

        // Add form 
        $model = new PtComplain();

        if (!empty($patient_id)) {
            
            //Load All compalain list
            $model->pc_complain_id = CHtml::listData(Complain::model()->findAllByAttributes(array(), 'cmp_fc_id IN("0" , "' . $facility_id . '")'), 'cmp_id', 'cmp_name');
            
        }

        //Previously added data
        $load_grid = new PtComplain('search');
        $load_grid->unsetAttributes();  // clear any default values
        if (isset($_GET['PtComplain']))
            $load_grid->attributes = $_GET['PtComplain'];
        

        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => false,
        );

        $this->renderPartial('_loadcomplain', array('model' => $model , 'load_grid'=>$load_grid), false, true);
        
        exit;
    }

    public function actionInlineedit() {

        $es = new EditableSaver('Complain');

        try {
            $es->update();
        } catch (CException $e) {
            echo CJSON::encode(array('success' => false, 'msg' => $e->getMessage()));
            return;
        }
        echo CJSON::encode(array('success' => true));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Complain;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Complain'])) {
            $model->attributes = $_POST['Complain'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Complain has been created successfully.');

                if (Yii::app()->request->isAjaxRequest) {

                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'div' => $msg
                    ));
                    exit;
                }
                Yii::app()->end();
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionChangeStatus($id) {
        $model = $this->loadModel($id);

        if ($model->cmp_status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }

        $model->cmp_status = $status;

        $model->saveAttributes(array('cmp_status'));

        Yii::app()->end();
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Complain'])) {
            $model->attributes = $_POST['ComplainController'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Complain has been updated successfully.');
                $this->redirect(array('/Complain/admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $result = $this->loadModel($id)->delete();

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> Complain has been deleted successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Complain has been not deleted successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Complain');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Complain('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Complain']))
            $model->attributes = $_GET['Complain'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Complain the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Complain::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Complain $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'complain-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
