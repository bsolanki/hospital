<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('login', 'logout', 'error', 'lostpassword', 'resetPassword', 'identify', 'mainidentity', 'signup', 'finish', 'profilepic','addtask','listtask','deletetask'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionprofilepic() {

        $model = new User('profilepic');
            echo "<pre>";
            print_r($_POST);
            exit;
        if (!empty($_FILES) && isset($_FILES)) {
            
            $model->usr_profile_pic = $_FILES["User"]["name"]['usr_profile_pic'];
        }

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'Image-form') {
            echo CActiveForm::validate($model);
            //
        }

        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];

            $image_path = Yii::app()->basePath . "/../images/users/";
            //$model->setAttribute('usr_image',$model->usr_username . ".png");

            if ($model->validate() && $model->save()) {

                $image_name = $model->usr_id . ".png";

                if (!empty($_FILES["User"]["name"]['usr_profile_pic'])) {
                    try {
                        move_uploaded_file($_FILES["User"]["tmp_name"]['usr_profile_pic'], $image_path . $image_name);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }

//                $model->usr_image = $image_name;
//                $model->saveAttributes(array('usr_image'));
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Profile has been updated successfully.');
                $role = Rights::getAssignedRoles(Yii::app()->user->id);

//                if (isset($role['Customer admin']) || isset($role['Sub Customer Admin'])) {
                $this->redirect(array('customer/dashboard'));
//                } else {
//                    $this->redirect(array('user/dashboard'));
//                }
            }
        }
        Yii::app()->end();
    }
    
    public function actionStart(){
        
        
        
        
    }


    public function actionaddtask() {

        $model = new Task;
        if (isset($_POST['task_name'])) {
           if($_POST['task_name']==""){
                echo "empty";
           } 
           else{          
                $model->task_description=$_POST['task_name'];
                $model->task_doc_id=Yii::app()->user->getState('usr_id');
          // print_r($model->attributes);exit;  
                if($model->save(false)){
                    echo "success"; 
                }
           }    
        }
        exit;
    }

    
    public function actiondeletetask() {

        $model = new Task('search');  
        $id = $_POST['taskid']; 
        $result = Task::model()->deleteAll('task_id = '.$id);
        echo "success"; exit;
    }

    public function actionlisttask() {

        $model = new Task('search');  
        $model->unsetAttributes();  // clear any default values
        $this->layout='';
        
        //$model_data = Task::model()->findByAttributes(array('task_doc_id' => trim(Yii::app()->user->getState('usr_id'))));


        $model_data = Task::model()->findAllByAttributes(array('task_doc_id' => trim(Yii::app()->user->getState('usr_id'))));
         
        $this->renderPartial('todolist', array('model_data'=>$model_data), false, true); 
        exit;
    }

    public function actionfinish() {
        $this->render('finish');
    }

    public function actionSignup() {
        //   Yii::app()->user->setState('signup1', '');

        $model = new User('signup1');

        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];


            if ($model->validate()) {

                $model->usr_original_password = $model->usr_password;
                $model->usr_password = $model->hashPassword($model->usr_password);
                $model->usr_dis_id = serialize($model->usr_dis_id);

                $model->usr_type = "doctor";
                $model->usr_status = '2';

                if ($model->save(false)) {
                    Rights::assign("doctor", $model->usr_id);
                    Yii::app()->user->setFlash('success', '<strong>Congratulations !</strong> User has been added successfully we will verify registration number and then will send you link on  register email.');
                    $this->redirect(array('site/login'));
                }
            }
        }

        $this->render('signup', array('model' => $model));
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        /*
          if (!Yii::app()->user->isGuest) {
          $this->redirect(array('discipline/admin'));
          }
         */

        if (Yii::app()->user->getState('usr_type') == "doctor") {
            $this->redirect(array('facility/admin'));
        } else {
            $this->redirect(array('discipline/admin'));
        }

        $this->renderPartial('index');
    }

    public function actionLostPassword() {

        if (!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $model = new LostPasswordForm;


        // uncomment the following code to enable ajax-based validation
        /*
          if(isset($_POST['ajax']) && $_POST['ajax']==='lost-password-form-LostPassword-form')
          {
          echo CActiveForm::validate($model);
          Yii::app()->end();
          }
         */

        if (isset($_POST['LostPasswordForm'])) {
            $model->attributes = $_POST['LostPasswordForm'];
            if ($model->validate()) {
                $user = User::model()->findByAttributes(array('usr_email' => trim($model->username)));

                $token_lemgth = 20 - strlen($user->usr_id);

                if ($token_lemgth > 0) {
                    $token = $user->usr_id . Mailer::randomToken($token_lemgth);
                } else {
                    $token = $user->usr_id . Mailer::randomToken(5);
                }

                $user->usr_reset_pass_token = $token;
                $user->usr_reset_pass_date = date('Y-m-d h:s:i');

                $retult = Mailer::resetPasswordNotification($token, $user);

                if ($retult && $user->save(false)) {
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Check your e-mail for the confirmation link.');
                    $this->redirect(array('/site/login'));
                } else {
                    $model->getErrors();

                    Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Error in sending mail!');
                }

                // form inputs are valid, do something here
                return;
            }
        }
        $this->render('LostPassword', array('model' => $model));
    }

    public function actionResetPassword($token) {
        $model = new ResetPasswordForm;

        // uncomment the following code to enable ajax-based validation
        /*
          if(isset($_POST['ajax']) && $_POST['ajax']==='reset-password-form-resetPassword-form')
          {
          echo CActiveForm::validate($model);
          Yii::app()->end();
          }
         */

        $key = base64_decode($token);

        $user_model = User::model()->findByAttributes(array('usr_reset_pass_token' => $key));

        if ($user_model == NULL) {
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Link Expire. Please regenerate link!');
            $this->redirect(array('/site/login'));
        } else {
            if (time() - strtotime($user_model->usr_reset_pass_date) > Yii::app()->params['PassResetExpiry']) {
                Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Reset Password link expired. Please Reset Password again.');
                $this->redirect(array('/site/login'));
            }
        }

        if (isset($_POST['ResetPasswordForm'])) {
            $model->attributes = $_POST['ResetPasswordForm'];
            if ($model->validate()) {
                $user_model->usr_password = $user_model->hashPassword($model->new_password);
                $user_model->usr_reset_pass_date = NULL;
                $user_model->usr_reset_pass_token = NULL;

                if ($user_model->save(false)) {
                    Mailer::resetPasswordConfirmNotification($user_model);
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Your password has been reset.');
                    $this->redirect(array('/site/login'));
                } else {
                    Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Something wrong in reset password. Please try after sometimes.');
                }
            }
        }
        $this->render('resetPassword', array('model' => $model));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            
            echo $error['message'];exit;
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {

                // Set Cookie For remember password
                if ($model->rememberMe == 1) {
                    $data = base64_encode($model->username . ":" . $model->password);
                    $cookie = new CHttpCookie('logindata', $data);
                    $cookie->expire = time() + (3600 * 24 * 365);
                    $cookie->path = Yii::app()->params['basepath_url'] . 'site/login';
                    Yii::app()->request->cookies['username'] = $cookie;
                }

                //Redirect To Last Visted URL
                //  $this->redirect(Yii::app()->user->returnUrl);
                $this->redirect(array('site/index'));
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Dasboard asction for the current doctor.
     */
    public function actionDashboard() {

        $model = User::model()->findByPk(Yii::app()->user->id);
        $team_member = 0;

        $this->render('dashboard', array(
            'model' => $model,
            'team_member' => $team_member,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
