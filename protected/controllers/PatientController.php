<?php

class PatientController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'details', 'list', 'complain', 'addComplain', 'frequency', 'instruction', 'customizecomplains'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array(),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionfrequency() {

        $frequency = new FcFrequency;
        $frequency->hff_fc_fre_id = Patient::model()->getFrequencyByfacilityid();

        $this->renderPartial('_frequency', array(
            'frequency' => $frequency
        ));
    }

    public function actioncustomizecomplains() {

        $model = new PtComplain();

        $model->pc_complain_id = $model->favComplain();

        $this->renderPartial('_complains', array(
            'model' => $model
        ));
    }

    public function actioninstruction() {

        $instruction = new FcInstruction();
        $instruction->hfi_instruction_id = Patient::model()->getInstructionByfacilityid();

        $this->renderPartial('_instruction', array(
            'instruction' => $instruction
        ));
    }

    public function actionaddComplain() {

        $model = new Complain();

        if (isset($_POST['complain_name'])) {

            $model->cmp_name = $_POST['complain_name'];
            $model->cmp_user_id = Yii::app()->user->id;
            $model->cmp_fc_id = Yii::app()->user->getState('facility');
            $model->cmp_cat_id = 0;

            if ($model->save()) {

                $complains_model = new FcComplains;
                $complains_model->hfc_fc_id = Yii::app()->user->getState('facility');
                $complains_model->hfc_compain_id = $model->cmp_id;
                $complains_model->save(false);

                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Complain has been created successfully.');

                if (Yii::app()->request->isAjaxRequest) {

                    echo CJSON::encode(array(
                        'success' => true,
                        'cmp_name' => $_POST['complain_name'],
                        'cmp_id' => $model->cmp_id
                    ));
                    exit;
                }
                Yii::app()->end();
            } else {

                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->renderPartial('_addcomplain', array('model' => $model));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = null) {


        //Fetch list of patient for perticular facility
        $model = new Patient('search');

        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Patient']))
            $model->attributes = $_GET['Patient'];

        $info = array();

        if (!empty($id)) {

            $info = Patient::model()->findByPk($id);

            
            Yii::app()->user->setState('rx_patient_id', $id);
        }

        $this->render('view', array(
            'model' => $model,
            'info' => $info,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Patient;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Patient'])) {
            $model->attributes = $_POST['Patient'];
            $model->pt_facility_id = Yii::app()->user->getState('facility');
            if ($model->validate()) {

                $model->save();
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Patient\'s record has been added successfully.');
                $this->redirect(array('admin'));
            }
        } else {
            $error = CActiveForm::validate($model);
            if ($error != '[]')
                echo $error;
            Yii::app()->end();
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {

        $model = $this->loadModel(Yii::app()->user->getState('rx_patient_id'));

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        
        if (isset($_POST['Patient'])) {

            $model->attributes = $_POST['Patient'];

            if ($model->validate()) {

                if ($model->save()) {

                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Patient\'s record has been updated successfully.');

                    if (Yii::app()->request->isAjaxRequest) {
                        $msg = '';
                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
                            $msg.= $message;
                        }
                        echo CJSON::encode(array(
                            'success' => true,
                            'div' => $msg
                        ));
                        exit;
                    }
                    Yii::app()->end();
                }
            } else {

                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
            //  $this->redirect(array('view', 'id' => $model->pt_id));
        }

        $this->renderPartial('update', array(
            'model' => $model,
        ));
    }

    public function actionDetails($id) {

        $model = new Patient;

        $this->render('details', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Patient');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Patient('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Patient']))
            $model->attributes = $_GET['Patient'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionlist($id = NULL) {

        $patient_details = array();
        $patient = new PatientDetails;

        $rx_model = new Rx();
        $createrx_model = new Rx;
        $patient_data = new PatientDetails;

        $vital = new Vitals;
        $create_vital = new Vitals();

        $fcadvice = new FcAdvice();

        $info = Patient::model()->findByPk($id);

        Yii::app()->user->setState('rx_patient_id', $id);

        if (!empty($id)) {

            $patient_details = Patient::model()->findByPk($id);
            $patient_data->pd_complains = CHtml::listData(FcComplains::model()->findAllByAttributes(array('hfc_fc_id' => Yii::app()->user->getState('facility'))), 'hfc_compain_id', 'hfc_compain_id');
        }

        $model = new Patient('search');

        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Patient']))
            $model->attributes = $_GET['Patient'];



        $this->render('list', array(
            'model' => $model,
            'patient_details' => $patient_details,
            'patient' => $patient,
            'createrx_model' => $createrx_model,
            'rx_model' => $rx_model,
            'patient_data' => $patient_data,
            'info' => $info,
            'fcadvice' => $fcadvice,
            'create_vital' => $create_vital,
            'vital' => $vital,
        ));
    }

    public function actioncomplain() {


        if (isset($_POST['pd_complains'])) {

            $complain_array = explode(",", $_POST['pd_complains']);


            FcComplains::model()->deleteAllByAttributes(array('hfc_fc_id' => Yii::app()->user->getState('facility')));

            foreach ($complain_array as $val) {

                $fccomplain = new FcComplains();

                $fccomplain->hfc_fc_id = Yii::app()->user->getState('facility');
                $fccomplain->hfc_compain_id = $val;

                $fccomplain->save(false);
            }
        }

        $data = CHtml::listData(Complain::model()->findAllByAttributes(array('cmp_id' => $complain_array)), 'cmp_id', 'cmp_name');

        echo json_encode($data);
        exit;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Patient the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Patient::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Patient $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'patient-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
