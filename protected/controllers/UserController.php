<?php

class UserController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'doctor', 'ChangeStatus', 'activate', 'staff', 'Editprofile', 'changePassword', 'admin', 'delete' , 'view'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array(),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('view', array(
                'model' => $this->loadModel($id),
                    ), false, true);
        } else {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        }
    }

    public function actionChangePassword() {
        $model = new ChangePasswordForm;

        // uncomment the following code to enable ajax-based validation
        /*
          if(isset($_POST['ajax']) && $_POST['ajax']==='change-password-form-ChangePassword-form')
          {
          echo CActiveForm::validate($model);
          Yii::app()->end();
          }
         */

        if (isset($_POST['ChangePasswordForm'])) {
            $model->attributes = $_POST['ChangePasswordForm'];

            if ($model->validate()) {

                $user_model = $this->loadModel(Yii::app()->user->id);
                $user_model->usr_password = $user_model->hashPassword($model->new_password);

                if ($user_model->save(false)) {
                    Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Password Changed successfully!');
//                    $role = Rights::getAssignedRoles(Yii::app()->user->id);
//                    if (isset($role['Customer admin']) || isset($role['Sub Customer Admin'])) {
                    $this->redirect(array('site/dashboard'));
//                    } else {
//                        $this->redirect(array('user/dashboard'));
//                    }
                } else {
                    Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Password could not Change successfully!');
                }
            }
        }

        $this->render('ChangePassword', array('model' => $model));
    }

    public function actionActivate($id) {
        $model = $this->loadModel($id);

        $model->usr_status = '1';
        $model->saveAttributes(array('usr_status'));
        $result = Mailer::ActivationMail($model);

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> User has been activated successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> User has been not activated successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('doctor'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    public function actionChangeStatus($id) {
        $model = $this->loadModel($id);

        if ($model->usr_status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }

        $model->usr_status = $status;

        $model->saveAttributes(array('usr_status'));

        Yii::app()->end();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User('staff');

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->validate()) {

                $model->usr_original_password = Utility::getToken(8);
                $model->usr_password = $model->hashPassword($model->usr_original_password);
                $model->usr_parent_id = Yii::app()->user->id;
                //$model->fac_usr_id = Yii::app()->user->id;

                $model->save(false);

                $user = User::model()->findByAttributes(array('usr_id' => trim($model->usr_id)));
                Mailer::CreateStaff($user);

                if ($model->usr_type == "provider") {
                    Rights::assign("provider", $model->usr_id);
                } else {
                    Rights::assign("staff", $model->usr_id);
                }



                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Staff has been added successfully.');
                if (Yii::app()->request->isAjaxRequest) {
                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'div' => $msg
                    ));
                    exit;
                }
                Yii::app()->end();
                #$this->redirect(array('/discipline/admin'));
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->usr_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        $result = $this->loadModel($id)->delete();

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> Staff has been deleted successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Staff has been not deleted successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('User');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionDoctor() {
        $model = new User('search');

        $model->unsetAttributes();  // clear any default values
        $model->usr_type = 'doctor';

        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('doctor', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'wizardForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionEditprofile() {

        $model = $this->loadModel(Yii::app()->user->getState('usr_id'));

        $model->scenario = 'editProfile';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];

            if ($model->validate() && $model->save()) {


                Yii::app()->user->setState('usr_username', ucfirst($model->usr_fname) . " " . ucfirst($model->usr_lname));

                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Profile has been updated successfully.');
//                $role = Rights::getAssignedRoles(Yii::app()->user->id);
//
//                if (isset($role['Customer admin']) || isset($role['Sub Customer Admin'])) {
//                    $this->redirect(array('customer/dashboard'));
//                }else{
                $this->redirect(array('user/admin'));
//                }
            }
        }

        $this->render('editprofile', array(
            'model' => $model,
        ));
    }

    public function actionstaff() {

        $model = new User('search');

        $create_model = new User('staff');

        $customer_id = Yii::app()->user->id;

        $model->unsetAttributes();  // clear any default values
        // $model->usr_type = 'doctor';

        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('staff', array(
            'model' => $model,
            'create_model' => $create_model,
        ));
    }

}
