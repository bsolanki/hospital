<?php

class RxController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Rx;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Rx'])) {
//            $model->commnet = '';
            $model->attributes = $_POST['Rx'];
            $model->rx_patient_id = Yii::app()->user->getState('rx_patient_id');

            if (!empty($model->rx_brand_id)) {
                $model->comment = '';
                // fetch generic details based on brand
                $genric_datails = Brand::model()->findByAttributes(array('brnd_id' => $model->rx_brand_id))->brnd_generic_details;
                if (!empty($genric_datails)) {
                    $genric_datails = unserialize($genric_datails);
                }
                $result = Brand::model()->CalculateMaxDose($genric_datails);

                if (!empty($result)) {
                    $total = 0;
                    if ($model->rx_frequency == 2) {
                        $total = $model->rx_dose;
                    } elseif ($model->rx_frequency == 3) {
                        $total = $model->rx_dose * 2;
                    } elseif ($model->rx_frequency == 4) {
                        $total = $model->rx_dose * 3;
                    }
                    if ($total != 0) {
                        foreach ($result as $checkdose) {
                            if ($total > $checkdose) {
                                $model->rx_colorcode = 'red';
                                $model->comment = 'Max Dose error.';
                            }
                        }
                    }
                }
            }

            if ($model->save()) {

                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Rx has been created successfully.');

                if (Yii::app()->request->isAjaxRequest) {
                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'comment' => $model->comment
                    ));
                    exit;
                }
                Yii::app()->end();

                //$this->redirect(array('/frequency/admin'));
            } else {

                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Rx'])) {
            $model->attributes = $_POST['Rx'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->rx_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Rx');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {

        $model = new Rx('search');
        $create_model = new Rx;
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Rx']))
            $model->attributes = $_GET['Rx'];
        
        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => false,
        );

        $this->renderPartial('admin', array(
            'model' => $model,
            'create_model' => $create_model,
                ), FALSE, TRUE);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Rx the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Rx::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Rx $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'rx-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
