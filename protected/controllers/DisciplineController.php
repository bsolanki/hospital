<?php

class DisciplineController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'ChangeStatus', 'Inlineedit'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionChangeStatus($id) {
        
        $model = $this->loadModel($id);

        if ($model->dis_status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }
    
        $model->dis_status = $status;

        $model->saveAttributes(array('dis_status'));

        Yii::app()->end();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.         telefoon
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Discipline;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Discipline'])) {
            $model->attributes = $_POST['Discipline'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Discipline has been created successfully.');
                if (Yii::app()->request->isAjaxRequest) {
                    $msg = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $msg.= '<div class="alert alert-' . $key . '">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . $message . "</div>\n";
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'div' => $msg
                    ));
                    exit;
                }
                Yii::app()->end();
                #$this->redirect(array('/discipline/admin'));
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

//    public function actionInlineedit(){
//        if(Yii::app()->request->isPostRequest) {
//        $model = new User;
//        $model->attributes = $_POST;
//        if($model->save()) {
//            echo CJSON::encode(array('id' => $model->primaryKey));
//        } else {
//            $errors = array_map(function($v){ return join(', ', $v); }, $model->getErrors());
//            echo CJSON::encode(array('errors' => $errors));
//        }
//    } else {
//      throw new CHttpException(400, 'Invalid request');  
//    }
//    }

    public function actionInlineedit() {

        $es = new EditableSaver('Discipline');
        try {
            $es->update();
        } catch (CException $e) {
            echo CJSON::encode(array('success' => false, 'msg' => $e->getMessage()));
            return;
        }
        echo CJSON::encode(array('success' => true));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Discipline'])) {
            $model->attributes = $_POST['Discipline'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Well done!</strong> Discipline has been updated successfully.');
                $this->redirect(array('/discipline/admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        $result = $this->loadModel($id)->delete();

        if ($result) {
            Yii::app()->user->setFlash('success', '<strong>Great!</strong> Discipline has been deleted successfully.');
        } else
            Yii::app()->user->setFlash('error', '<strong>Oh snap!</strong> Discipline has been not deleted successfully');

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else {
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">'
                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                . $message . "</div>\n";
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Discipline');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Discipline('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Discipline']))
            $model->attributes = $_GET['Discipline'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Discipline the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Discipline::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Discipline $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'discipline-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
