<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'hsp_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
