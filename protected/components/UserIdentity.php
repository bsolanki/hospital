<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    const ERROR_USER_INACTIVE = 3;

    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {

        $user = User::model()->findByAttributes(array('usr_email' => $this->username));

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (!$user->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else if ($user->usr_status == 'n') {
            $this->errorCode = self::ERROR_USER_INACTIVE;
        } else {

            $this->_id = $user->usr_id;
            // $this->setId($user->usr_id);
            $this->setState('usr_id', $user->usr_id);
            $this->setState('usr_type', $user->usr_type);
            $this->setState('usr_username', ucfirst($user->usr_fname) . " " . ucfirst($user->usr_lname));
            $this->setState('usr_last_login', $user->usr_last_login);

            $attributes = array('usr_last_login' => date('Y-m-d H:i:s'));

            User::model()->updateByPk($user->usr_id, $attributes);

            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
