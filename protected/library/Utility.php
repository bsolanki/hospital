<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Utility
{

    public static function parameterize_array($array) {
        $out = array();
        foreach($array as $key => $value)
        {
            $out[$key] = "$key - $value";
        }            
        return $out;
    } 
    
    public static function getToken($length = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
       
    public static function getTimezone($country_code=NULL)
    {
        $sql = "SELECT t.id, t.abbreviation, t.gmt_offset , z.zone_name FROM vxp_zone as z, vxp_timezone as t WHERE t.zone_id = z.zone_id";
        if(!empty($country_code))
        {
            $sql .= " AND z.country_code='$country_code'";
        }
        $sql .= " GROUP BY t.abbreviation,t.gmt_offset"; 
        
        $connection=Yii::app()->db;
        $command=$connection->createCommand($sql);
        $rows=$command->queryAll(true);  
        $timezone = array();
        foreach ($rows as $row) {
            $val = "UTC ";
            $val .= ($row['gmt_offset']>0)?"+":"";
            $val .=  round($row['gmt_offset']/3600,2);      
            $val .= " (".$row['abbreviation'].")";
            $timezone[$row['id']] =$row['zone_name'] . " - " . $val ;
        }
        
        return $timezone;        
    }
    
    public static function timezoneConvert($date, $offset=0)
    {
        $time = strtotime($date)+$offset;
        return date('d, M Y', $time);
    }
}