<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Mailer {

    public static function mailsend($to, $subject, $message, $attachment = false, $attachmentFileName = false) {
        $mail = Yii::app()->Smtpmail;
    
        $mail->Subject = $subject;
	//$mail->SetFrom('name@yourdomain.com', 'First Last');
        
     //   $config = CHtml::listData(Config::model()->findAll(), 'cfg_key', 'cfg_value');
        
        $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <body style="margin: 0px;pCing: 0px;">
      <table border="0" style="width:650px;height:auto;font-family: Lato, sans-serif;background:#FFF;" cellspacing="0" cellpadding="0" align="center">
            <tr>
	      <td>
		  <table border="0" style="width : 650px ;background:#0075d0" align="center" cellspacing="0">
		      <tr>
			      <td style=" background : #; ">
				    <table border="0" style="color:#FFFFFF;padding:10px 5px;height:30px;width:750px;margin:0;font-size:12px;font-weight:600;" cellspacing="0">
                                    <tr>
                          <td style="height: 30px;background: #0075cf;color: #FFFFFF;">
                              <span style="margin-left: 20px; font-family: Arial Regular;font-size:20px">' . $subject . '</span>
                          </td>
                      </tr>
				     </table>
                                     </td><td colspan="2"> 
						    </td>
				    </tr></table>
			      
			      </td>
		      </tr>
                      
                      <tr>
                          <td style="height: 115px;border: 1px solid #4C7697;color:#747474;text-align: justify;padding: 13px; font-size: 14px;line-height:23px">
                                    ' . $message . '
                          </td>
                      </tr>
                      <tr style="height: 15px">
                      </tr>
            <tr>
                <td style="height: 159px;background: #0075cf" valign="bottom">
                    <table style="padding-bottom: 0px;margin-left: 20px;">
                        <tr>
                            <td style="color:#ff9704;font-family: HelveticaNeue MediumCond;font-size: 22px;width:420px;">'. Yii::app()->params['COMPANY_NAME_IN_EMAIL_TEMPLATE'] .'</td>
                            <td style="font-family: HelveticaNeue MediumCons;text-align: right">
                            <a href="tel:'.Yii::app()->params['CONTACT_NUMBER_IN_EMAIL_TEMPLATE'] .'" style="text-decoration:none;color:#ff9704;font-size: 22px;">
                                '. Yii::app()->params['CONTACT_NUMBER_IN_EMAIL_TEMPLATE'] .'
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#ffffff;font-family: Helvetica;font-size: 14px;width:400px;font-weight: bold;line-height: 25px;">'. Yii::app()->params['COMPANY_ADDRESS_IN_EMAIL_TEMPLATE'] .'</td>
                            
                            <td style="font-family: helvetica;font-size: 18px;text-align: right" valign="top"><a href="mailto:'. Yii::app()->params['EMAIL_ADDRESS_IN_EMAIL_TEMPLATE'] .'" style="text-decoration:none;color:#ffffff;">'.Yii::app()->params['EMAIL_ADDRESS_IN_EMAIL_TEMPLATE'].'</a></td>
                            
                        </tr>
                        <tr></tr>
                        <tr style="">
                            <td colspan="2" align="center" style="color:#FFFFFF; font-size:12px;line-height:3;font-family:HelveNueThin;">Copyright by '. Yii::app()->params['COMPANY_NAME_IN_EMAIL_TEMPLATE'] .' </td>
                        </tr>
                    </table>
                    
                </td>
              
            </tr>
      </table>
  </body>
</html>';
        $mail->MsgHTML($msg);
        $mail->ClearAddresses(); //clear addresses for next email sending
        if (!empty($attachment) && !empty($attachmentFileName)) {
            $mail->AddStringAttachment($attachment, $attachmentFileName);
        }

        // Set destination addresses

        if (!is_array($to))
            $to = explode(',', $to);

        foreach ((array) $to as $recipient) {
            try {
                // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
                $recipient_name = '';
                if (preg_match('/(.*)<(.+)>/', $recipient, $matches)) {
                    if (count($matches) == 3) {
                        $recipient_name = $matches[1];
                        $recipient = $matches[2];
                    }
                }
                $mail->AddAddress($recipient, $recipient_name);
//                $mail->AddCC("foram.patel@ecosmob.com", "Foram Patel");
            } catch (phpmailerException $e) {
                continue;
            }
        }

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
exit;
            return false;
        } else {
            //echo "Message sent!";
            return true;
        }
    }

    public static function randomToken($length = 8) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    public static function CreateStaff($user) {

        $email = $user->usr_email;
        
        $site_url = Yii::app()->createAbsoluteUrl('/site/login');

        $data = EmailTemplate::model()->findByAttributes(array('email_template_name' => 'CREATE_STAFF'));

        $subject = $data->email_template_subject;

        $trans = array("##SITE_URL##" => "$site_url", "##USER_NAME##" => $user->usr_fname . " " . $user->usr_lname , "##PASSWORD##"=>$user->usr_original_password);
        $message = strtr("$data->email_template_body", $trans);

        return self::mailsend($email, $subject, $message);
        
    }

    public static function resetPasswordNotification($token, $user) {

        $email = $user->usr_email;

        $template = EmailTemplate::model()->findByAttributes(array('email_template_name' => 'RESET_PASSWORD_NOTIFICATION'));

        $url = "<a href='" . Yii::app()->createAbsoluteUrl('/site/resetPassword', array('token' => base64_encode($token))) . "' target='_blank'>Click here to reset password</a>";

        $trans = array("_SITE_URL_" => "$url", "##URL##" => "$url", "##USER_NAME##" => "$user->usr_fname");

        $message = strtr("$template->email_template_body", $trans);

        $subject = $template->email_template_subject;

        return self::mailsend($email, $subject, $message);
    }
    
    public static function ActivationMail($user){
        
        $email = $user->usr_email;

        $data = EmailTemplate::model()->findByAttributes(array('email_template_name' => 'DOCTOR_ACTIVATION_MAIL'));
        $site_url = Yii::app()->createAbsoluteUrl('/site/login');
        $subject = $data->email_template_subject;

        $trans = array("##SITE_URL##" => "$site_url", "##USER_NAME##" => $user->usr_email  , "##PASSWORD##"=>$user->usr_original_password ,"##FIRST_NAME##"=>$user->usr_fname );
        $message = strtr("$data->email_template_body", $trans);

        return self::mailsend($email, $subject, $message);
        
    }

    public static function resetPasswordConfirmNotification($user) {
        $email = $user->usr_email;
        $site_url = Yii::app()->createAbsoluteUrl('/site/login');

        $data = EmailTemplate::model()->findByAttributes(array('email_template_name' => 'RESET_PASSWORD_CONFIRM_NOTIFICATION'));

        $subject = $data->email_template_subject;

        $trans = array("##SITE_URL##" => "$site_url", "##USER_NAME##" => "$user->usr_fname");
        $message = strtr("$data->email_template_body", $trans);

        return self::mailsend($email, $subject, $message);
    }
    
    public static function adminAccount($user , $password){
        
        $email = $user->usr_email;
        $site_url = Yii::app()->createAbsoluteUrl('/site/login');

        $data = EmailTemplate::model()->findByAttributes(array('email_template_name' => 'ADMIN_SIGNUP_MAIL'));

        $subject = $data->email_template_subject;

        $trans = array("##SITE_URL##" => "$site_url", "##USER_NAME##" => "$user->usr_username" , "##PASSWORD##"=>$password);
        $message = strtr("$data->email_template_body", $trans);

        return self::mailsend($email, $subject, $message);
        
    }
    
    public static function customerAccount($user , $password){
        
        $email = $user->usr_email;
        $site_url = Yii::app()->createAbsoluteUrl('/site/login');

        $data = EmailTemplate::model()->findByAttributes(array('email_template_name' => 'CUSTOMER_SIGNUP_MAIL'));

        $subject = $data->email_template_subject;

        $trans = array("##SITE_URL##" => "$site_url", "##USER_NAME##" => "$user->usr_username" , "##PASSWORD##"=>$password);
        $message = strtr("$data->email_template_body", $trans);

        return self::mailsend($email, $subject, $message);
        
    }

}
