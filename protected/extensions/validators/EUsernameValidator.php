<?php
/**
 * 
 * EUsernameValidator class
 * 
 */

class EUsernameValidator extends CValidator{
    
    //Minimum string length
    public $min = 6;
    
    //Maximum string length
    public $max = 20;
    
    /**
	 * (non-PHPdoc)
	 * @see CValidator::validateAttribute()
	 */
    protected function validateAttribute($object,$attribute){
       if(!$this->checkUsername($object->$attribute)){
            $message=$this->message!==null?$this->message:Yii::t("EUsernameValidator","{attribute} is minimum is ".$this->min." characters and must start with character and only allow special character are -_. ");
			$this->addError($object,$attribute,$message);
       }
    }
    
    /**
     * Check if password is strong enought
     * @param string $password
     * @return boolean 
     */
    protected function checkUsername($string){
        if (preg_match("/^[a-zA-Z][a-zA-Z0-9-_\.]{" . ($this->min-1) . "," . $this->max . "}$/", $string)) {
            return true;
        } else {
            return false;
        }
    }
        
}

