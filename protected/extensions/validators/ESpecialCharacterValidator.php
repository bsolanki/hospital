<?php
/**
 * 
 * EUsernameValidator class
 * 
 */

class ESpecialCharacterValidator extends CValidator{
    
    //Minimum string length
    public $min = 6;
    
    /**
	 * (non-PHPdoc)
	 * @see CValidator::validateAttribute()
	 */
    protected function validateAttribute($object,$attribute){
       if(!$this->checkSpecialCharacter($object->$attribute)){
            $message=$this->message!==null?$this->message:Yii::t("ESpecialCharacterValidator","{attribute} must be Alpha-Numeric.");
			$this->addError($object,$attribute,$message);
       }
    }
    
    protected function checkSpecialCharacter($string){
        if (preg_match("/[a-zA-Z0-9]+/", $string)) {
            return true;
        } else {
            return false;
        }
    }
        
}

