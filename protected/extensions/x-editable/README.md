X-editable for Yii
======================

Bundle of widgets and server-side component for creating editable elements in [Yii](http://www.yiiframework.com) application.
This extension comes instead of previous [yii-bootstrap-editable](http://www.yiiframework.com/extension/yii-bootstrap-editable) that was upgraded to [X-editable](http://vitalets.github.com/x-editable) library.  
Main changes are:

* support of several libraries: Twitter Bootstrap, jQuery UI and pure jQuery
* popup and inline modes. You can toggle it from config without changing code
* update of related models

##Demo & Documentation
Please see **[x-editable.demopage.ru](http://x-editable.demopage.ru)**

##Issues
Feel free to open issue if you find bug or have an idea.
Thanks for your feedback!	

##Contribution
Submit your ideas/solutions to `dev` branch. Thank you!

## License
Copyright (c) 2012 Vitaliy Potapov  
Licensed under the MIT licenses.


Truelister
vxp
advertisement phase 1
advertisement phase 3
ipageMD

<?php

Yii::app()->clientScript->registerScript('search1', "

function changestatus(model, id) {
            $.ajax({url: ".Yii::app()->baseUrl . "/"." +model + '/changestatus/' + id, success: function (result) {
                   setTimeout(update_grid_view() , 2000);
                }});
        }
$(document).ready(function(){
    $('.ustauts').on( 'click' ,function(){
        changestatus('user' ,  $(this).parent().attr('class') );
        
    });

});        

");
?>



