<?php
class uniqueColumnValidator extends CValidator
{
	private $allowEmpty = false;
	public function setAllowEmpty($value) {$this->allowEmpty = $value;}
	public function getAllowEmpty() {return $this->allowEmpty;}

	private $caseSensitive = false;
	public function setCaseSensitive($value) {$this->caseSensitive = $value;}
	public function getCaseSensitive() {return $this->caseSensitive;}



	protected function validateAttribute($object,$attribute)
	{
		$attributes = null;
		$criteria=array('condition'=>'');
		if(false !== strpos($attribute, "+"))
		{
			$attributes = explode("+", $attribute);
		}
		else
		{
			$attributes = array($attribute);
		}

                if(isset($attributes[0]))
		{
                        $attribute = $attributes[0];
			$value = $object->$attribute;
			if($this->allowEmpty && ($value===null || $value===''))
				return;
			$column=$object->getTableSchema()->getColumn($attribute);
			if($column===null)
				throw new CException(Yii::t('yii','{class} does not have attribute "{attribute}".',
			array('{class}'=>get_class($object), '{attribute}'=>$attribute)));
			$columnName=$column->rawName;
			if(''!=$criteria['condition'])
			{
				$criteria['condition'].= " AND ";
			}
			$criteria['condition'].=$this->caseSensitive ? "$columnName=:$attribute" : "LOWER($columnName)=LOWER(:$attribute)";
			$criteria['params'][':'.$attribute]=$value;
		}
	
                //Status NOT DELETED
                if(isset($attributes[1]))
		{
                        $attribute = $attributes[1];
			$value = $object->$attribute;
			if($this->allowEmpty && ($value===null || $value===''))
				return;
			$column=$object->getTableSchema()->getColumn($attribute);
			if($column===null)
				throw new CException(Yii::t('yii','{class} does not have attribute "{attribute}".',
			array('{class}'=>get_class($object), '{attribute}'=>$attribute)));
			$columnName=$column->rawName;
			if(''!=$criteria['condition'])
			{
				$criteria['condition'].= " AND ";
			}
			$criteria['condition'].=$this->caseSensitive ? "$columnName<>:$attribute" : "LOWER($columnName)<>LOWER(:$attribute)";
			$criteria['params'][':'.$attribute]='2';
		}                             
                
		if($column->isPrimaryKey)
			$exists=$object->exists($criteria);
		else
		{
			// need to exclude the current record based on PK
			$criteria['limit']=2;
			$objects=$object->findAll($criteria);
			$n=count($objects);
			if($n===1)
			{
				if(''==$object->getPrimaryKey())
				{
					$exists = true;
				}
				else
				{
					$exists=$objects[0]->getPrimaryKey()!==$object->getPrimaryKey();
				}
			}
			else
				$exists=$n>1;
		}
		if($exists)
		{
			$message = '';
			$labels = $object->attributeLabels();
                        
                        if(isset($attributes[0]))
                        {   
                            $attribute = $attributes[0];
                            $message .= $labels[$attribute];
                            $message = $this->message!==null ? $this->message : "$message '".$object->$attribute."' has already been taken.";
                            
                            $this->addError($object,$attribute,$message);
                        }
			
		}
	}
}
?>
