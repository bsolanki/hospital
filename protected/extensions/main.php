<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Centralized Fonality Reporting System',
    'defaultController' => 'site/index',
    'preload' => array('log', 'booster'),
    'theme' => 'abound',
    'aliases' => array(
        'editable.assets.bootstrap-editable' => realpath(__DIR__ . '/../extensions/editable/assets/bootstrap-editable'), // change if necessary
        'editable' => 'application.extensions.editable',
    ),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
        'application.modules.auditTrail.models.AuditTrail',
        'application.extensions.bootstrap.*',
        'application.extensions.editable.*', //easy include of editable classes
        'ext.ECompositeUniqueValidator',
    ),
    // modules details
    'modules' => array(
        // Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'ecosmob',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1', '192.168.1.123', '192.168.1.145', '192.168.1.122', '192.168.1.102'),
        ),
        'rights' => array(
            'superuserName' => 'Admin', // Name of the role with super user privileges. 
            'userClass' => 'User',
            'authenticatedName' => 'Authenticated', // Name of the authenticated user role. 
            'userIdColumn' => 'id', // Name of the user id column in the database. 
            'userNameColumn' => 'username', // Name of the user name column in the database. 
            'enableBizRule' => true, // Whether to enable authorization item business rules. 
            'enableBizRuleData' => true, // Whether to enable data for business rules. 
            'displayDescription' => true, // Whether to use item description instead of name. 
            'flashSuccessKey' => 'RightsSuccess', // Key to use for setting success flash messages. 
            'flashErrorKey' => 'RightsError', // Key to use for setting error flash messages. 
            'baseUrl' => '/rights', // Base URL for Rights. Change if module is nested. 
            'layout' => 'rights.views.layouts.main', // Layout to use for displaying Rights. 
            'appLayout' => 'application.views.layouts.main', // Application layout. 
            'cssFile' => 'rights.css', // Style sheet file to use for Rights. 
            'install' => false, // Whether to enable installer. 
            'debug' => false,
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'class' => 'RWebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('/site/login'),
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        //X-editable config
        'editable' => array(
            'class' => 'application.extensions.editable.EditableConfig',
            'form' => 'bootstrap', //form style: 'bootstrap', 'jqueryui', 'plain' 
            'mode' => 'popup', //mode: 'popup' or 'inline'  
            'defaults' => array(//default settings for all editable elements
                'emptytext' => 'Click to set'
            )
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array('Authenticated', 'user'),
        ),
        'messages' => array(
            'class' => 'CPhpMessageSource',
            'basePath' => 'protected/messages',
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
          // */
// 		'db'=>array(
// 			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
// 		),
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=crt_fonality_clientdb',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'ecosmob',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'chintan.fadia@ecosmob.com',
        'defaultPageSize' => 50,
        'rowsPerPage' => array(10 => 10, 20 => 20, 50 => 50, 100 => 100),
        'localAddress' => 'http://202.131.119.126',
        'smtp_server' => 'smtp.gmail.com',
        'smtp_port' => '465',
        'smtp_username' => 'notification.fonality@gmail.com',
        'smtp_password' => 'ec0sm0bt',
        'root_path' => '/var/www/html/',
        'flasMsgTimeOut' => 5000,
        'start_year' => 2000,
        'months_array' => array(
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ),
    ),
);
